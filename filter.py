import os

def get_lexi_failed():
  lexi = 'data/lexico-summed'
  edus = 'data/clean_labeled'
  lexi_failed = open('analysis/lexicon_failed.txt', 'w')
  
  failed = []
  for f in os.listdir(lexi):
    fl = open(os.path.join(lexi,f)).readlines()
    fe = open(os.path.join(edus,f)).readlines()
    
    print 'Processing:',f
    if len(fl) != len(fe):
      print 'Bad file'
    
    for i in range(len(fl)):
      if (int(fl[i].split(',')[0])>0 and fl[i].split(',')[-1].strip() != 'pos') or \
      (int(fl[i].split(',')[0])==0 and fl[i].split(',')[-1].strip() != 'neu') or \
      (int(fl[i].split(',')[0])<0 and fl[i].split(',')[-1].strip() != 'neg'):
	failed.append((fl[i].split(',')[-1].strip(),fl[i].split(',')[0],fe[i].split('\t')[0],f))

  
  failed = sorted(failed,cmp=lambda x,y:cmp(x[0],y[0]))
  lexi_failed.write('\n'.join(map(lambda x: '\t'.join(x), failed)))
  lexi_failed.close()
  
  
  
get_lexi_failed()