import os


def process():
  source = 'lexico-summed'
  output = 'adjacent/adjacent_2'
  
  for name in os.listdir(source):
    process_file(os.path.join(source,name), os.path.join(output,name))

def process_file(f, fo):
  fc = open(f)
  foc = open(fo, 'w')
  
  score = []
  senti = []
  aspect = []
  for line in fc:
    entry = line.split(',')
    score.append(entry[0])
    senti.append(entry[-1].strip())
    aspect.append(' '.join(entry[1:-1]))
  
  # one adjacent edu
  #foc.write('%s,%s,%s,%s,%s,%s\n'%('Start','Start',senti[1],asp_shift(aspect[0],aspect[1]),score[0],senti[0]))
  #for i in range(1,len(score)-1):
    #foc.write('%s,%s,%s,%s,%s,%s\n'%(senti[i-1],asp_shift(aspect[i-1],aspect[i]),senti[i+1],asp_shift(aspect[i],aspect[i+1]),score[i],senti[i]))
  #foc.write('%s,%s,%s,%s,%s,%s\n'%(senti[len(score)-2],asp_shift(aspect[len(score)-2],aspect[len(score)-1]),'End','End',score[len(score)-1],senti[len(score)-1]))
  #foc.close()
  
  #two adjacent edus
  foc.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%('Start','Start',senti[1],asp_shift(aspect[0],aspect[1]),'Start','Start',senti[2],asp_shift(aspect[0],aspect[2]),score[0],senti[0]))
  foc.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(senti[0],asp_shift(aspect[1],aspect[0]),senti[2],asp_shift(aspect[1],aspect[2]),'Start','Start',senti[3],asp_shift(aspect[1],aspect[3]),score[1],senti[1]))
  for i in range(2,len(score)-2):
    foc.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(senti[i-1],asp_shift(aspect[i-1],aspect[i]),senti[i+1],asp_shift(aspect[i],aspect[i+1]),\
    senti[i-2],asp_shift(aspect[i],aspect[i-2]),senti[i+2],asp_shift(aspect[i],aspect[i+2]),score[i],senti[i]))
  foc.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(senti[i],asp_shift(aspect[i+1],aspect[i]),senti[i+2],asp_shift(aspect[i+1],aspect[i+2]),senti[i-1],asp_shift(aspect[i+1],aspect[i-1]),'End','End',score[i+1],senti[i+1]))
  foc.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(senti[i+1],asp_shift(aspect[i+2],aspect[i+1]),'End','End',senti[i],asp_shift(aspect[i+2],aspect[i]),'End','End',score[i+2],senti[i+2]))
  foc.close()
  
  
  
def asp_shift(asp1,asp2):
  if (asp1 in asp2) or (asp2 in asp1):
    return 'No'
  else:
    return 'Yes'
    
    
def summarize_raw():
  to_sum = 'adjacent/adjacent_2'
  outpath = '../result/baseline/adjacent.2'
  fo = open(outpath, 'w')
  
  for name in os.listdir(to_sum):
    fo.write(open(os.path.join(to_sum,name)).read())
    
    
def summarize_merge():
  to_sum = 'adjacent/adjacent_2'
  outpath = '../result/baseline/adjacent.3'
  fo = open(outpath, 'w')
  
  for name in os.listdir(to_sum):
    f = open(os.path.join(to_sum,name))
    for line in f:
      entry = line.split(',')
      fo.write(','.join([entry[0]+entry[1],entry[2]+entry[3],entry[4]+entry[5],entry[6]+entry[7],entry[8],entry[9]]))
    
    
def go():
  summarize_merge()
  
go()