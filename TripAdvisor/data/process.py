import os
import itertools
import re
import copy
import codecs
import treetaggerwrapper
import sys
sys.path.append('/home/eastdog/Software/stanford-parser-python-r22186/src/stanford_parser/')
import stanfordparser



sentences = [] #sentence list
s_nouns = [] #list of relevant nouns in every sentence
aspects = [] #aspect list of every sentence
polarities = [] #polarity list of every sentence

doc_index = {} #doc_name:[index of items (sen, asp, pol, etc.)]

afinn_score_pure = [] # afinn polarity score for each sentence
huliu_score_pure = []
ofinder_score_pure = [] #opinion finder's subjective dict

#aspect sensitive scores
afinn_score = []
huliu_score = []
ofinder_score = []

tagger = treetaggerwrapper.TreeTagger(TAGLANG='en',TAGDIR='/home/eastdog/Software/TreeTagger')
sp = stanfordparser.Parser()

negation = ["not", "no", "don't", "doesn't", "never", "hardly", 'none', 'nothing', 'nowhere', 
	    'neither', 'nor', 'nobody', 'scarely', 'barely', "can't", "won't", "wouldn't", "shouldn't", "couldn't"]
neg_except = ['only', 'but']

def load_dir(dir):
  for root, dirs, files in os.walk(dir):
    for name in files:
      load_file(root, name)
      
      
def load_file(d, n):
  global sentences, aspects, polarities, s_pw, doc_index
  
  temp_start = copy.copy(len(sentences))
  
  f = codecs.open(os.path.join(d, n), encoding='utf-8')
  for line in f:
    if (not line.split()) or line.startswith('.') or line.startswith('0') or line.startswith('!') or line.startswith('-RRB-') or line.startswith(','):
      pass
    else:
      entry = line.rsplit('\t')
      sentences.append(entry[0])
      aspects.append(entry[1].strip())
      polarities.append(entry[2].strip())
  
  f.close()
  
  temp_end = copy.copy(len(sentences))
  doc_index[n] = range(temp_start,temp_end)


  
def calculate_afinn():
  global afinn_score_pure, negation, neg_except
  afinn_path = '../polarity list/AFINN-111.txt'
  afinn = dict(map(lambda (k,v): (k,int(v)), [ line.split('\t') for line in open(afinn_path) ]))
  
  
  for i in range(len(sentences)):
    score_pure = 0
    score = 0
    negation_position = 1
    for w in sentences[i].lower().split():
      if w in negation:
	negation_position = -1
      if w in neg_except:
	negation_position = 1
	
      elif afinn.get(w):
	ini_pure = 0
	score_pure += afinn.get(w)*negation_position
	if len(s_nouns[i])!=0 and is_relevant(w, i):
	  ini = 0
	  score += afinn.get(w)*negation_position
	  
    afinn_score_pure.append(score_pure)
    afinn_score.append(score)

  
  """   
  for s in sentences:
    afinn_score_pure.append(sum(map(lambda word: afinn.get(word,0), s.lower().split())))
   
   """
    
def calculate_huliu():
  global huliu_score_pure, huliu_score, negation, neg_except
  huliu_negative_path = '../polarity list/Hu_Liu/negative-words.txt'
  huliu_positive_path = '../polarity list/Hu_Liu/positive-words.txt'
  hn = dict.fromkeys([line.strip() for line in open(huliu_negative_path)], -1)
  hp = dict.fromkeys([line.strip() for line in open(huliu_positive_path)], 1)
  
  for i in range(len(sentences)):
    score_pure = 0
    score = 0
    negation_position = 1
    
    for w in sentences[i].lower().split():
      if w in negation:
	negation_position = -1
      if w in neg_except:
	negation_position = 1
	
      if hn.get(w):
	score_pure += -1*negation_position
	if len(s_nouns[i])!=0 and is_relevant(w, i):
	  score += -1*negation_position
      elif hp.get(w):
	score_pure += 1*negation_position
	if len(s_nouns[i])!=0 and is_relevant(w, i):
	  score += 1*negation_position
    

    huliu_score_pure.append(score_pure)
    huliu_score.append(score)  


def calculate_ofinder():
  global ofinder_score_pure, ofinder_score, negation, neg_except
  ofinder_path = '../polarity list/subjclueslen1-HLTEMNLP05.tff'
  of = {i[2][6:]:i[5][14:] for i in [line.split() for line in open(ofinder_path)]}  #dictionary of opinion finder's
  
  for i in range(len(sentences)):
    score_pure = 0
    score = 0
    negation_position = 1
    for w in sentences[i].lower().split():
      temp = of.get(w)
      if w in negation:
	negation_position = -1
      if w in neg_except:
	negation_position = 1
      if temp=='positive':
	score_pure += 1*negation_position
	if len(s_nouns[i])!=0 and is_relevant(w, i):
	  score += 1*negation_position
      elif temp=='negative':
	score_pure -= 1*negation_position
	if len(s_nouns[i])!=0 and is_relevant(w, i):
	  score -= 1*negation_position
    

    ofinder_score_pure.append(score_pure)
    ofinder_score.append(score)  
      
      
#detemine whether a polarity-carrying word is aspect-relevant, for sentence i
def is_relevant(word, i):
  global sp, sentences, aspects, s_nouns
  dependencies = sp.parseToStanfordDependencies(sentences[i].rstrip())
  tuple_dep = set([(gov.text.lower(), dep.text.lower()) for rel, gov, dep in dependencies.dependencies])
  
  for entries in s_nouns[i]:
    if (entries[1],word) in tuple_dep:
      return True
    elif (word,entries[1]) in tuple_dep:
      return True
    else:
      for tuples in tuple_dep:
	if tuples[1] == entries[1] and (tuples[1],word) in tuple_dep:
	  return True
  
  return False
      

def extrac_noun(sen):
  global tagger
  noun = []
  tags = tagger.TagText(sen)
  for w in tags:
    entry = w.rsplit('\t')
    if (entry[1]=='NN' or entry[1]=='NNS' or entry[1]=='PP') and entry[2]!='<unknown>' and entry[2]!='%' and entry[2]!='class':
      noun.append((entry[2],entry[0])) #(lemma, token)
  return noun


#construct a list of aspect-relevant nouns for each sentence 
def aspect_noun_mapping():
  global sentences, aspects, s_nouns
  dict = {}.fromkeys(set(aspects),[])
  for i in range(len(aspects)):
    nouns = extrac_noun(sentences[i])
    s_nouns.append(nouns)
    map(lambda x:dict[x.strip()].extend([entry[0] for entry in nouns]), aspects[i].split(','))
  
  signaficant = 3
  

  for key in dict.keys():
    templist = dict[key]
    tempset = set(templist)
    setcopy = copy.copy(tempset)
    for item in setcopy:
      if templist.count(item) < signaficant:
	tempset.remove(item)
    
    dict[key] = copy.copy(tempset)

  for i in range(len(s_nouns)):
    for word_tuple in s_nouns[i]:
	if not word_tuple[0] in dict[aspects[i]]:
	  s_nouns[i].remove(word_tuple)
	

  
""" 
def get_all_noun():
  #all_noun = set(itertools.chain(*list(itertools.chain(*dict.values()))))
  global s_nouns
  s_nouns = [extrac_noun(s) for s in sentences]
  all_noun = set(itertools.chain(*s_nouns))
  index = range(0, len(all_noun))
  return dict(zip(all_noun, index))
"""


def arff():
  global afinn_score_pure, afinn_score, huliu_score_pure, ofinder_score_pure, polarities, sentences, aspects
  calculate_afinn()
  print "afinn	Done"
  calculate_huliu()
  print "huliu Done"
  calculate_ofinder()
  print "ofinder Done"
  f = open('result/baseline_aspectsensitive.arff', 'w+')
  f_lf = open('analysis/lexicon_failed.txt','w+')
  f.write('@RELATION EDU_polarity_prediction \n\n')
  f.write('@ATTRIBUTE afinn_pure NUMERIC\n')
  f.write('@ATTRIBUTE afinn NUMERIC\n')
  f.write('@ATTRIBUTE huliu_pure NUMERIC\n')
  f.write('@ATTRIBUTE huliu NUMERIC\n')
  f.write('@ATTRIBUTE ofinder_pure NUMERIC\n')
  f.write('@ATTRIBUTE ofinder NUMERIC\n')
  
  f.write("@ATTRIBUTE class {pos,neg,neu}\n\n")
  f.write('@DATA \n')
  
  for i in range(len(polarities)):
    f.write("%s,%s,%s,%s,%s,%s,%s\n" % (afinn_score_pure[i],afinn_score[i],huliu_score_pure[i],  \
             huliu_score[i],ofinder_score_pure[i],ofinder_score[i],polarities[i]))
    if (afinn_score_pure[i]+afinn_score[i]+huliu_score_pure[i]+huliu_score[i]+ofinder_score_pure[i]+ofinder_score[i]) == 0 \
        and (polarities[i]!='neu'):
      f_lf.write(polarities[i] + '\t' + sentences[i])
  
  f.close()
  f_lf.close()

  """
def construct_arff(dict):
  global aspects, s_nouns
  f = open('noun_based.arff', 'w+')
  f.write('@RELATION noun_based_aspect_classification \n\n')
  for key in dict.keys():
    f.write('@ATTRIBUTE '+ key +' NUMERIC\n')
  
  aspect_set = set(aspects)
  f.write('@ATTRIBUTE class {' + ','.join(aspect_set) + '}\n')
  f.write('@DATA\n')
  
  last = str(len(dict))
  for i in range(0,len(dict)):
    f.write('{')
    bag = set(s_nouns[i])
    temp = []
    for w in bag:
      temp.append(dict[w])
    temp.sort()
    f.write(', '.join(["%s 1" %(t) for t in temp]))
    f.write(' '+last+' '+aspects[i]+'}\n')
"""
def build_doc_rep():
  
  global doc_index, afinn_score_pure, afinn_score, huliu_score_pure, ofinder_score_pure, polarities, sentences, aspects
  outpath = 'lexicon-based'
  keys = doc_index.iterkeys()  
  for k in keys:
    fo = open(os.path.join(outpath, k), 'w')
    indexs = doc_index.get(k)
    
    for i in indexs:
      fo.write("%s,%s,%s,%s,%s,%s,%s,%s\n"  % (afinn_score_pure[i],afinn_score[i],
      huliu_score_pure[i],huliu_score[i],ofinder_score_pure[i],ofinder_score[i],aspects[i],polarities[i]))


def go():
  dir = 'labeled'
  load_dir(dir)
  aspect_noun_mapping()
  calculate_afinn()
  print "afinn	Done"
  calculate_huliu()
  print "huliu Done"
  calculate_ofinder()
  print "ofinder Done"
  build_doc_rep()
  
go()


    
      
      
        
    
    
    
