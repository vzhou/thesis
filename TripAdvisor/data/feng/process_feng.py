import os
import re
import numpy as np
import math
import copy
import itertools
#from sklearn import cross_validation
#from sklearn import svm
#from sklearn import tree
#from sklearn import metrics
#from sklearn.feature_selection import SelectKBest
#from sklearn.feature_extraction import FeatureHasher


parsedpath = '/home/eastdog/thesis/data/feng/parsed'
labeledpath = '/home/eastdog/thesis/data/clean_labeled'
lexipath = '/home/eastdog/thesis/data/lexicon-based'
#outpath = '/home/eastdog/thesis/data/feng/feng-featured' #original
#outpath = '/home/eastdog/thesis/data/feng/feng-featured-1' # predicted value
outpath = '/home/eastdog/thesis/data/feng/feng-featured-2' # dont predict, keep the lexi score (sum up) 

#def lexi_classify(scores):
  #upper = 0
  #lower = 0
  
  #score = sum(map(lambda x:int(x), scores.split(',')[:6]))
  #if score > upper:
    #return 'pos'
  #elif score < lower:
    #return 'neg'
  #else:
    #return 'neu'
  

def load_file(parsedfile, lexifile, outfile):
  
  fp = open(parsedfile)
  fl = open(lexifile).readlines()
  
  fo = open(outfile, 'w')
  
  #read parsedfile
  lines = []
  content_index = 0
  false_index = -1
  for line in fp:
    line = line.rstrip()
    space = (len(line)-len(line.strip()))/2
    line = line.strip()
    content = False
    index = false_index
    if not ('[' in line and line.startswith('(')):
      content = True
      index = content_index
      content_index += 1
    lines.append((line, space, content, index))  
  
  #read labeledfile, and the lexi info
  aspects = [','.join(line.split(',')[6:-1]).strip() for line in fl]
  senti_labels = [line.split(',')[-1].strip() for line in fl]
  #sentiments = map(lexi_classify, fl) #[lexi_classify(line) for line in fl]
  #sentiments = senti_labels
  sentiments = [str(sum(map(lambda x:int(x), line.split(',')[:6]))) for line in fl]
  
  
  
  #write
  relation_recognizer = re.compile(r'\(([\w-]+)\[([NS])\]\[([NS])\]')
  i = 1
  length = len(lines)
  empty = ('Non','Non','Non','Non') #name, role, aspect-shift, sentiment
  while i < length:
    if not lines[i][2]:
      i += 1
      pass
    
    elif (i+3)<length and (not lines[i+1][2]) and lines[i+2][2] and lines[i+3][2] and \
         lines[i][1] == lines[i+1][1] == (lines[i+2][1]-1) == (lines[i+3][1]-1):
      index1 = lines[i][3]
      index2 = lines[i+2][3]
      index3 = lines[i+3][3]
      relation1 = relation_recognizer.search(lines[i-1][0])
      relation2 = relation_recognizer.search(lines[i+1][0])
      aspect_shift1 = find_aspect_shift(aspects[index1], aspects[index2], aspects[index3])
      aspect_shift2 = find_aspect_shift(aspects[index2], aspects[index3])
      e11 = empty # for the first edu, relation depth one
      e12 = (relation1.group(1), relation1.group(2), aspect_shift1, find_sentiment(sentiments[index2], sentiments[index3], relation2.group(2)))
      e21 = (relation2.group(1), relation2.group(2), aspect_shift2, sentiments[index3])
      e22 = e32 = (relation1.group(1), relation1.group(3), aspect_shift1, sentiments[index1])
      e31 = (relation2.group(1), relation2.group(3), aspect_shift2, sentiments[index2])
      fo.write('%s,%s,%s\n'%(','.join(e11), ','.join(e12), senti_labels[index1]))
      fo.write('%s,%s,%s\n'%(','.join(e21), ','.join(e22), senti_labels[index2]))
      fo.write('%s,%s,%s\n'%(','.join(e31), ','.join(e32), senti_labels[index3]))
      i += 4
      pass
      
    elif (i+1)< length and lines[i+1][2] and lines[i][1] == lines[i+1][1]:
      relation1 = relation_recognizer.search(lines[i-1][0])
      index1 = lines[i][3]
      index2 = lines[i+1][3]
      aspect_shift1 = find_aspect_shift(aspects[index1], aspects[index2])
      e11 = (relation1.group(1), relation1.group(2), aspect_shift1, sentiments[index2])
      e21 = (relation1.group(1), relation1.group(3), aspect_shift1, sentiments[index1])     
      
      if (i+2)<length and lines[i+2][2] and (lines[i+2][1]+1) == lines[i][1]:
	relation2 = relation_recognizer.search(lines[i-2][0])
	index3 = lines[i+2][3]
	e31 = empty
	aspect_shift2 = find_aspect_shift(aspects[index3], aspects[index1], aspects[index2])
	e12 = e22 = (relation2.group(1), relation2.group(2), aspect_shift2, sentiments[index3])
	e32 = (relation2.group(1), relation2.group(3), aspect_shift2, find_sentiment(sentiments[index1], sentiments[index2],relation1.group(2)))
	fo.write('%s,%s,%s\n'%(','.join(e11), ','.join(e12), senti_labels[index1]))
	fo.write('%s,%s,%s\n'%(','.join(e21), ','.join(e22), senti_labels[index2]))
	fo.write('%s,%s,%s\n'%(','.join(e31), ','.join(e32), senti_labels[index3]))
	i += 3
	pass
      
      elif (i+4)<length and (not lines[i+2][2]) and lines[i+3][2] and lines[i+4][2] and \
           lines[i+3][1] == lines[i+4][1] == lines[i][1]:
	relation2 = relation_recognizer.search(lines[i-2][0])
	relation3 = relation_recognizer.search(lines[i+2][0])
	index3 = lines[i+3][3]
	index4 = lines[i+4][3]
	aspect_shift2 = find_aspect_shift(aspects[index3], aspects[index4])
	e31 = (relation3.group(1), relation3.group(2), aspect_shift2, sentiments[index4])
	e41 = (relation3.group(1), relation3.group(3), aspect_shift2, sentiments[index3])
	
	aspect_shift3 = find_aspect_shift(aspects[index1], aspects[index2], aspects[index3], aspects[index4])
	e12 = e22 = (relation2.group(1), relation2.group(2), aspect_shift3, find_sentiment(sentiments[index3], sentiments[index4], relation3.group(2)))
	e32 = e42 = (relation2.group(1), relation2.group(3), aspect_shift3, find_sentiment(sentiments[index1], sentiments[index2], relation1.group(2)))
	fo.write('%s,%s,%s\n'%(','.join(e11), ','.join(e12), senti_labels[index1]))
	fo.write('%s,%s,%s\n'%(','.join(e21), ','.join(e22), senti_labels[index2]))
	fo.write('%s,%s,%s\n'%(','.join(e31), ','.join(e32), senti_labels[index3]))
	fo.write('%s,%s,%s\n'%(','.join(e41), ','.join(e42), senti_labels[index4]))
	i+=5
	pass
      
      else:
	e12 = e22 = empty
	fo.write('%s,%s,%s\n'%(','.join(e11), ','.join(e12), senti_labels[index1]))
	fo.write('%s,%s,%s\n'%(','.join(e21), ','.join(e22), senti_labels[index2]))
	i+=2
	pass
    
    else:
      e11 = e12 = empty
      fo.write('%s,%s,%s\n'%(','.join(e11), ','.join(e12), senti_labels[lines[i][3]]))
      i+=1
      pass
  
    
      
def load_all():
  global lexipath, parsedpath, outpath
  files = os.listdir(lexipath)
  for f in files:
    print 'Processing\t', f
    extendf = f+'.txt.tree'
    p = os.path.join(parsedpath, extendf)
    l = os.path.join(lexipath, f)
    o = os.path.join(outpath, f)
    load_file(p, l, o)      
      

def find_aspect_shift(a1, a2, a3=None, a4=None):
  
  if a3 == None:
    linked_aspect = a2
    current = a1
  elif a3 != None and a4 == None:
    linked_aspect = a2+a3
    current = a1
  else:
    linked_aspect = a3+a4
    current = a1+a2
  
  aspect_shift = 'Yes'
  if current in linked_aspect or linked_aspect in current:
    aspect_shift = 'No'
    
  return aspect_shift

  
def find_sentiment(s1, s2, role_of_s1):
  if role_of_s1 == 'N':
    return s1
  else:
    return s2
      
def seek_lexi_distr():
  lexi_label = []
  f = open('/home/eastdog/thesis/data/feng/analysis/golden')
  for line in f:
    entry = line.split(',')
    lexi_label.append((int(entry[-2]), entry[-1].strip()))
  f.close()
  
  for i in range(0, 5):
    pos = [ll[1] for ll in lexi_label if ll[0]>i]
    pos_acc = copy.copy(float(pos.count('pos'))/len(pos))
  for j in range(0, -5, -1):
    neg = [ll[1] for ll in lexi_label if ll[0]<j]
    neg_acc = copy.copy(float(neg.count('neg'))/len(neg))
    neu = [ll[1] for ll in lexi_label if j<=ll[0]<=i]
      neu_acc = copy.copy(float(neu.count('neu'))/len(neu))
      print i, pos_acc, j, neg_acc, neu_acc
      
      
def test():
  feng = '/home/eastdog/thesis/data/feng/feng-featured-1'
  lexi = '/home/eastdog/thesis/data/lexicon-based'
  
  for f in os.listdir(feng):
    fe = open(os.path.join(feng,f)).readlines()
    fl = open(os.path.join(lexi,f)).readlines()
    if not len(fe) == len(fl):
      print f, len(fe), len(fl)
    

def test2():
  feng = '/home/eastdog/thesis/data/feng/feng-1.arff'
  content = open(feng).readlines()
  fo = open('temp.arff', 'w')
  for line in content:
    entry = line.split(',')
    if -1<int(entry[-2])<1:
      fo.write(line)
  fo.close()
  

def test3():
  f = '/home/eastdog/thesis/data/feng/temp.arff'
  content = open(f).readlines()
  one = [line.split(',')[0] for line in content]
  print set(one)
  print '#####'
  two = [line.split(',')[1] for line in content]
  print set(two)
  #relations = set(one)|set(two)
  #for r in relations:
    #print r, one.count(r)+two.count(r)
    
    
def test4():
  f = '/home/eastdog/thesis/data/feng/feng-2.arff'
  fop = '/home/eastdog/thesis/data/feng/temp.arff'
  content = open(f).readlines()
  fo = open(fop, 'w')
  for line in content:
    entry = line.split(',')
    #one = '_'.join(entry[:4])
    #two = '_'.join(entry[4:8])
    one = '%s_%s'%(entry[0],entry[3])
    two = '%s_%s'%(entry[4],entry[7])
    others = ','.join(entry[8:])
    fo.write('%s,%s,%s'%(one,two,others))
  fo.close()

def test5():
  g = '/home/eastdog/thesis/data/feng/golden'
  p = '/home/eastdog/thesis/data/feng/predicted'
  gp = 'golden+predicted'
  
  fg = open(g).readlines()
  fp = open(p).readlines()
  fo = open(gp, 'w')
  
  for i in range(len(fg)):
    gentry = fg[i].split(',')
    pentry = fp[i].split(',')
    temp = gentry[:4]
    temp.append(pentry[3])
    temp.extend(gentry[4:8])
    temp.append(pentry[7])
    temp.extend(gentry[8:])
    fo.write(','.join(temp))
  
  fo.close()
    
    
    
  
def summarize():
  feng = '/home/eastdog/thesis/data/feng/feng-featured-2'
  lexi = '/home/eastdog/thesis/data/lexicon-based'
  o = open('analysis/numeric','w')
  files = os.listdir(feng)
  for f in files:
    ff = open(os.path.join(feng,f)).readlines()
    fl = open(os.path.join(lexi,f)).readlines()
    for i in range(len(ff)):
      if ff[i].split(',')[-1].strip()==fl[i].split(',')[-1].strip():
	o.write(','.join(ff[i].split(',')[:-1]))
	o.write(',%s'%sum(map(lambda x:int(x), fl[i].split(',')[:6])))
	o.write(',%s'%ff[i].split(',')[-1])
      else:
	print f, i

	
	
def go():
  #test5()
  #load_all()
  #summarize()
  seek_lexi_distr()

  
  
go()  