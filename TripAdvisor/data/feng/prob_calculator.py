from __future__ import print_function
import copy
from collections import defaultdict
from pprint import pprint



golden = 'analysis/golden'
relations = ['Elaboration', 'Joint', 'Contrast', 'Background', 'Enablement', 'Condition', 'Attribution', 'Temporal', 'Explanation', 'Cause']
senti_shift = [x+y for x in ['pos','neg','neu'] for y in ['pos','neg','neu']]
senasp_shift = [x+y+z for x in ['Yes','No'] for y in ['pos','neg','neu'] for z in ['pos','neg','neu']]

def load():
  global golden, relations
  
  f = open(golden)
  depOne = defaultdict(list)
  depOne.fromkeys(relations)
  depTwo = defaultdict(list)
  depTwo.fromkeys(relations)
  #depOne = {}.fromkeys(relations)
  #depTwo = {}.fromkeys(relations, list())
  
  for line in f:
    entry = line.split(',')
    if entry[0] in relations:
      depOne[copy.deepcopy(entry[0])].append(copy.deepcopy(entry[:4]+[entry[-1]]))
    if entry[4] in relations:
      depTwo[copy.deepcopy(entry[4])].append(copy.deepcopy(entry[4:8]+[entry[-1]]))
  
  f.close()
  return depOne, depTwo
  

def raw():
  global relations
  model = 'raw'
  
  d1, d2 = load()
  d1, d2 = calculate_prob(d1, model), calculate_prob(d2, model)
  return d1, d2

def asp():
  global relations
  model = 'asp'
  
  d1, d2 = load()
  d1, d2 = calculate_prob(d1, model), calculate_prob(d2, model)
  return d1, d2
  
  
def calculate_prob(d, model):
  global senti_shift, senasp_shift
  
  if model == 'raw':
    keyset = senasp_shift
  elif model == 'asp':
    keyset = senasp_shift
        
  result = {}
  for k in d.keys():
    content = extract(d.get(k), model)
    content_set = set(content)
    senti_prob = {}.fromkeys(keyset, 0)
    for c in content_set:
      temp_prob = float('%0.3f'%(float(content.count(c))/len(content)))
      senti_prob[c] = copy.deepcopy(temp_prob)
    result[k] = copy.copy(senti_prob)
  return copy.deepcopy(result)
  
  
def extract(l, model):
  if model == 'raw':
    return map(lambda x: (x[-2]+x[-1].strip()), l)
  elif model == 'asp':
    return map(lambda x: (x[2]+x[3]+x[-1].strip()), l)
  
  
#d1, d2 = asp()
#for k in d2.keys():
  #print (k+':')
  #map(lambda x:print (x), sorted(d2.get(k).items(), key = lambda prob: prob[1]))
  ###pprint()
    
