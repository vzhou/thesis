import os
import copy
import prob_calculator as pc
import math

relations = ['Elaboration', 'Joint', 'Contrast', 'Background', 'Enablement', 'Condition', 'Attribution', 'Temporal', 'Explanation', 'Cause']

def check_multitag():
  folder = '/home/eastdog/thesis/data/clean_labeled'
  propertags = set(['pos', 'neg', 'neu'])
  for f in os.listdir(folder):
    for line in open(os.path.join(folder,f)):
      if line.split('\t')[2].strip() not in propertags:
	print f, line
	
	
def merge_lexi():
  to_merge = '/home/eastdog/thesis/data/feng/feng.arff'
  mergerd = '/home/eastdog/thesis/data/feng/feng_merged_lexi.arff'
  fo = open(mergerd, 'w')
  for line in open(to_merge):
      if line.startswith('@') or not line.split():
	fo.write(line)
	pass
      else:
	entry = line.split(',')
	discourse = entry[:8]
	mergerd_sum = sum(map(lambda x:int(x), entry[8:14]))
	fo.write('%s,%s,%s'%(','.join(discourse),mergerd_sum,entry[-1]))
	
  fo.close()

  
  
def transform2():
  """
  Prob countings
  """
  
  global relations
  d1, d2 = pc.raw()
  fg = 'analysis/golden'
  fo = open('analysis/golden.3.1', 'w')
  non = ['0','0','0']  
  for line in open(fg):
    entry = line.split(',')
    if entry[0] in relations:
      ppos1, pneg1, pneu1 = prob_cast(entry[0], entry[3], None, d1, asp = '')
      dp1p = map(lambda x:str(x), [ppos1, pneg1, pneu1])
    else:
      dp1p = non
    if entry[4] in relations:
      ppos2, pneg2, pneu2 = prob_cast(entry[4], entry[7], None, d2, asp = '')
      dp2p = map(lambda x:str(x), [ppos2, pneg2, pneu2])
    else:
      dp2p = non
      
    fo.write('%s,%s,%s,%s'%(','.join(dp1p), ','.join(dp2p), entry[-2], entry[-1]))

    
def transform2_1():
  """
  Prob countings, asp model
  """
  
  global relations
  d1, d2 = pc.asp()
  fg = 'analysis/golden'
  fo = open('analysis/golden.6', 'w')
  non = ['0','0','0']  
  for line in open(fg):
    entry = line.split(',')
    if entry[0] in relations:
      ppos1, pneg1, pneu1 = prob_cast(entry[0], entry[3], None, d1, entry[2])
      dp1p = map(lambda x:str(x), [ppos1, pneg1, pneu1])
    else:
      dp1p = non
    if entry[4] in relations:
      ppos2, pneg2, pneu2 = prob_cast(entry[4], entry[7], None, d2, entry[6])
      dp2p = map(lambda x:str(x), [ppos2, pneg2, pneu2])
    else:
      dp2p = non
      
    fo.write('%s,%s,%s,%s'%(','.join(dp1p), ','.join(dp2p), entry[-2], entry[-1]))    
    
    
def transform3():
  """
  multiple depth one and two
  """
  f = open('/home/eastdog/thesis/data/feng/analysis/golden.3').readlines()
  fo = open('/home/eastdog/thesis/data/feng/analysis/golden.4', 'w')
  for line in f:
    entry = line.split(',')
    numbers = map(lambda x:float(x), entry[:6])
      
    pos = numbers[0]*numbers[3] or (numbers[0]+numbers[3])
    neg = numbers[1]*numbers[4] or (numbers[1]+numbers[4])
    neu = numbers[2]*numbers[5] or (numbers[2]+numbers[5])
    fo.write('%s,%s,%s,%s,%s'%(pos,neg,neu,entry[6],entry[7]))

    
# This function cast lexi score of linking edu to prob    
def transform4():
  """
  casting prob model, pc.raw()
  """
  global relations
  d1, d2 = pc.raw()
  dlx = lexi_prob()
  fn = '/home/eastdog/thesis/data/feng/analysis/numeric'
  fo = open('analysis/prob.3', 'w')
  non = ['0', '0', '0']
  for line in open(fn):
    entry = line.split(',')
    if entry[0] in relations and inrange(entry[3]):
      dp1p = map(lambda x:str(x), prob_cast(entry[0], int(entry[3]), dlx, d1))
    else:
      dp1p = non
    if entry[4] in relations and inrange(entry[7]):
      dp2p = map(lambda x:str(x), prob_cast(entry[4], int(entry[7]), dlx, d1))
    else:
      dp2p = non
    
    fo.write('%s,%s,%s,%s'%(','.join(dp1p), ','.join(dp2p), entry[-2], entry[-1]))

def transform4_1():
  """
  variation of transform 4, pc.asp()
  """
  global relations
  d1, d2 = pc.asp()
  dlx = lexi_prob()
  fn = '/home/eastdog/thesis/data/feng/analysis/numeric'
  fo = open('analysis/prob.6', 'w')
  non = ['0', '0', '0']
  for line in open(fn):
    entry = line.split(',')
    if entry[0] in relations and inrange(entry[3]):
      dp1p = map(lambda x:str(x), prob_cast(entry[0], int(entry[3]), dlx, d1, entry[2]))
    else:
      dp1p = non
    if entry[4] in relations and inrange(entry[7]):
      dp2p = map(lambda x:str(x), prob_cast(entry[4], int(entry[7]), dlx, d1, entry[6]))
    else:
      dp2p = non
    
    fo.write('%s,%s,%s,%s'%(','.join(dp1p), ','.join(dp2p), ','.join(map(lambda x:str(x), dlx.get(int(entry[-2])))), entry[-1]))
  
def transform4_2():
  """
  special for lexi = 0
  """
  f = open('analysis/prob.6')
  fo = open('analysis/prob.6_1', 'w')
  for line in f:
    entry = line.split(',')
    if float(entry[6]) == 0.189:
      fo.write(line)
    
  
  
  
  
    
#This function prob-ize lexi score
def transform5():
  f = open('analysis/prob.3')
  fo = open('analysis/prob.4', 'w')
  d = lexi_prob()
  for line in f:
    entry = line.split(',')
    lexi_score = int(entry[-2])	
    fo.write('%s,%s,%s'%(','.join(entry[:6]), ','.join(map(lambda x:str(x), d.get(lexi_score))), entry[-1]))



def transform7():
  """
  try to merge three probs, in different ways
  """
  
  f = open('analysis/prob.6')
  fo = open('analysis/prob.7', 'w')
  for line in f:
    entry = line.split(',')
    pos = float('%0.3f'%((float(entry[0]) or 1) + (float(entry[3]) or 1) + (float(entry[6]) or 1)))
    neg = float('%0.3f'%((float(entry[1]) or 1) + (float(entry[4]) or 1) + (float(entry[7]) or 1)))
    neu = float('%0.3f'%((float(entry[2]) or 1) + (float(entry[5]) or 1) + (float(entry[8]) or 1)))
    fo.write('%s,%s,%s,%s'%(pos,neg,neu,entry[9]))

def transform8():
  """
  by counting numbers of success and not fail
  """
  f = open('analysis/prob.6_1')
  fo = open('analysis/prob.6_1_1', 'w')
  for line in f:
    entry = line.split(',')
    win1, nolose1 = find_count(map(lambda x:float(x), entry[:3]))
    win2, nolose2 = find_count(map(lambda x:float(x), entry[3:6]))
    win3, nolose3 = find_count(map(lambda x:float(x), entry[6:9]))
    win = [win1, win2]
    nolose = [nolose1, nolose2]
    result_count = [win.count('pos'), nolose.count('pos'), win.count('neg'), nolose.count('neg'), win.count('neu'), nolose.count('neu')]
    fo.write('%s,%s'%(','.join(map(lambda x:str(x), result_count)), entry[-1]))
  
def transform9():
  """
  combine gold-depth-two with adjacent
  """
  gold = 'feng-featured'
  adjacent = '../adjacent'
  fo = open('analysis/golden/golden.7','w')
  
  for f in os.listdir(gold):
    fg = open(os.path.join(gold,f)).readlines()
    fa = open(os.path.join(adjacent,f)).readlines()
    for i in range(len(fg)):
      eg = fg[i].split(',')
      ea = fa[i].split(',')
      fo.write('%s,%s,%s,%s,%s,%s'%(''.join(eg[:4]), ''.join(eg[4:8]), (ea[0]+ea[1]), (ea[2]+ea[3]), ea[4], ea[5]))
  fo.close()

def transform10():
  """
  combine golden 6 and adjacent
  """
  fg = open('analysis/golden/golden.6').readlines()
  fa = open('analysis/golden/golden.7').readlines()
  fo = open('analysis/golden/golden.8', 'w')
  
  for i in range(len(fg)):
    featureg = ','.join(fg[i].split(',')[:6])
    featurea = ','.join(fa[i].split(',')[2:])
    fo.write('%s,%s'%(featureg, featurea))
    
  fo.close()
  
  
  
def find_count(l):
  nolose = None
  if max(l) == 0:
    win = None
  elif l[0] == max(l):
    win = 'pos'
    if l[1]>0.33:
      nolose = 'neg'
    elif l[2]>0.33:
      nolose = 'neu'
  elif l[1] == max(l):
    win = 'neg'
    if l[0]>0.33:
      nolose = 'pos'
    elif l[2]>0.33:
      nolose = 'neu'
  elif l[2] == max(l):
    win = 'neu'
    if l[0]>0.33:
      nolose = 'pos'
    elif l[1]>0.33:
      nolose = 'neg'
  return copy.copy(win), copy.copy(nolose)    
  
    
def inrange(num):
  upper = 0
  lower = 0
  num = int(num)
  if num>=upper or num<=lower:
    return True
  else:
    return False
  
  
  
def lexi_prob():
  """
  return the probs of each lexi score
  """
  
  f = open('/home/eastdog/thesis/data/feng/analysis/golden').readlines()
  num = {}
  for line in f:
    entry = line.split(',')
    num[int(entry[-2])] = num.get(int(entry[-2]),[])
    num[int(entry[-2])].append(entry[-1].strip())
  
  for k in num.keys():
    total = float(len(num.get(k)))
    pos = float('%0.3f'%(num.get(k).count('pos')/total))
    neg = float('%0.3f'%(num.get(k).count('neg')/total))
    neu = float('%0.3f'%(num.get(k).count('neu')/total))
    num[k] = (pos,neg,neu)
    
  return num

  
def prob_cast(relation, score, dlexi, drelation, asp = ''):
  """
  calculate the prob of being pos/neg/neu given the linking relation and linking edu's sentiment probs
  """
  if dlexi == None:
    lexi_pos = lexi_neg = lexi_neu = 0
    if score == 'pos':
      lexi_pos = 1
    elif score == 'neg':
      lexi_neg = 1
    elif score == 'neu':
      lexi_neu = 1
  else:
    lexi_pos, lexi_neg, lexi_neu = dlexi.get(score)
  
  re_prob = drelation.get(relation)
  pos = re_prob.get(asp+'pospos',0)*lexi_pos + re_prob.get(asp+'negpos',0)*lexi_neg + re_prob.get(asp+'neupos',0)*lexi_neu
  neg = re_prob.get(asp+'posneg',0)*lexi_pos + re_prob.get(asp+'negneg',0)*lexi_neg + re_prob.get(asp+'neuneg',0)*lexi_neu
  neu = re_prob.get(asp+'posneu',0)*lexi_pos + re_prob.get(asp+'negneu',0)*lexi_neg + re_prob.get(asp+'neuneu',0)*lexi_neu
  normalizer = (pos+neg+neu) or 1
  return (float('%0.3f'%(pos/normalizer)), float('%0.3f'%(neg/normalizer)), float('%0.3f'%(neu/normalizer)))
  
  
def merge():
  """
  merge lexi prob with discourse prob
  """
  f1 = open('analysis/lexi_prob').readlines()
  f2 = open('analysis/golden.3').readlines()
  fo = open('analysis/golden.5', 'w')
  
  for i in range(len(f1)):
    lexi_entry = f1[i].split(',')
    golden_entry = f2[i].split(',')
    fo.write('%s,%s,%s'%(','.join(golden_entry[:6]), ','.join(lexi_entry[:3]), golden_entry[-1]))

    
def wekaize(f):
  fi = open(f).readlines()
  fo = open('%s.arff'%f, 'w')
  
  number_features = len(fi[0].split(','))-1
  att = ['@ATTRIBUTE feature%s NUMERIC'%(x+1) for x in range(number_features)]
  
  fo.write('@RELATION EDU_polarity_prediction\n\n')
  fo.write('\n'.join(att))
  fo.write('\n')
  fo.write('@ATTRIBUTE class {pos,neg,neu}\n\n')
  fo.write('@DATA\n')
  fo.write(''.join(fi))
  fo.close()
  
  
    
#def transform1():
  #fp = '/home/eastdog/thesis/data/feng/golden'
  #fo = open('golden.2', 'w')
  
  #c = open(fp).read()
  #c = c.replace('Non','?')
  #fo.write(c)

  
#def transform0():
  #"""
  #casting the golden stardard features values as following,
  #Non --0 (for role, shift, semtiment)
  #other values shift to somewhere no-0
  #"""
  #fp = '/home/eastdog/thesis/data/feng/golden'
  #fo = open('golden.1', 'w')
  
  #for line in open(fp):
    #entry = line.split(',')
    #entry[1], entry[5] = covertRole(entry[1]), covertRole(entry[5])
    #entry[2], entry[6] = covertShift(entry[2]), covertShift(entry[6])
    #entry[3], entry[7] = covertSenti(entry[3]), covertSenti(entry[7])
    #fo.write(','.join(map(lambda x:str(x), entry)))
    
  #fo.close()  
  
#def covertRole(role):
  #if role == 'Non':
    #return 0
  #elif role == 'N':
    #return 1
  #elif role == 'S':
    #return 2

#def covertShift(shift):
  #if shift == 'Non':
    #return 0
  #elif shift == 'Yes':
    #return 2
  #elif shift == 'No':
    #return 1

#def covertSenti(senti):
  #if senti == 'Non':
    #return 0
  #elif senti == 'neg':
    #return 1
  #elif senti == 'neu':
    #return 2
  #elif senti == 'pos':
    #return 3

  
  
def go():
  transform10()
  #wekaize('analysis/golden.6')
  
  
  
go()