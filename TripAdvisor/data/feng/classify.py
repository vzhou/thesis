import numpy as np
import math
import copy
import itertools
from sklearn import svm
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from sklearn.feature_selection import SelectKBest
from sklearn import cross_validation
from sklearn.feature_extraction import DictVectorizer

FILE = '/home/eastdog/thesis/data/feng/golden'
relations = ['Elaboration', 'Joint', 'Contrast', 'Background', 'Enablement', 'Condition', 'Attribution', 'Temporal', 'Explanation', 'Cause']
#feature_names = ['name1', 'role1', 'shift1', 'senti1', 'name2', 'role2', 'shift2', 'senti2', 'lexi']
feature_names = ['name1', 'role1', 'shift1', 'senti1']

#def classify():
  #global FILE, relations
  #X = []
  #y = []
  #for line in open('sparse'):
    #entry = line.split(',')
    #entry[-2] = int(entry[-2])
    #X.append(entry[:-1])
    #y.append(entry[-1].strip())
  
  #names = []
  #for i in [1]:
    #names.extend(map(lambda x:str(i)+x, relations))
  #names.append('lexi')
  #X = map(lambda y:dict(zip(names,y)), X)
  #vec = DictVectorizer()
  #xt = vec.fit_transform(X).toarray()
  #clf = GaussianNB()
  ##clf = svm.SVC(kernel='rbf', C=1)
  ##clf.fit(xt, y)
  ##print clf.score(xt, y)
  #scores = cross_validation.cross_val_score(clf, xt, y, cv = 5)
  #print scores/5

  
  
def classify(f):
  X = []
  y = []
  
  for line in open(f):
    entry = numeric(line.split(','))
    X.append(entry[:-1])
    y.append(entry[-1].strip())
    
  divide = int(math.ceil(len(X) / float(10)))
  Xs = [X[i:i+divide] for i in range(0, len(X), divide)]  # ten subs of X
  ys = [y[i:i+divide] for i in range(0, len(y), divide)]
    
  print cross_classify(Xs, ys)

def cross_classify(Xs, ys):
  global feature_names
  score = 0
  foldernum = 10
  for i in range(foldernum):
    clf = svm.SVC(kernel='rbf', C=1)
    #clf = GaussianNB()
    train_X = copy.deepcopy(cross(i, Xs))
    train_y = cross(i, ys)
    #test_X = map(to_test, copy.deepcopy(Xs[i]))
    test_X = copy.deepcopy(Xs[i])
    test_y = ys[i]
    train_X, test_X = transform(feature_names, train_X, test_X)
    clf.fit(train_X, train_y)
    score += clf.score(test_X, test_y)
  
  return float(score)/10
    
  
def to_train(l):
  del l[4], l[8]
  return l
  
def to_test(l):
  del l[3], l[7]
  return l

def transform(feature_names, xtrain, xtest):
  x = xtrain + xtest
  x = map(lambda y:dict(zip(feature_names,y)), x)
  vec = DictVectorizer()
  xt = vec.fit_transform(x).toarray()
  return xt[:len(xtrain)], xt[len(xtrain):]

def numeric(l):
  for i in range(len(l)):
    try:
      l[i] = int(l[i])
    except Exception:
      pass
  return l
  
def cross(i, l):
  temp =  l[:i]
  temp.extend(l[i+1:])
  return list(itertools.chain(*temp))  
  
  
def go():
  #tree_classify()
  #reform_features()
  classify('/home/eastdog/thesis/data/feng/analysis/golden.4')
  
go()