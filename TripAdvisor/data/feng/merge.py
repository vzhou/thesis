import os
import re
import itertools
adja = '../adjacent/adjacent_2'
f4 = 'feng-featured-4'  #RST FP
f5 = 'feng-featured-5'  #RST LC

dotslash = re.compile(r'(\w+),(\w+),(\w+),(\w+),(\w+),(\w+),(\w+),(\w+)')

def mergef5():
  out = open('../../result/feng/rst_lc','w')
  outweka = open('../../result/feng/rst_lc.arff','w')
  space = []
  for f in os.listdir(f5):
    adjac = open(os.path.join(adja,f)).readlines()
    f5c = open(os.path.join(f5,f)).readlines()
    for i in range(len(f5c)):
      space.append(f5c[i].strip().split(','))
      temp = dotslash.sub(r'\1-\2,\3-\4,\5-\6,\7-\8',adjac[i].strip())
      out.write('%s,%s'%(temp,f5c[i]))
  out.close()

  space = set(itertools.chain(*space))
  space.remove('')
  index = dict(zip(space,range(5,len(space)+5)))
  total = len(space)+5
  outweka.write('@RELATION EDU_polarity_prediction\n\n')
  outweka.write('@ATTRIBUTE prev1 {Start-Start,pos-Yes,pos-No,neg-Yes,neg-No,neu-Yes,neu-No}\n')
  outweka.write('@ATTRIBUTE next1 {End-End,pos-Yes,pos-No,neg-Yes,neg-No,neu-Yes,neu-No}\n')
  outweka.write('@ATTRIBUTE prev2 {Start-Start,pos-Yes,pos-No,neg-Yes,neg-No,neu-Yes,neu-No}\n')
  outweka.write('@ATTRIBUTE next2 {End-End,pos-Yes,pos-No,neg-Yes,neg-No,neu-Yes,neu-No}\n')
  outweka.write('@ATTRIBUTE lexi NUMERIC\n')
  for fea in space:
    outweka.write('@ATTRIBUTE %s NUMERIC\n'%fea)
  outweka.write('@ATTRIBUTE class {pos,neg,neu}\n')
  outweka.write('@DATA\n')
  for line in open('../../result/feng/rst_lc'):
    entry = line.strip().split(',')
    if entry[6] == '':
      temp1 = ['%s %s'%(i,entry[i]) for i in range(5)]
      outweka.write('{%s,%s %s}\n'%(','.join(temp1), total, entry[5]))
    else:
      temp1 = ['%s %s'%(i,entry[i]) for i in range(5)]
      temp2 = sorted(list(set([index.get(e) for e in entry[6:]])))
      temp2 = ['%s 1'%i for i in temp2]
      outweka.write('{%s,%s,%s %s}\n'%(','.join(temp1), ','.join(temp2), total, entry[5]))
    

mergef5()