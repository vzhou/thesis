import copy
import re
import os
import itertools

class TreeNode:
  
  def __init__(self):
    self.senti = None
    self.role = None # N or S, None if its root node
    self.lexi = 0 # lexi score of this clause (the lexi score of nucleus if this is a relation)
    self.aspect = None
    self.clause_type = None #either e (an EDU) or r (a relation)
    self.isleft = None # whether it appears first, 1 or 0
    
    self.name = None # relation name if clause_type == r
    
    self.parent = None
    self.satellite = None
    self.nucleus = None
    
  def seek_low(self):
    if self.clause_type == 'e':
      pass
    else:
      currentNode = self
      while currentNode.clause_type != 'e':
	currentNode = currentNode.nucleus
	#print currentNode.clause_type
      #print currentNode
      self.senti = copy.copy(currentNode.senti)
      self.lexi = copy.copy(currentNode.lexi)
      self.aspect = copy.copy(currentNode.aspect)
      
      
def load():
  
  parsed = 'parsed'
  lexi = '../lexico-summed'
  out = 'feng-featured-6'
  
  files = os.listdir('parsed')
  for f in files:
    print 'Processing', f
    fr = f.replace('.txt.tree', '')
    load_file(os.path.join(parsed,f), os.path.join(lexi,fr), os.path.join(out,fr))
      
      
def load_file(f,lexi,out):
  ft = open(f).readlines()
  fl = open(lexi).readlines()
  fo = open(out, 'w')
  
  lines = []   # lines of structured content line
  
  lexis = []
  aspects = []
  sentis = []
  
  for line in fl:
    entry = line.split(',')
    lexis.append(entry[0])
    aspects.append(','.join(entry[1:-1]))
    sentis.append(entry[-1].strip())
  
  content_index = 0
  false_index = -1
  for line in ft:
    line = line.rstrip()
    space = (len(line)-len(line.strip()))/2
    line = line.strip()
    index = false_index
    if not ('[' in line and line.startswith('(')):
      index = content_index
      content_index += 1
    lines.append((space, line, index)) #structure of a line is #space, line content, index (edu index, or -1 if it is a relation)
    
  root = TreeNode()
  root.name = extract_relation_name(lines[0][1])
  root.clause_type = 'r'
  
  edus = {}
  feed(root, lines, lexis, aspects, sentis, edus)
  extend(root)
  
  for i in range(len(edus)):
    feature = []
    feature_temp = ''
    breakpoint = 0
    currentNode = edus.get(i)
    while currentNode.parent != None:
      if currentNode.role == 'S':
	breakpoint += 1
      
      # to control the depth
      if breakpoint > 1:
	break
	
      if currentNode.isleft:
	#feature_temp = feature_temp + currentNode.role + currentNode.parent.name
	currentNode = currentNode.parent
	#breakpoint += 1
      elif not currentNode.isleft:
	parent = currentNode.parent
	if currentNode == parent.nucleus:
	  sibling = parent.satellite
	else:
	  sibling =  parent.nucleus
	
	#feature_temp = feature_temp + currentNode.role + parent.name 
	feature_temp = currentNode.role + parent.name 
	if (sibling.aspect in currentNode.aspect) or (currentNode.aspect in sibling.aspect):
	  aspect_shift = 'NO'
	else:
	  aspect_shift = 'YES'
	#feature.append('%s-%s-%s'%(feature_temp,sibling.senti,aspect_shift))
	feature.append('%s-%s'%(feature_temp,sibling.senti))
	currentNode = currentNode.parent
	#breakpoint += 1
    
    fo.write(','.join(feature)+'\n')
    
  
def extend(node):
  if node.clause_type == 'r':
    node.seek_low()
    extend(node.nucleus)
    extend(node.satellite)
  else:
    pass
  
def feed(node, content, lexis, aspects, sentis, edus):
  to_feed = 1
  one = TreeNode()
  one.parent = node
  one.isleft = 1
  two = TreeNode()
  two.parent = node
  two.isleft = 0
  parent_line = content[0]
  current_sp = parent_line[0]
  
  # find which two lines are splited
  i = 2
  #print 'cotent:',content
  while to_feed>0:
    if content[i][0] == current_sp+1:
      to_feed -= 1
    else:
      i += 1
  
  #feed the two lines' contents
  feed_line(one, content[1], lexis, aspects, sentis, edus)
  feed_line(two, content[i], lexis, aspects, sentis, edus)
  one.role, two.role = role_type(parent_line[1])
  
  #link parent and children
  if one.role == 'N':
    node.nucleus = one
    node.satellite = two
  else:
    node.nucleus = two
    node.satellite = one
  
  #recursion
  if one.clause_type == 'r':
    feed(one, copy.deepcopy(content[1:i]), lexis, aspects, sentis, edus)
  if two.clause_type == 'r':
    feed(two, copy.deepcopy(content[i:]), lexis, aspects, sentis, edus)
  
  
def feed_line(node, line, lexis, aspects, sentis, edus):
  if line[2] == -1:
    node.name = extract_relation_name(line[1])
    node.clause_type = 'r'
  else:
    index = line[2]
    node.clause_type = 'e'
    node.senti = sentis[index]
    node.aspect = aspects[index]
    node.lexi = lexis[index]
    edus[index] = node
    
def role_type(r):
  s = re.search(r'\([\w-]+\[(\w)\]\[(\w)\]',r)
  r1 = s.group(1)
  r2 = s.group(2)
  return r1, r2
  
  
def extract_relation_name(r):
  """
  return the name of a relation
  """
  name = re.search(r'\((\w+)',r).group(1)
  return copy.copy(name)
  
#load_file('parsed/BostonPark_601_2.0.txt.tree','../lexico-summed/BostonPark_601_2.0','feng-featured-3/BostonPark_601_2.0')  


def extract_feature_space(folder):
  files = os.listdir(folder)
  features = []
  
  for f in files:
    for line in open(os.path.join(folder,f)):
      features.append(line.strip().split(','))
  
  features = set(itertools.chain(*features))
  features.remove('')
  indexs = range(len(features))
  return dict(zip(features, indexs))

  
def summarize():
  featured = 'feng-featured-6'
  adjacent = '../adjacent/adjacent_1'
  outpath = 'analysis/path/path.1.1'
  fo = open(outpath,'w')
  
  feature_space = extract_feature_space(featured)
  extra = len(feature_space)

  
  for f in os.listdir(featured):
    ff = open(os.path.join(featured,f)).readlines()
    fa = open(os.path.join(adjacent,f)).readlines()
    for i in range(len(ff)):
      entryf = ff[i].strip().split(',')
      entrya = fa[i].split(',')
      
      if entryf == ['']:
	pass
      else:
	entryf = [feature_space.get(item) for item in set(entryf)]
	entryf.sort()
      
	for item in entryf:
	  fo.write('%s 1,'%item)
	fo.write('%s %s,%s %s,%s %s,%s %s'%(extra,(entrya[0]+entrya[1]),(extra+1),(entrya[2]+entrya[3]),(extra+2),entrya[4],(extra+3),entrya[5]))
      
  
  fo.close()
  wekaize(outpath,extra)
  
  
def wekaize(path, size_space):
  fi = open(path)
  fo = open(path+'.arff','w')
  
  fo.write('@RELATION EDU_polarity_prediction\n\n')
  for i in range(size_space):
    fo.write('@ATTRIBUTE feature%s NUMERIC\n'%i)
  fo.write('@ATTRIBUTE feature%s {StartStart,posYes,posNo,negYes,negNo,neuYes,neuNo}\n'%size_space)
  fo.write('@ATTRIBUTE feature%s {EndEnd,posYes,posNo,negYes,negNo,neuYes,neuNo}\n'%(size_space+1))
  fo.write('@ATTRIBUTE feature%s NUMERIC\n'%(size_space+2))
  fo.write('@ATTRIBUTE class {pos,neg,neu}\n\n')
  fo.write('@DATA\n')
  
  for line in fi:
    fo.write('{%s}\n'%line.strip())
  
load()  
summarize()  
