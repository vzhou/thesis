# coding=utf-8
from __future__ import unicode_literals
import itertools
import os
import codecs

inp = 'pdtb_annotated_exp'

def swith(arg):
  if arg == 'Arg1':
    return 'Arg2'
  elif arg == 'Arg2':
    return 'Arg1'

def onefile(f, path):
  out = 'pdtb_featured/explicit'
  fo = open(os.path.join(out,f), 'w')
  #load all the pairs, in order to look up the polarity of the other argument
  index = {}
  f = open(os.path.join(path,f)).readlines()
  for line in f:
    entry = line.strip().split('\t')
    relations = entry[3:]
    if len(relations)!=0:
      for r in relations:
        temp_relation_index = r.split('_')[0]
        index[temp_relation_index] = index.get(temp_relation_index,{})
        index[temp_relation_index][r.split('_')[1]] = entry[2] #(arg1/2: sentiment)

  for line in f:
    entry = line.strip().split('\t')
    relations = entry[3:]
    features = []
    if len(relations)!=0:
      for r in relations:
        items = r.split('_')
        if index.get(items[0]).get(swith(items[1])) != None:
          features.append('%s_%s_%s'%(items[1],items[2],index.get(items('_')[0]).get(swith(items[1]))))
    if len(features)==0:
      features.append('non')
    fo.write('%s,%s\n'%(','.join(features), entry[2]))
  fo.close()

def allfile():
  global inp
  for fil in os.listdir(inp):
    print fil
    onefile(fil, inp)

def summarize():
  sourcep = 'pdtb_featured/explicit'
  lexi = '../lexico-summed'
  adjacecnt = '../adjacent/adjacent_2'
  target = open('lin-gold', 'w')

  for fil in os.listdir(sourcep):
    fs = open(os.path.join(sourcep,fil)).readlines()
    fa = open(os.path.join(adjacecnt,fil)).readlines()
    for i in range(len(fs)):
      temp = []
      temp.extend([fa[i].split(',')[0],fa[i].split(',')[2],fa[i].split(',')[4],fa[i].split(',')[6],fa[i].split(',')[8]])
      temp.extend(fs[i].split(','))
      target.write(','.join(temp))

def wekaize():
  source = open('lin-gold').readlines()
  out = open('lin-gold.arff', 'w')
  allrelation = [line.split(',')[5:-1] for line in source]
  allrelation = set(itertools.chain(*allrelation))
  index = dict(zip(allrelation,range(len(allrelation))))
  total = len(allrelation)+5

  out.write('@RELATION EDU_polarity_prediction\n\n')
  out.write('@ATTRIBUTE prev1 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE prev2 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE next1 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE next2 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE lexi NUMERIC\n')
  for i in range(len(allrelation)):
    out.write('@ATTRIBUTE pdtb%s NUMERIC\n'%i)
  out.write('@ATTRIBUTE class {pos,neg,neu}\n')
  out.write('@DATA\n\n')

  for line in source:
    entry = line.split(',')
    relations = entry[5:-1]
    relationindexs = sorted(list(set([index[r]+5 for r in relations])))
    relationindexs = ['%s %s'%(item,1) for item in relationindexs]
    pdtb = ','.join(relationindexs)
    rest = ['%s %s'%((i,entry[i])) for i in range(5)]
    rest = ','.join(rest)
    out.write('{%s,%s,%s %s}\n'%(rest,pdtb,total,entry[-1].strip()))



summarize()
wekaize()