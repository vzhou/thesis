index = {}
    for line in fic:
      entry = line.strip().split('\t')
      relations = entry[3:]
      if len(relations)!=0:
	for r in relations:
	  temp_relation_index = r.split('_')[0]
	  index[temp_relation_index] = index.get(temp_relation_index,[])
	  index[temp_relation_index].append((r.split('_')[1],entry[1],entry[2])) #(arg1/2, aspect, sentiment)
	  
    i = 0
    for line in fic:
      features = [0,0,0,0]*len(EXP)
      temp_asp = line.split('\t')[1]
      if 'Arg' in line:
	entry = line.strip().split('\t')[3:]
	relations = [e for e in entry if e in EXP]
	for r in relations:
	  r_items = r.split('_')
	  temp_index = r_items[0]
	  temp_arg = r_items[1]
	  temp_name = r_items[2]
	  features[m(temp_name)*4+0] = 1   # set this relation exsit
	  features[m(temp_name)*4+1] = int(temp_arg[3]) #set its arg info
	  features[m(temp_name)*4+2] = 1 #will change to 2 if the aspect changes
	  for v in index.get(temp_index):
	    if v[0] != temp_arg:
	      features[m(temp_name)*4+2] = s(v[2])
	      if not (v[1] in temp_asp or temp_asp in v[1]):
		features[m(temp_name)*4+3] = 2
	      
      fo.write(','.join(map(lambda x: str(x), features)))
      fo.write(',')