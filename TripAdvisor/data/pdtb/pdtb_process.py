import os
import copy
import re


# path of lexicon baseline features and pdtb relations annotated data
lexi = '../lexicon-based'
pdtb_sig = 'pdtb_annotated_sig'
pdtb_exp = 'pdtb_annotated_exp'

SIG = ['Conjunction', 'Contrast', 'Cause', 'Condition', 'Concession']
EXP = ('Conjunction', 'Contrast', 'Cause', 'Condition', 'Concession', 'Synchrony', 'Asynchronous')

def arg2_string():
  """
  This models feature values as string(say, pos) and only for arg2. See pdtb_featured/arg2/readme.txt
  """
  
  global lexi, pdtb_sig
  outputpath = 'pdtb_featured/arg2_s'
  
  for f in os.listdir(pdtb_sig):
    index = {} #key, relation index;value, a tuple (arg1/2, coresponding sentiment) 
    fic = open(os.path.join(pdtb_sig,f)).readlines()
    fo = open(os.path.join(outputpath,f), 'w')
    flc = open(os.path.join(lexi,f)).readlines()
    
    for line in fic:
      entry = line.strip().split('\t')
      relations = entry[3:]
      if len(relations)!=0:
	for r in relations:
	  if r.split('_')[1]=='Arg1':
	    index[r.split('_')[0]] = entry[2]

    i = 0
    for line in fic:
      features = ['No','No','No','No','No']
      if 'Arg2' in line:
	relations = line.strip().split('\t')[3:]
	for r in relations:
	  if 'Arg2' in r:
	    features[m(r.split('_')[2])] = index.get(r.split('_')[0],'No')
      fo.write(','.join(features))
      fo.write(',')
      fo.write(flc[i])
      i+=1
    
    fo.close()

def arg2_numeric():
  """
  similar to arg2_string, but numeric values for discourse relation features, where
  3 for pos, 2 for neu, 1 for neg, 0 for none
  """
  
  string_file = '/home/eastdog/Documents/Thesis/data/pdtb_featured/arg2_s.arff'
  outputpath = 'pdtb_featured/arg2_n.arff'
  
  fsc = open(string_file).read()
  fo = open(outputpath, 'w')
  
  foc = fsc.replace('{pos,neg,neu,No}','NUMERIC')
  foc = foc.replace('pos,','3,')
  foc = foc.replace('neu,','2,')  
  foc = foc.replace('neg,','1,')
  foc = foc.replace('No,','0,')
  
  fo.write(foc)
  fo.close()

def arg12_string():
  global lexi, pdtb_sig
  outputpath = 'pdtb_featured/arg12_s'
  
  for f in os.listdir(pdtb_sig):
    fic = open(os.path.join(pdtb_sig,f)).readlines()
    fo = open(os.path.join(outputpath,f), 'w')
    flc = open(os.path.join(lexi,f)).readlines()
    
    index = {}
    for line in fic:
      entry = line.strip().split('\t')
      relations = entry[3:]
      if len(relations)!=0:
	for r in relations:
	  temp_relation_index = r.split('_')[0]
	  index[temp_relation_index] = index.get(temp_relation_index,[])
	  index[temp_relation_index].append((r.split('_')[1],entry[2]))
	  
    i = 0
    for line in fic:
      features = ['No','No','No','No','No']
      if 'Arg' in line:
	relations = line.strip().split('\t')[3:]
	for r in relations:
	  conflict = 0
	  r_items = r.split('_')
	  for v in index.get(r_items[0]):
	    if v[0]!=r_items[1]:
	      conflict += 1
	      features[m(r_items[2])] = v[1]
	    if conflict > 1:
	      features[m(r_items[2])] = 'neu'
	      
      fo.write(','.join(features))
      fo.write(',')
      fo.write(flc[i])
      i+=1
    
    fo.close()

def arg12_numeric():
  
  string_file = '/home/eastdog/Documents/Thesis/data/pdtb_featured/arg12_s.arff'
  outputpath = 'pdtb_featured/arg12_n.arff'
  
  fsc = open(string_file).read()
  fo = open(outputpath, 'w')
  
  foc = fsc.replace('{pos,neg,neu,No}','NUMERIC')
  foc = foc.replace('pos,','3,')
  foc = foc.replace('neu,','2,')  
  foc = foc.replace('neg,','1,')
  foc = foc.replace('No,','0,') 
  
  fo.write(foc)
  fo.close()

  
  
def arg2_numeric2():
  """
  Similar to previous numeric idea, but model numbers as following,
  pos == 1, neg == -1, neu == No ==0
  
  """
  string_file = '/home/eastdog/Documents/Thesis/data/pdtb_featured/arg2_s.arff'
  outputpath = 'pdtb_featured/arg2_n2.arff'
  
  fsc = open(string_file).read()
  fo = open(outputpath, 'w')
  
  foc = fsc.replace('{pos,neg,neu,No}','NUMERIC')
  foc = foc.replace('pos,','1,')
  foc = foc.replace('neu,','0,')  
  foc = foc.replace('neg,','-1,')
  foc = foc.replace('No,','0,') 
  
  fo.write(foc)
  fo.close()
  
def arg12_string_recent():
  """
  consider only ONE most recent relation, instead of all concerning relations
  """
  global lexi, pdtb_sig
  outputpath = 'pdtb_featured/arg12_s_r'
  
  for f in os.listdir(pdtb_sig):
    fic = open(os.path.join(pdtb_sig,f)).readlines()
    fo = open(os.path.join(outputpath,f), 'w')
    flc = open(os.path.join(lexi,f)).readlines()
    
    index = {} #key, relation index;value, a tuple (arg1/2, coresponding sentiment) 
    for line in fic:
      entry = line.strip().split('\t')
      relations = entry[3:]
      if len(relations)!=0:
	for r in relations:
	  temp_relation_index = r.split('_')[0]
	  index[temp_relation_index] = index.get(temp_relation_index,[])
	  index[temp_relation_index].append((r.split('_')[1],entry[2]))
    
    
    i = 0
    for line in fic:
      features = ['No','No','No','No','No']
      if 'Arg' in line:
	relation = line.strip().split('\t')[3]
	conflict = 0
	r_items = relation.split('_')
	for v in index.get(r_items[0]):
	  if v[0]!=r_items[1]:
	    conflict += 1
	    features[m(r_items[2])] = v[1]
	  if conflict > 1:
	    features[m(r_items[2])] = 'neu'
	      
      fo.write(','.join(features))
      fo.write(',')
      fo.write(flc[i])
      i+=1
    
    fo.close()
  

def all_exp():
  """
  model all seven exp relations as following,
  four features for each relation: relation existance, arg, polarity of the other arg, whether aspect shift from the other
  """
  
  
  global lexi, pdtb_exp, EXP
  outputpath = 'pdtb_featured/all_exp'
  
  
  for f in os.listdir(pdtb_exp):
    print f
    fic = open(os.path.join(pdtb_exp,f)).readlines()
    fo = open(os.path.join(outputpath,f), 'w')
    flc = open(os.path.join(lexi,f)).readlines()
    
    index = {}
    for line in fic:
      entry = line.strip().split('\t')
      relations = entry[3:]
      if len(relations)!=0:
	for r in relations:
	  temp_relation_index = r.split('_')[0]
	  index[temp_relation_index] = index.get(temp_relation_index,[])
	  index[temp_relation_index].append((r.split('_')[1],entry[1],entry[2])) #(arg1/2, aspect, sentiment)
	  
    i = 0
    for line in fic:
      features = [0,0,0,0]*(len(EXP)+1)
      temp_asp = line.split('\t')[1]
      if 'Arg' in line:
	relations = line.strip().split('\t')[3:]
	for r in relations:
	  r_items = r.split('_')
	  temp_index = r_items[0]
	  temp_arg = r_items[1]
	  temp_name = r_items[2]
	  features[m(temp_name)*4+0] = 1   # set this relation exsit
	  features[m(temp_name)*4+1] = a(temp_arg[3]) #set its arg info
	  features[m(temp_name)*4+2] = -1 #will change to 1 if the aspect changes
	  for v in index.get(temp_index):
	    if v[0] != temp_arg:
	      features[m(temp_name)*4+2] = s(v[2]) #the coresponding sentiment
	      if not (v[1] in temp_asp or temp_asp in v[1]):
		features[m(temp_name)*4+3] = 1 #aspect changes
	      
      features = features[:-4]
      fo.write(','.join(map(lambda x: str(x), features)))
      fo.write(',')
      fo.write(flc[i])
      i+=1
    
    fo.close()

def a(str_arg):
  if str_arg == '1':
    return 1
  if str_arg == '2':
    return -1
    
    
def s(str_sentiment):
  if str_sentiment == 'pos':
    return 5
  if str_sentiment == 'neu':
    return 0
  if str_sentiment == 'neg':
    return -5
    
  
def m(str_relation):
  if str_relation == 'Conjunction':
    return 0
  if str_relation == 'Contrast':
    return 1
  if str_relation == 'Cause':
    return 2
  if str_relation == 'Condition':
    return 3
  if str_relation == 'Concession':
    return 4
  if str_relation == 'Synchrony':
    return 5
  if str_relation == 'Asynchronous':
    return 6
  else:
    return 7

def summarize(model):
  path = 'pdtb_featured/%s'%model
  outputpath = 'pdtb_featured/%s.arff'%model
  if '_s' in model:
    feature_type = '{pos,neg,neu,No}'
  else:
    feature_type = 'NUMERIC'
  
  fo = open(outputpath,'w')
  fo.write('@RELATION EDU_polarity_prediction \n\n')
  fo.write('@ATTRIBUTE Conjunction %s\n'%feature_type)
  fo.write('@ATTRIBUTE Contrast %s\n'%feature_type)
  fo.write('@ATTRIBUTE Cause %s\n'%feature_type)
  fo.write('@ATTRIBUTE Condition %s\n'%feature_type)
  fo.write('@ATTRIBUTE Concession %s\n'%feature_type)
  fo.write('@ATTRIBUTE Synchrony %s\n'%feature_type)
  fo.write('@ATTRIBUTE Asynchronous %s\n'%feature_type)
  fo.write('@ATTRIBUTE afinn_pure NUMERIC\n')
  fo.write('@ATTRIBUTE afinn NUMERIC\n')
  fo.write('@ATTRIBUTE huliu_pure NUMERIC\n')
  fo.write('@ATTRIBUTE huliu NUMERIC\n')
  fo.write('@ATTRIBUTE ofinder_pure NUMERIC\n')
  fo.write('@ATTRIBUTE ofinder NUMERIC\n')
  
  fo.write("@ATTRIBUTE class {pos,neg,neu}\n\n")
  fo.write('@DATA \n')
  
  for f in os.listdir(path):
    if 'readme' in f:
      print 'sb'
    for line in open(os.path.join(path,f)):
      fo.write(','.join(line.split(',')[:34]))
      fo.write(',%s'%line.split(',')[-1])
    
def go():
  #all_exp()
  summarize('all_exp')
  #arg12_numeric()
  #arg12_string()
  #arg12_string_recent()
  
go()