import os


lexi = 'lexicon-based'

def classify():
  global lexi
  correct = 0
  total = 0
  
  for f in os.listdir(lexi):
    contents = open(os.path.join(lexi,f)).readlines()
    total += len(contents)
    for line in contents:
      entry = line.split(',')
      number = sum(map(lambda x: int(x), [entry[0],entry[2],entry[4]]))
      tag = line.split(',')[-1].strip()
      if  (number>0 and tag == 'pos') or (number==0 and tag == 'neu') or (number<0 and tag == 'neg'):
	correct += 1

  print float(correct)/total
	
	
	
	
classify()