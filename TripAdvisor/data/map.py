# coding=utf-8
from __future__ import unicode_literals

import re
import os
import difflib
import copy
import codecs


SIG = ['Conjunction', 'Contrast', 'Cause', 'Condition', 'Concession']

#remove all implite relations in one file
def delete_Imp_file(file, outputfile):
  f = open(file,'r')
  of = open(outputfile,'w')
  of.write(re.sub(r'{?\b[\w_]*Non[\w|_}]+\b}?', '', f.read()))
  
  f.close()
  of.close()
  
def delete_Imp():
  source_dir = 'pdtb_parsed'
  target_dir = 'exp_only'
  
  files = os.listdir(source_dir)
  for f in files:
    delete_Imp_file(os.path.join(source_dir,f), os.path.join(target_dir,f))
    
    
def delete_Exp_NonSig_file(file, outputfile):
  global SIG
  
  f = open(file, 'r')
  content = f.read()
  of = open(outputfile, 'w')
  
  for relation in SIG:
    sig_relation = re.compile(r'Exp_(\d+)_conn_%s'%relation)
    exp_index = sig_relation.findall(content)
    for i in exp_index:
      content = re.sub(r'({?)Exp_%s_([\w]+)\b(}?)'%i, r'\1exp_%s_\2\3'%i, content)
      
  
  content = re.sub(r'{?\bExp_[\w|_}]+\b}?', '', content)
  content = re.sub(r'{?\bAttr_[\w|_}]+\b}?', '', content)
  content = re.sub(' +', ' ', content)
  of.write(content)

def delete_Exp_NonSig():
  source_dir = 'exp_only'
  target_dir = 'exp_sig'
  
  files = os.listdir(source_dir)
  for f in files:
    delete_Exp_NonSig_file(os.path.join(source_dir, f), os.path.join(target_dir, f))
  
def test():
  a = '/home/eastdog/Documents/Thesis/data/labeled/Travelodge_London_296_3.0'
  b = '/home/eastdog/Documents/Thesis/data/exp_sig/Travelodge_London_296_3.0'
  c = '/home/eastdog/Documents/Thesis/data/ptdb_formated/Travelodge_London_296_3.0'
  match_file(a,b,c)

def match_pdtb():
  a = '/home/eastdog/thesis/data/clean_labeled'         #path of annotated folder
  b = '/home/eastdog/thesis/data/pdtb/exp_only'         #path of ptdb parsed files folder
  c = '/home/eastdog/thesis/data/pdtb/pdtb_annotated_exp'   #path of outputfile folder
  print a
  
  files = os.listdir(a)
  for f in files:
    print 'processing ', f
    match_file(os.path.join(a,f), os.path.join(b,f), os.path.join(c,f))
  
def match_file(annotatedfile, parsedfile, outputfile):
  fa = codecs.open(annotatedfile, encoding='utf-8')
  fp = codecs.open(parsedfile, encoding='utf-8')
  fo = codecs.open(outputfile, 'w', encoding='utf-8')
  
  pdtb = fp.read()#.replace('£','pound').replace('$','USD')
  relation_name = re.compile(r'{Exp_(\d+)_conn_(\w+\b) ({?\bExp_[\w_]*\b}?)?([ \'\w]+)Exp_[^}]+}')
  pdtb_relations = relation_name.findall(pdtb)
  pdtb_relation_index = {item[0]:(item[1],item[3]) for item in pdtb_relations}
  pdtb = relation_name.sub(r'\4', pdtb)
  

  
  for line in fa:
    if (not line.split()) or line.startswith('.') or line.startswith('0') or line.startswith('!') or line.startswith('-RRB-') or line.startswith(','):
      pass
    else:
      entry = line.rsplit('\t')
      sentence = entry[0].strip()
      aspect = entry[1].strip()
      polarity = entry[2].strip()
      
      matched = match_EDU(sentence.replace('£','# ').replace('$','# '), pdtb)
      if not matched:
	fo.write('%s\t%s\t%s\n'%(sentence, aspect, polarity))
      else:
	fo.write('%s\t%s\t%s'%(sentence, aspect, polarity))
	for t in matched:
	  fo.write('\t%s_%s'%('_'.join(t), pdtb_relation_index[t[0]][0]))
	fo.write('\n')
  
  fa.close()
  fp.close()
  fo.close()
  
  
def match_EDU(str1, str2):
  """
  This match function compares one annotated sentence/EDU(string) and pdtb-parsed document, 
  return list of tuple(expIndex, argIndex)
  """
  str1 = str1.strip()
  str2 = str2.strip()
  s = difflib.SequenceMatcher(None, str2, str1)
  start2, start1, length = s.find_longest_match(0,len(str2), 0,len(str1))
  tempcan = []
  
  if not (length+16 > len(str1)): #matched length + 16 (where 14 is the longest conn word) > len(target annotated string), considered as match
    if extend(str1[start1:start1+length], str2) != None:
      str2 = extend(str1[start1:start1+length], str2)
      match_EDU(str1, str2)
    else:
      return None
      
  else:
    tempstr = str2[0:start2+length]
    tempentry = tempstr.split()
    i = len(tempentry)
    rightbrackt = []
    while i>0:
      if tempentry[i-1].startswith("{") and not (tempentry[i-1].replace('{','')+'}') in rightbrackt:
	tempcan.append(tempentry[i-1])
      elif tempentry[i-1].startswith("{") and (tempentry[i-1].replace('{','')+'}') in rightbrackt:
	rightbrackt.remove(tempentry[i-1].replace('{','')+'}')
      elif "}" in tempentry[i-1]:
	rightbrackt.append(tempentry[i-1])
      i-=1
      
  return copy.copy(map(lambda x:tuple(x.split('_')[1:]), tempcan))
    
    
  
def extend(matchpart, whole):
  """
Once annotated sentence doesn't exist in parsed data,

extend the matched part in the whole document
by removing most recent relation notation between matched part
"""

  extend = re.compile(r'\{\bExp_(\d+)_\w+.*\%s.*\}'%matchpart)
  if not extend.search(whole):
    return None
  else:
    index = int(extend.search(whole).group(1))
    whole = re.sub(r'{?\bExp_%s[\w_}]+\b}?'%index, '', whole)
    whole = re.sub(' +', ' ', whole)
    return copy.copy(whole)

  
  
#def get_content(somestr):
  #tag = re.compile(r'{?Exp_[\w_]+}?')
  #temp_content = tag.sub('', somestr).strip()
  #temp_content = re.sub(r' +', ' ', temp_content)
  #return copy.copy(temp_content)




#match_pdtb()

match_pdtb()