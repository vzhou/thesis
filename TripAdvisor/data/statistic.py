#! /usr/bin/env python
#coding=utf-8
"""
This script is to make some statistics of parsed texts from pdtb parser,including,
    # #of explicit discourse relations
    # #of non-explicit relations
    # list of exp/non-exp relations
    # coverage of text covered by discourse relations

Codes outside functions are just for IMDb movie reviews corpus,
modification requred for other corpus

"""


import re
import string
import os

non_exp = 0
exp = 0

non_exp_drs = []
exp_drs = []

coverage = 0

pattern_non_exp = re.compile(r'NonExp_\d+_Arg2_(\w+) ')
pattern_exp = re.compile(r'Exp_\d+_conn_(\w+) ')


def static(file):
    global non_exp, exp, non_exp_drs, exp_drs, pattern_non_exp, pattern_exp
   
    f = open(file)
    try:
        c = f.read()
    finally:
        f.close()
    
    non_exp += c.count("{NonExp_")/2
    exp += c.count("{Exp_")/3
    
    non_exp_drs.extend(pattern_non_exp.findall(c))
    exp_drs.extend(pattern_exp.findall(c))
    
    #update_coverage(c)
 
    
def update_coverage(c):
    global coverage
    
    temp = c.replace("\n\n"," ").translate(string.maketrans("",""), string.punctuation.replace("{","").replace("}","")).replace("RRB","").replace("LRB","").rsplit(" ")
        
    br = 0
    undercover = 0
    
    for i in range(len(temp)):
        if "{" in temp[i]:
            br += 1
        elif "}" in temp[i]:
            br -= 1
        elif br > 0:
            undercover += 1
    
    coverage += float(undercover)/len(temp)
        
    
    
    

#dirpath = 'exp_only'
#files = os.listdir(dirpath)
#for f in files:
  #static(os.path.join(dirpath,f))

#exp_relations = set(exp_drs)
#for r in exp_relations:
  #print r, exp_drs.count(r)