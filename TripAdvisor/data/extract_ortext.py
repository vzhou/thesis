import os
import codecs

dirpath = 'labeled'
targetpath = 'orginal_text'

def extract_file(root,filename):
  global targetpath
  labelfile = codecs.open(os.path.join(root,filename))
  targetfile = open(os.path.join(targetpath,filename),'w+')
  
  for line in labelfile:
    if (not line.split()) or line.startswith('0'):
      pass
    else:
      targetfile.write(line.split('\t')[0])
      
  labelfile.close()
  targetfile.close()

def sum_lex():
  to_sum = 'lexicon-based'
  summed = 'lexico-summed'
  
  for fname in os.listdir(to_sum):
    f = open(os.path.join(to_sum, fname))
    fo = open(os.path.join(summed, fname), 'w')
    for line in f:
      entry = line.split(',')
      if len(entry)>8:
	print fname, line
      score = sum(map(lambda x:int(x), [entry[0],entry[2],entry[4]]))
      fo.write('%s,%s'%(score, ','.join(entry[6:])))
    
    f.close()
    fo.close()  
  
  
def extract_all():
  global dirpath
  for ro, dis, fis in os.walk(dirpath):
    for fi in fis:
      print os.path.join(ro,fi)
      extract_file(ro,fi)
  
extract_all()