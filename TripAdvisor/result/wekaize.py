
def wekaize(path):
  f = open(path).readlines()
  fo = open((path+'.arff'), 'w')
  
  size = len(f[0].split(','))
  data = [[] for i in range(size)]
  feature_types = []
  
  for i in range(len(f)):
    entry = f[i].split(',')
    for j in range(size):
      data[j].append(entry[j])
  
  for i in range(size-1):
    try:
      float(f[0].split(',')[i])
      feature_types.append('NUMERIC')
    except:
      feature_types.append('{%s}'%(','.join(set(data[i]))))
  att = ['@ATTRIBUTE feature%s %s'%(i,feature_types[i]) for i in range(size-1)]
   
  
  fo.write('@RELATION EDU_polarity_prediction\n\n')
  fo.write('\n'.join(att))
  fo.write('\n')
  fo.write('@ATTRIBUTE class {pos,neg,neu}\n\n')
  fo.write('@DATA\n')
  fo.write(''.join(f))
  fo.close()
  
      
    
wekaize('baseline/adjacent.3')  