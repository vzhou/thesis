import numpy as np
from sklearn import cross_validation
from sklearn import svm
from sklearn import metrics

def svm_cross():
  X = []
  y = []
  
  f = open('temp.txt')
  for line in f:
    X.append(map(lambda x: int(x), line.split(',')[:6]))
    y.append(line.split(',')[6].strip())
  
  X = np.array(X)
  y = np.array(y)
    
  clf = svm.SVC(kernel='rbf', C=1)  
  scores = cross_validation.cross_val_score(clf, X, y, cv=10, score_func=metrics.f1_score)
  print sum(scores)/10
  
svm_cross()