import numpy as np
import math
import copy
import itertools
from sklearn import cross_validation
from sklearn import svm
from sklearn import tree
from sklearn import metrics
from sklearn.feature_selection import SelectKBest

FILE = 'all_exp'


def feature_select():
  
  X = []
  y = []
  global FILE
  f = open(FILE)
  for line in f:
    X.append(map(lambda x: int(x), line.split(',')[:-1]))
    y.append(line.split(',')[-1].strip())
  
  X = np.array(X)
  y = np.array(y)
  
  selector = SelectKBest()
  selector.fit(X,y)
  print selector.scores_
  

def see_result(model='full'):
  X = []
  y = []
  if model == 'full':
    start = 0
    end = -1
  if model == 'base':
    start = 28
    end = -1
  if model == 'discourse':
    start = 0 
    end = 28
  
  
  global FILE
  f = open(FILE)
  for line in f:
    X.append(map(lambda x: int(x), line.split(',')[start:end]))
    y.append(line.split(',')[-1].strip())
  
  X = np.array(X)
  y = np.array(y)
  

  clf = svm.SVC(kernel='rbf', C=1)
  scores = cross_validation.cross_val_score(clf, X, y, cv=10, score_func=metrics.f1_score)
  print sum(scores)/10

def meta_apply(model='full'):
  if model == 'full':
    features = tuple(range(34))
  if model == 'base':
    features = tuple(range(28,34))
  if model == 'discourse':
    features = tuple(range(28))
  
  X = []
  y = []
  
  output = 'prob_%s'%model
  fo = open(output, 'w')
  global FILE
  f = open(FILE)
  
  for line in f:
    entry = line.split(',')
    X.append(map(lambda x: int(x), [entry[i] for i in range(len(entry)) if i in features]))
    y.append(entry[-1].strip())
    
    
  divide = int(math.ceil(len(X) / float(10)))
  Xc = [X[i:i+divide] for i in range(0, len(X), divide)]
  y = [y[i:i+divide] for i in range(0, len(y), divide)]
  
  accuracy = 0
  for i in range(10):
    training = (cross(i,Xc), cross(i,y))
    testing = (Xc[i],y[i])
    accuracy += meta_classify(training, testing, fo)
  
  print accuracy/10
  fo.close()
  f.close()
  
def meta_classify(train, test, f):  
  poscopy = copy.copy(train)
  negcopy = copy.copy(train)
  correct = 0
  total = len(test[0])
  
  for i in range(len(train[1])):
    if poscopy[1][i] == 'neu':
      poscopy[1][i] = 'neg'
    if negcopy[1][i] == 'neu':
      negcopy[1][i] = 'pos'
  
  clfp = svm.SVC(kernel='rbf', C=1, probability=True)
  clfn = svm.SVC(kernel='rbf', C=1, probability=True)
  clfp.fit(train[0], train[1])
  clfn.fit(train[0], train[1])
  
  for i in range(len(test[0])):
    # format of a line is: correct label \t prob_p/not \t not/prob_n \t meta classified label
    f.write(test[1][i]+'\t')
    #pl = clfp.predict(test[0][1])
    #pn = clfn.predict(test[0][1])
    ppos = clfp.predict_log_proba(test[0][i])
    pneg = clfn.predict_log_proba(test[0][i])
    f.write(str(ppos[0][1]-ppos[0][0])+'\t')
    f.write(str(pneg[0][0]-pneg[0][1])+'\t')
    #f.write('/'.join(map(str, ppos))+'\t')
    #f.write('/'.join(map(str, pneg))+'\t')
    
    classified_label = meta_decide(ppos[0], pneg[0])
    f.write(classified_label+'\n')
    if classified_label == test[1][i]:
      correct += 1
      
    
  return float(correct)/total

  
  
      
def meta_decide(p, n):
  label = ''
  if p[0]<p[1] and n[0]<n[1]:
    label = 'pos'
  elif p[0]>p[1] and n[0]>n[1]:
    label = 'neg'
  elif p[0]>p[1] and n[0]<n[1]:
    label = 'neu'
  elif p[0]<p[1] and n[0]>n[1]:
    label = 'confused'
  
  return label
  
  
  
  
def classify(training1, training2, testing1, testing2):
  clf1 = svm.SVC(kernel='rbf', C=1, probability=True)
  clf2 = svm.SVC(kernel='rbf', C=1, probability=True)
  
  clf1.fit(training1[0], training1[1])
  clf2.fit(training2[0], training2[1])
  
  c11 = c10 = c01 = c00 = 0
  for i in range(len(testing1[0])):
    score1 = clf1.predict(testing1[0][i])
    score2 = clf2.predict(testing2[0][i])
    
    
    r = testing1[1][i]
    if score1 == r and score2 == r:
      c11 += 1
    elif score1 == r and score2 != r:
      c10 += 1
    elif score1 != r and score2 == r:
      c01 += 1
    elif score1 != r and score2 != r:
      c00 += 1
  
  return c11, c10, c01, c00
  
  
def test_sig(model='n6'):
  X1 = []
  X2 = []
  y = []
  
  if model == 'n3':
    features1 = (0,1,2,3,4,5,7,9)
    features2 = (5,7,9)
  elif model == 'n6':
    features1 = (0,1,2,3,4,5,6,7,8,9,10)
    features2 = (5,6,7,8,9,10)
  elif model == '63':
    features1 = (5,6,7,8,9,10)
    features2 = (5,7,9)
  elif model == 'no':
    features1 = (0,1,2,3,4,9,10)
    features2 = (9,10)
  elif model == 'na':
    features1 = (0,1,2,3,4,5,6)
    features2 = (5,6)
  elif model == 'nhl':
    features1 = (0,1,2,3,4,7,8)
    features2 = (7,8)
  elif model == 'fu6':
    features1 = tuple(range(34))
    features2 = tuple(range(28,34))
  elif model == 'fu3':
    temp = range(28)
    temp.extend([28,30,32])
    features1 = tuple(temp)
    features2 = (28,30,32)
  
  global FILE
  f = open(FILE)
  
  for line in f:
    entry = line.split(',')
    X1.append(map(lambda x: int(x), [entry[i] for i in range(len(entry)) if i in features1]))
    X2.append(map(lambda x: int(x), [entry[i] for i in range(len(entry)) if i in features2]))
    y.append(entry[-1].strip())
    
    
  divide = int(math.ceil(len(X1) / float(10)))
  X1c = [X1[i:i+divide] for i in range(0, len(X1), divide)]
  X2c = [X2[i:i+divide] for i in range(0, len(X2), divide)]
  y = [y[i:i+divide] for i in range(0, len(y), divide)]
  
  r11=r10=r01=r00=0
  
  for i in range(10):
    train1 = (cross(i,X1c), cross(i,y))
    train2 = (cross(i,X2c), cross(i,y))
    test1 = (X1c[i],y[i])
    test2 = (X2c[i],y[i])
    tr11, tr10, tr01, tr00 = classify(train1, train2, test1, test2)
    r11 += tr11
    r10 += tr10
    r01 += tr01
    r00 += tr00
  
  
  print r11, r10, r01, r00

def cross(i, l):
  temp =  l[:i]
  temp.extend(l[i+1:])
  return list(itertools.chain(*temp))

def go():
  meta_apply('base')
  #feature_select()
  #test_sig('fu6')
  #see_result('discourse')

  
  
  
  
  
go()  
  
  