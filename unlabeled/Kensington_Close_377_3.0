
The Kensington Close Hotel is a regular venue for my meetings in London 
and it is sufficiently convenient for road and rail 
so delegates can attend without too much trouble . 

Reception was handled efficiently and off I went in search room… .. a long search 
since some of these corridors are very long 
and I feared I was well on my way into the next borough . 
Like a lot of the other reviewers , I have to say that my room was rather pokey and , if the fire evacuation map was true to scale , it was no smaller than most of the other adjacent rooms . 

If you want to sit at the desk 
then you will need to make some difficult decisions about where to put your suitcase 
because there is insufficient room for a chair and a suitcase stand . 

Unfortunately , on all of my visits to the Kensington Close , I have yet to master the heating controls in the room . 

It is not that I am technology phobic 
or that I cannot read instructions 
but that air vent seems to pump out cold air whatever I set as the temperature 
and whether or not I turn the fan to slow or fast . 
The food in the hotel is sufficient ……but running out of beef and pork 
by 8.00pm in the evening is bad planning 
if all you have on offer is a carvery . 

The breakfast is a bit of a mad dash 
and now they have instituted a system of giving us all breakfast tickets 
so that only those paying a full rate can get to the cooked breakfast . 

Lunch was better 
and the chef had found some more beef and some more pork 
so now we only had to make sure we were in the right queue . 
The conference facilities at this place are very good 
and the rooms are contained in their own area 
so you don’t feel obliged to hang around in the corridors 
while you drink your tea and enjoy a warm muffin . 
