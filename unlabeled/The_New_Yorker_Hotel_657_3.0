
We stayed at the New Yorker for a week at the end of October 2009 . 

We were on the 27th floor and were impressed by the modern decor and efficient elevators , 
which were plentiful and fast . 

The lobby is a marvel in itself 
-LRB- maybe I 'm easily pleased -RRB- 
and the location is perhaps the best thing about the place . 
Minuses would firstly be the staff on reception . 

We did not have much interaction with them , just check-in and check-out really , 
and on both occasions , they could 've been better . 
Check-in involved queuing in a very vague line , 
waiting to catch a staff member 's attention rather than be called over . 

And instead of being welcomed , 
the member of staff seemed more concerned with outlining the dos and do n't s of the hotel , 
essentially reading the rulebook to us . 
Our room was quite small , but within NYC norms by all accounts . 

All furnishings and fixtures were impressive , 
as was the bed and TV . 

The shower , however , required extensive adjustment to get any temperature resembling bearable . 

My wife simply gave up on it , 
as it veered from freezing cold to boiling hot with the tiniest movement of the tap . 
We did not eat at the hotel per se 
but we did try the hotel 's Tick Tock Diner . 

While its finish seemed very authentic 
-LRB- we 're big diner fans -RRB- 
, 
the staff were again abrupt and more than a little world-weary and the food varied from ok -LRB- Denver Omelette -RRB- to not good -LRB- pancakes -RRB- to downright awful -LRB- the iced water , admittedly not really the hotel 's fault ! -RRB- . 

We also tried the Skylight Diner nearby 
but neither came close to the Comfort Diner on East 45th St , 
which is well worth the walk . 
Funnily enough , the security staff were about the friendliest employees at the hotel . 

We got some pointers on good places to eat from one lady at the security desk 
and those checking for room keys at the elevators always had a smile . 

The hotel arranged seats for us on a bus to Newark Airport -LRB- €27 each -RRB- 
but fumbled a little in the execution of it , 
seemingly sending one bus to Newark with one couple on it and leaving the rest of us behind 
- apparently they had reached the NJ side of the Lincoln Tunnel 
before turning back 
. 

Then we were nearly sent on the wrong bus -LRB- to JFK -RRB- . 

I really got the impression the staff were a little lazy in this aspect of their job . 
Overall , however , I 'd love to return to the New Yorker . 

We had a very comfortable 7 nights there , 
the cleaning staff were in and out any day we wanted and left our room immaculate , 
showing a pride in their work clearly lacking amongst other -LRB- possibly better-paid -RRB- members of staff , 
the location is , 
as I 've said , just about perfect 
and the facilities seem pretty good . 

One final note 
- we saw on the back of our door that the room rate is €299 to €499 per night 
and I spoke briefly with another guest in the elevator who had paid this much 
. 

Having paid much less than this , 
I would certainly not consider this hotel to be worth such a rate 
and I hope guests are paying significantly less than this in reality. Great 
