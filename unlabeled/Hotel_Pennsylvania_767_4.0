
I think a lot of the reviews of this hotel are based on other NY hotels , or on unreasonable expectations of the city from the glamour they 've seen in guidebooks , tv shows etc . 

It 's not the best hotel ever 
but it 's far from being the worst I 've encountered . 

We were pleased with our stay , especially for the price we paid - much lower than all the other hotels in a similar location . 
Firstly the location . 

You really can not fault the location of this hotel . 

It is above a subway station which takes you to Times Square and Central Park , one block from a subway station with direct transfer to the JFK AirTrain , opposite Pennsylvania Station , opposite Madison Square Garden , opposite a taxi rank , 2 blocks from the Empire State Building , one block from the New York 
- New Jersey PATH train , one block from Macy 's , one block from the main post office , has a mall behind it and it literally has everything NY has to offer on the doorstep 
. 
Check in was a little slow . 

We waited five minutes to be told there were not any rooms available with our specification -LRB- 2 beds etc -RRB- for another hour or so . 

This was not a problem as we were early anyway so we took our luggage to the baggage room in the basement . 

The basement could do with some serious refurbishment 
but the entire hotel is currently being refurbished 
and I doubt a largely unused basement is top of their list of priorities . 

A piece of advice though 
- make sure you have some 1 dollar bills with you at all times 
. 

When we collected our bags we did not have any small bills as we had just arrived in the country 
and the staff in the luggage room were quite rude to us for not leaving a tip . 

All we had were $ 20 and $ 50 bills though which we were not leaving . 

This was an isolated incident though 
- most staff were friendly and helpful 
. 

We then proceeded to check in , with a 10 minute or so queue to get to the front desk . 
We had one of the rooms that had not been refurbished yet . 

The paint on the ceiling was chipped in a few places 
and the window was stiff to open 
but these were the only real problems 
- no horror stories that people have mentioned in other reviews 
. 

The hotel was not busy 
so we probably could have asked to be moved to a refurbished room 
but we were tired 
and I really could not be bothered . 

The room was clean enough , big enough and generally good enough for our needs 
- we were in NY so how long were we honestly going to spend in the room 
? 
There is a snack bar in the hotel which serves drinks , 
ice cream , coffee , breakfasts 
to go , danishes etc 
and we used it quite a lot . 

The food was nice considering they probably buy it in bulk . 

There is a restaurant behind the hotel with direct access from the hotel lobby 
but this was closed for refurbishment . 

It looked really nice 
though so I wish we had been there when it was open . 
For anybody , like my friend , who can not go 24 hours without checking facebook or e-mailing your family 
next to the snack bar there is a business suite with probably 8 computers , a printer and fax machine . 

It was not that expensive to connect to the internet 
so I checked my e-mail and directions to things a couple of times when there . 

It was occasionally full 
but I never had to wait more than 5 minutes for a computer to be available . 
One thing I did really like though in addition to the location 
- along with your swipe cards for your room they give you a little popup map with all the sights , subway connections etc in 
. 

It doubles as a place to store your key 
and we used it quite a lot . 

I 'd probably recommend not using it 
to store your key as if you use it a lot you 'll end up losing the key 
but it 's a nice little map . 
Overall I would say this is a good hotel for the price you pay . 

It is literally half the price of other hotels in the same area or with the same location compared to transportation 
so it was never going to be a luxurious , world-class hotel I will remember forever . 

However I did not mind staying there . 

It would most likely have been better in one of the refurbished rooms so if you do not like the room you 're in then just ask to be moved 
- I 've seen reports on here of people doing that , 
and the hotel has 1700 rooms 
so they 're never without spare rooms 
. 
Overall I 'd say there are definitely better hotels in NY . 

However we were on a strict budget and had quite picky requirements 
-LRB- we needed 2 separate beds and private facilities -RRB- 

so for what we paid , 
in the location it was , 
the hotel probably could not have been better . 

I 'd recommend it to others on a budget 
but if you want a world-class hotel and do not mind paying extra then there are LOTS of hotels in New York 
so have a look around . 

As I 've said in reviews of other hotels 
I think it just depends on your standards 
and how fussy a traveller you are . 

This is an acceptable hotel in this price range 
so I do not see what the fuss is about . 

Yes I am a seasoned traveller and have stayed in some really bad places , 
and camped at places with no facilities in Africa 
but this is nowhere near being the worst place I have encountered . 
I 'm usually on a strict budget when I travel 
-LRB- well ... more like keep the price as low as possible 
while keeping the hotel within acceptable limits -RRB- 
and will consider this hotel again . 

I 'll look around for other places 
if I go to New York again 
but this hotel will be on my list of possibilities . 
