
This hotel has some definite pros and cons , many of which other reviewers mentioned . 

A couple pros : even the smallest room 
-LRB- which we had -RRB- 
is big for New York standards , the staff was quiet but still courteous and helpful , 
and the rates were reasonable . 

Lobby spacious and pretty , with computers . 

-LRB- They had some kind of spa-like aromatherapy thing going on scent-wise , 
which my sister loved , 
but was a little much for me . -RRB- 

We did not try the restaurant or fitness center , 
but both looked perfectly acceptable . 

And we did not venture over to the pool at the other Affinia nearby , 
but that 's a nice option . 
Some cons : slow elevators , lots of noise from renovation -LRB- room next door to us , -RRB- no coffee maker . 

Also , we were told our &quot ; My Affinia&quot ; 
requests were ready and waiting 
-LRB- which they were not , and never did show after repeated requests . -RRB- 


I 'm torn over the location . 

Yes , it 's walkable to Times Square , 30 Rock 
but definitely not a stroll . 

Really convenient to Grand Central and Empire State Building . 

Only close to green line of the subway . 

I guess it depends how close to the ' action&quot ; 
you want to be . 
So we went into this fully aware of those types of details , from tripadvisor , 
but I had high hopes that the Shelburne could exceed , 
because we were using a gift certificate that I won at a charity auction . 

The certificate was issued by the hotel itself , 
and invited the recipients to be &quot ; special guests&quot ; of the Shelburne . 

It was not specific as to what that meant exactly , 
but with a $ 1000 value , it seemed like that could be a really nice weekend . 

Let me just say , 
they were kind enough to tweak the terms right away , 
letting us check in a day earlier than specified , and also letting us add an extra day for a reasonable price . 

But from that point on , our experience was NOT SO HOT . 

From my initial contact with the manager , David , I had asked what would be included in our weekend as &quot ; special guests&quot ; -LRB- their words , not mine . -RRB- 

The manager said that because the room was a $ 500 value 
-LRB- it had two beds -RRB- 

our first 2 days were the full value of the certificate . 

No meals , no extras , no drink at the bar . 

Okay , not what I was expecting to hear , 
but surely a $ 500 room surely would be nice , right ? 

WRONG . 

When we arrived , 
they stuck us in one of the smallest rooms , across from the elevator , right next to a room under contruction . 

-LRB- One morning , we were not so pleasantly roused from sleep by a drill ; the next day , a shop vac . -RRB- 

The bathroom vanity was ridiculously tiny , with no shelf or anything , with only the back of the toilet for keeping your toiletries handy , 
which is not great for three people . 

I realized pretty much right away that this was not a $ 500 room . 

A quick check online put our room at $ 250 
-LRB- with the larger , more suite-like rooms going for $ 369 and up . -RRB- 


And I was REALLY irked 
when I saw online that this room says max 2 people when you are booking , and here they stuck THREE of us in there . 

Our room had no coffee maker , no lovely granite pantry or kitchenette 
-LRB- but we managed to squish some drinks and snacks in around the honor bar items . -RRB- 


Yes , we fit just fine , 
but this was not what was implied by the certificate . 

If it had been just me , or my family , 
I would have complained quite vigorously , 
but I was with friends who did not want to move or make a fuss . 

Compared to other rooms they had experienced in New York -LRB- i.e. 

no room to walk around bed , 
no room for luggage , etc -RRB- this one was pleasant and decently sized 
and they were not inclined to want to move . 

I went along with that to make my friends happy , but I was getting more and more steamed at the &quot ; special guest&quot ; line , 
because no My Affinia amenities ever showed up , even after repeated requests . 

They never delivered the paper as promised . 

Also , part of me was expecting at least a &quot ; 
Hi , how are we doing ? &quot ; 
phonecall , or a welcoming note , but nothing . 

And I kept hoping our My Affinia &quot ; complimentary&quot ; 
kit would show up with the maid at some point . 

Or that internet would be comped . 

Or they would remember THEY had invited us as &quot ; special guests for the weekend&quot ; 
and offer us a drink or some breakfast . 

But they NEVER DID . 

Not even a peep . 

Affinia has a long way to go to reach the level of service that they boast . 

We visit New York frequently 
and Affinia had the chance to win over new customers , 
and they failed , 
actually falling quite flat . 

In my experience , Kimpton properties still win hands down . 

It really felt like the Shelburne shut us away in the closet , despite the promise the weekend offered -LRB- $ 1000 value , my foot . -RRB- 

You do not want to rush back to a place where you are not treated fairly . 
