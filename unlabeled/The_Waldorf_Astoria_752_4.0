
My wife and I stayed at the Waldorf-Astoria for two nights in late January 2009 . 

We made our reservation through American Express for a room listed as “1 King Bed Concierge Level” . 

The Waldorf had always been a hotel I wished to stay in 
so I was excited about the visit . 
The hotel is truly magnificent . 

Staying there really takes one back to a time when a visit to a hotel was an EVENT , not something we do simply as a necessity . 

I was taken thinking of how my parents or grandparents would have felt as a guest there , 
and how much the hotel has changed in the past century , 
but how little -LRB- overall -RRB- the Waldorf had managed to retain the grand charm of days long past . 

If for no other reason than connecting with history , 
staying in the Waldorf is an experience one must do at some point in their life . 
The room we were given was in the Waldorf Towers 
and we were very pleased with the room overall . 

The size was exceptionally large for a New York hotel room . 

In addition to the main bedroom , there was an entry foyer , a kitchenette area , a dressing area , and a very large bathroom . 

The fixtures were a nice blend of old and modern , from the trio of closets to the multi-jetted shower . 

The condition of the room was excellent 
and all the linens and amenities were of superior levels . 

The view was a decent view down Park Avenue but nothing postcard worthy . 
The Concierge Level lounge provided a very nice continental breakfast each morning in a private room . 

The quality of the food and the selection were excellent , 
even if you had only a moment to grab a coffee to go . 

The staff in this area did a nice job of helping with most all requests . 

The area is small though , 
so don’t be shocked 
if you have to wait a few moments for a table . 
The main amenities of the hotel are all superior . 

The fitness facility is very modern and well thought out , 
even providing guests with fitness gear in case you have left yours at home . 

The concierge is most helpful , 
providing good information and reviews on local restaurants . 

The shops and business center are well done and very convenient . 

And the location of the hotel is hard to beat . 
My main complaint with the hotel is one of attitude towards guests who are younger than , say , 50 or 60 years . 

Although they were always polite , 
the staff seemed to be uncomfortable dealing with my wife and I 
-LRB- both in our 30’s but looking much more like two people in their 20’s -RRB- 
. 

From the front desk to the bell attendants , there seemed to be this underlying sentiment of “are you sure you are old enough to be staying here ? ” 

whenever we dealt with staff members . 

Given that we are polite , courteous and professional people , it caught us very off guard . 

Overall , this is a fine hotel and well worth a visit . 

I would recommend it to anyone , especially those over the age of 40 . 
