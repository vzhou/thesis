
My spouse and I stayed at the Waldorf-Astoria for one night in mid-November 2010 . 

We live in Pennsylvania , 
so we often spend a night in the city 
so that we can enjoy dinner or a show . 

This is our second time staying at this property 
-LRB- the first was several years ago -RRB- 
. 

As others have said , 
it was once a great hotel that now requires some updating . 

It still retains a prestigious name , 
but its service level leaves something to be desired . 

My spouse is a Hilton gold member -LRB- only one night until Platinum status -RRB- , 
but we didn’t feel that we received the same perks that we do at other Hilton properties . 

Namely , there is no dedicated check-in line for anything below Platinum level 
-LRB- but at that level , there’s a room , not just a desk -RRB- 
. 

We arrived around 12:00 noon on a busy Saturday afternoon , 
and spent about 30 minutes in line for check-in . 

The property could use some velvet rope dividers in the lobby � '' there are so many check-in desks , 
yet everyone is supposed to know to queue up at the center one 
and then disperse accordingly . 

After checking-in , we were told that our room was not ready , 
but to supply a phone number which they would call 
if the room was ready prior to 3:00 pm 
-LRB- it wasn’t , 
so we didn’t receive a call -RRB- 
. 

We were told to check our overnight bag with the bellman , 
which we did , 
and that upon returning to the hotel at check-in , 
we would just need to request our key , 
because we had already given our credit card information and been assigned a room number . 

When we returned hours later , 
again there was an enormous line for checking in . 

There was no indication that we would be able to bypass that long line , 
even though it was our second time standing in it . 

After speaking with the concierge , 
he directed us , along with half a dozen other people , to one of the leftmost reception lines for the abbreviated check-in process . 

But when each of us reached the desk , 
the clerk asked us each to go back to the main line . 

Very confusing � '' it seems that no one knows what the other is doing in the lobby . 

Even though there were two of us , 
one could not fetch the overnight bag 
while the other checked in , 
because the bellman wouldn’t release the bag 
without knowing our room number , 
and we couldn’t ascertain the room number 
without waiting to check in a second time . 

This situation must occur frequently at the hotel , 
yet they’ve made no provisions to ease the process . 

Our room was fine � '' small but in decent shape at first glance . 

The air conditioning did not work , however , 
and the windows could be cracked opened other than 
by calling the maintenance department 
-LRB- it was a very warm fall weekend -RRB- 
. 

We suffered in the uncomfortable temperature until about 10:00 pm , 
then finally called the front desk . 

We were told that the hotel was old and on a one-pipe system , 
meaning no circulating air was available other than the humid , warm air from the outside . 

The maintenance man could only open one of our two windows , 
and the one that did open could only be raised by about an inch . 

So it was a rather unpleasant night temperature-wise . 

Because we realize that is an old hotel , 
we tried to overlook the heating and cooling problem . 

Our bigger problem was the sound and vibration that came from the main ballroom until 12:00 midnight . 

I realize that it’s the “city that never sleeps” , 
but we wanted to ! 

The vibration from the music was more of a problem more than the volume � '' hard to understand , 
because what else could have been causing the vibrations and pulsations than the music ? 

We were assured by the front desk that the music would stop by 11:00 pm , 
but our room vibrated until at least midnight , 
which was a little late for us . 

Needless to say , 
upon check-out the next day , no one asked how our stay was 
-LRB- I guess our call to the front desk the night before was noted on our record , 
and the clerk knew better than to ask -RRB- 
. 

As Gold members , we were given vouchers for the Starbucks continental breakfast -LRB- pastry and drink for each person -RRB- , 
but the line was so unreasonably long , 
that we didn’t even bother . 

Everyone was waiting for coffee to brew ! 
It’s a coffee shop ! 

On a Sunday morning ! 

I think that this Starbucks outlet is overloaded � '' I’m sure it was not built with intention of feeding half the hotel guests . 

As someone else said , 
we expected more from the Waldorf-Astoria brand in terms of its treatment of Hilton Hhonors members . 

There is no lounge access , no upgrades . 

The lobby , while beautiful , is usurped by the outrageously-priced $ 95 per adult Sunday breakfast buffet . 

As another poster said , in this enormous hotel , is there no other place to do this but somewhere that is overrun with people checking in and out , and the germs that we bring in on our shoes and luggage ? 

It seems like a prime spot for a health-code violation ! 

I can’t imagine paying over $ 200 for two adults to eat in the lobby , with guests and visitors traipsing in and out � '' I can’t believe anyone actually pays for it at all ! 

We also got a voucher for two free drinks from any one of the bars � '' not a bad deal 
because they had a value of $ 20 each 
-LRB- and guess how much a martini costs : $ 20 -RRB- 
! 
We won’t stay here again � '' there are many other high-end properties in the city from which to choose , as well as many preferable Hilton-associated hotels . 

And if we thought that our problems ended at check-out � '' not so ! 

When we arrived home and checked our credit card statement , 
there was an additional $ 26.13 charge that we didn’t make . 

It is still pending an investigation by American Express . 

If it was our charge , why wasn’t it part of the final bill that we signed 
when we checked out ? 

The bottled water that we removed from the weighted electronic mini-bar two minutes before our departure was included in our final tally 
-LRB- and then removed 
because we were gold members -RRB- 
, 
so what could this charge possibly have been ? 

I know that it’s a small amount , 
but I shouldn’t have to pay for something that I didn’t buy or use , 
and they shouldn’t charge me after the fact . 
