
Our experience at the Waldorf was simply out of this world . 

I am a Hilton Hhonors Diamond level frequent sleeper and made our reservation 11 months in advance redeeming points for a free nights stay . 

Then a week before we arrived I wrote the general manager a snail mail letter advising him we would be staying there on our wedding night . 

The results were astonishing . 
I received a call two days before our arrival from someone at the Waldorf telling me that we had been upgraded to a luxury suite and inquiring our estimated arrival time . 

When we did arrive we were driven by a car service and were directed from the front of the building on Park Ave. to the Towers entrance on 50th St. . 

There a bellman took our luggage and gave me a chit 
as we then went to the top of Rockefeller Center for sunset . 
When we arrived back at the hotel to check in there must have been 14 available desk clerks able to assist us . 

The one we approached noted on his computer that it was our wedding night and asked if we liked champagne . 

The form for redeeming the room with points was in my luggage , 
and the clerk told us not to worry . 

I exchanged the luggage chit for the room key 
and off we went . 
Our room , 14H , was a corner suite . 

There was a marble foyer with a wet bar ; 
a sitting room with sofa , chairs , fireplace 
and built in bookcases -LRB- with books -RRB- ; a bedroom with a four poster king size bed and a walk in closet with two robes ; 
a dressing room with a make up table , fainting sofa and a five foot by seven foot floor standing mirror ; 
and a marble tiled bathroom with his and her sinks , a shower in a tub 
AND a shower stall -LRB- with five nozzles -RRB- 
and the toilet ~in~ a closet . 

The windows inthe various rooms looked out on Park Ave except for one in the living room which overlooked 49th St. and down Park Ave. to the Met Life building . 
Just a few minutes after we arrived there was a buzz at the doorbell -LRB- -RRB- ! and a bellman arrived with our luggage . 

He carried all the bags into the dressing room where he unfolded the luggage stands and set our suitcases on top of them . 

When I could not find my jacket with my wallet to tip 
him he waved &quot ; no thanks&quot ; 
and let himself out of the door . 
On a table in the living room was a tray with an ice bucket and a bottle of Waldorf-Astoria Champagne , a plate of chocolate dipped strawberries , two flutes and a note from the general manager thanking us -LRB- -RRB- ! for choosing to spend our wedding night with them . 

We sat on the sofa and sipped our champagne , ate the berries and relaxed our first night as husband and wife . 

Just as we were finishing the doorbell rang again and another -LRB- -RRB- ! bottle of champagne and plate of strawberries were delievered . 

The note on this tray was from the clerk who checked us in. I ordered a room service breakfast , 
hung the card on the door , 
and we put the second bottle and berries in the fridge in the wet bar for the morning . 

The bed was wonderfully comfortable . 

In the morning our breakfast was delivered at the exact time we had checked on the form , 
and though I laughed a lot at the $ 73 charge for six muffins , two glasses of juice and a pot of coffee , 
it was worth every penny . 

The cart was wheeled in with a linen tablecloth and napkins . 

Two folding leaves easily opened up 
to make it a nice size table . 

The butter was in an urn sitting atop a pile of ice to keep it chilled . 

The muffins melted in my mouth 
and the croissants so buttery they did not need any more . 

There was real cream for the coffee , 
which was rich and dark . 

The silverwear was heavy 
and the plates monogramed with the distinctive Waldorf-Astoria logo . 
We enjoyed a leisurley breakfast , 
and then opened the other bottle of champagne and ate the strawberries in a hobbittlike second breakfast . 

I called and inquired about a late check out , 
but was told none was available . 

We did not vacate the room until about 12:30 , 
which was still fine with us and no on had any issue with that at all . 
As we were leaving the room 
I opened the card that came with the second bottle of champagne . 

In it was a coupon for breakfast for two at Oscar 's , one of the restaurants on site . 

When we were checking out in the lobby 
I mentioned to the clerk that we had not used it 
because we 'd ordered room service 
and he converted it to lunch for two . 
We gave our bags to a bellman and walked around the city a bit 
before heading to lunch . 

There was no problem using the coupon , 
which included both an appetizer and an entree . 

We , of course , had to try a Waldorf salad . 

We went to retrieve our bags in the bowels of the building and thought we would have to then roll them out front to Park Ave where the car to take us to the airport was supposed to meet us . 

The bellman asked us our name 
and who the car service was , told us to have a seat 
and he called the service , the car materialized in the undergound passageway , 
the bags were loaded in the trunk 
and the rear door was held open for my wife . 

What incomparable service ! 
I know our experience is not the norm , 
but we did not want the norm when we planned to spend our wedding night at the Waldorf-Astoria . 

Every person we encountered there , from bellmen to desk clerks to the bartender in the restaurant was polite to a fault , gracious and warm . 

I had high hopes for a memorable stay when I booked the Waldorf , 
and they were exceeded beyond my wildest dreams . 
