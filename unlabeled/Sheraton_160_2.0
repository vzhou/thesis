
This hotel was undergoing renovation 
when we stayed there , 
although the website did not mention this . 

Our room was on the 11th floor , very close to the elevators , and next to some kind of utility room , 
which was under construction . 

Our window was blocked 
by scaffolding , 
and we were both startled and taken aback to find a workman standing on that scaffolding , 
working directly outside our room . 

This tended to interfere with our view and with our privacy while in the room , 
so we kept the drapes closed most of the time . 
The drapes were stained , 
and the carpet had large dark stains as well . 

The room was very small , even by NYC standards , 
and at more than $ 500 a day , it was not inexpensive . 

The bathroom was not clean : 
the toilet seat was dirty , 
and the fixtures had fingerprints and water spots . 

We complained . 

A desk clerk explained that we were in an &quot ; unrenovated&quot ; room , 
but we could get an upgrade to a renovated room 
if we paid an extra $ 40 and became Starwood members . 

However , this room would not be available until later that day . 

We told the clerk we 'd deal with the room issues later , 
as we needed to leave for a pressing appointment . 

We wanted to think about it : 
we did not like our accommodations at all , 
but were offended that we had to pay extra 
to upgrade from an already-expensive room . 
We did not return until well after midnight , too tired to deal with switching rooms . 

This is unfortunate , 
because we could hear the noisy elevator whistling in the elevator shaft and dinging loudly throughout the night . 

Construction began at 8:00 a.m. with loud banging on the scaffolding . 

About an hour or so later , a workman began using a saws-all or similar loud power tool in the utility room next to our room . 

My husband and I had to yell 
to hear each other . 

&quot ; Let 's get out of here ! &quot ; 

we yelled , packed as fast as we could , 
and left . 

And of course , 
although we never did move to the upgraded room , 
we had nevertheless been charged the $ 40.We complained again . 

This time , the desk clerk was very helpful . 

She removed the upgrade charge , reduced the overall charge by about 20 % , and threw in some extra Starwood points . 

I do not have a problem with that employee 
- I think she did everything she had discretion to do 
. 

I do have a problem with the Sheraton , however . 

I think the Sheraton deliberately provided a compromised and inadequate room at market rates , apparently on the assumption that it does not need to provide accommodations commensurate with their significant cost unless the guests complain . 
