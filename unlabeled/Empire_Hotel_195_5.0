
The Empire Hotel is a refurbished classic with 11 floors , just across the street from Lincoln Center and very close to Central Park . 

It is timelessly stylish . 

The front desk is on one side of an oblong waiting room 
and flanked at each end by a large container full of fresh red apples . 

The cocktail lounge opens onto the same waiting room . 

The concierge was always staffed by courteous and informed , multilingual people , 
one of whom got me to a florist for an orchid corsage , provided a map , got me a complimentary New York Times , recommended a restaurant with outstanding lox and bagels , made a reservation at a jazz club , and deftly pocketed the tip I gave her for her invaluable assistance . 

Its proficient and courteous staff pressed my suits and shirts promptly , kept the room immaculate , and were unflappable when i was not ready for housekeeping before 9 a.m. . 

The rooms tend toward exotic decor , with animal stripes on pillows and chairs , and white , tan and brown colors . 

They have high ceilings , commendably comfortable beds , and a flat-screen 32&quot ; 
television , wireless internet service , a comprehensively stocked honor bar 
-LRB- sensuous oils , scented candles , imported wines , an umbrella , and so on -RRB- 
and access to a free fitness center . 

The bathroom was the most unusual feature . 

There was a kind of funky feng shui shower with smooth wooden boards on the bottom , separated to let the water run down between them . 

There was only a partial glass 
- no shower curtain 
. 

I found my card had a lower out of town limit 
and they accommodated me 
promptly as I arranged for a higher one . 

I was unaccountably charged for parking 
-LRB- since I had not brought a car -RRB- 
but they removed the charge without question 
only to have it recur 
when I got back , as a separate charge against the card . 

I called the accounting office 
and the charge was removed , this time for good . 

This was the only problem I encountered in a stay of four nights . 

Everyone we dealt with was courteous and professional . 
