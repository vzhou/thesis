
We stayed in the hotel for four nights in December . 

Prior to arriving we requested a room with a view and a bottle of wine for check in . 

Unfortunately the check out was not as efficient as it could have been . 

Although we arrived at midday 
-LRB- and the hotel states that they cannot guarantee check in until 3 pm -RRB- we had to wait until at least 4 pm as we were told our requested room was being cleaned . 

We were happy to accept this delay as we had been quite specific about wanting a room with a view , 
but was disappointed to find that the view was pretty limited , 
overlooking the air con units . 

However we could see the main street 
and this may have been as good as it gets for a standard room in NYC . 

The room itself was spacious and clean . 

The heating was straightforward to use , 
and this had to be put on ' high ' 
as soon as we got into the room . 

Obviously , the mini bar and the bar are a little pricey 
but we expected this and 
as we were in NYC for such a short time we wanted to eat and drink out as much as possible , 
so we did not use any of the hotel facilities . 

The rooms were quiet 
and communal areas were clean which was impressive given the amount of guests they had at this time of year . 

Would I book this hotel agin ? 

Definitely . 

Was there a hotel we passed that seemed better value for money , better location , with the wow factor of the Waldorf ? 

Not that we saw . 
We debated for ages 
before booking due to some of the reviews on trip advisor , 
but we were not disappointed . 

We went there with the view that we would only be in the room to sleep , 
but we could have comfortably spent more time in the hotel 
if we had the time . 

The location was ideal . 

A short walk from 5th Avenue and about 20 minutes walk from time square . 

We felt safe walking back to the hotel in the night 
as the main streets were well lit and busy . 

We even walked to central park from the hotel -LRB- about 30 minutes -RRB- 
and grand central terminal was just around the corner . 
Sightseeing : If this is your first time to NYC , plan ahead . 

This was our first trip 
and we were told to be prepared that we would not see everything that we wanted . 

We did . 

With a bit of planning it is possible to hit all the main sights . 

To give you an idea our itinerary was : Day 1 : Subway to Battery Park . 

Go to Grand central terminal help desk and get a subway map and all day travel pass 
-LRB- cost about £10 -RRB- 
. 

The subway is colour coordinated so pretty straight forward to use . 

From battery park 
-LRB- we bought breakfast from a street vendor : very nice -RRB- 
we went to trinity church , 
and then back to the park to board the sightseeing statue cruise to Statue of Liberty Island and Ellis Island . 

I would recommend both . 

Book these tickets online 
before you go to beat the cruise . 

Leave half a day to tour both islands 
-LRB- Ellis took us the longest about 90mins -RRB- 
. 

Back on land , head for financial district -LRB- wall street etc -RRB- 
and then to WTC . 

Make sure you go into St Pauls Chapel directly opposite the site 
as it is filled with memorabilia of the rescue efforts . 

There is a tribute centre nearby , and a developmental centre opposite the church to end on a more uplifting note . 

We then got subway to china town for dinner , 
and then back to hotel by about 7pm . 
Day 2 : We walked to Central Park and had breakfast in a small diner nearby . 

We then walked to strawberry fields and some other sights nearby , 
but we did not go further up . 

Too cold and not enough interests further north . 

We then headed back towards 5th Avenue and walked the length of this down to Times Square . 

Fantastic Pizzeria 's around here . 

Keep walking 
to reach Flat Iron 
-LRB- that was for my husband I was not too interested -RRB- 
. 

We broke up the walking with shopping , 
and it took us the best part of the day . 

After a couple of hours rest in the hotel we went to the Top of the Rock at about 7pm . 

Again pre booked online saved the queues . 

Fantastic views of manhattan . 

Day 3 : We got on the subway to Brooklyn and walked the Brooklyn Promenade to take beautiful photos of Manhattan skyline . 

You can walk Brooklyn Bridge , 
but at this point we had enough walking ! 

We walked a few streets in Brooklyn to look at the beautiful houses . 

Again we felt perfectly safe . 

We got on the subway and headed back towards times square 
and we went to the Empire State building to have a view of Manhatten by day . 

Again fantastic views but cold . 

We also did a lot of shopping this day : 
$ Other recommendations would be to visit Juniors Cheesecakes in Central Terminal 
-LRB- I thought the cheesecake here beat the Magnolia Bakery cupcakes that are often raved about -RRB- 
, Central Library , The Oyster Bar -LRB- a pint and 6 oysters for $ 12- unusual but nice -RRB- , Famous Dave 's BBQ -LRB- the ribs and shrimp were fantastic - and cheap . 

Do not have starters ... 
trust me , 
you will not fit it all in . 

We went twice it was that good -RRB- , Radio City Music Hall , and a Karoake Bar in Times Square -LRB- not sure of the name , but entertaining . 

Do not expect the standard to be similar to UK Karaoke , 
it was full of Beyonce type singers -RRB- . 

We managed to squeeze all that in within our trip , 
and still have time for cat nap before going out in the night . 

Obviously we did not visit everything -LRB- museums etc -RRB- 
but we managed to visit all the main sights that have been recommended on trip advisor and fitted our interests -RRB- . 

So if you organised and wearing good walking shoes 
it can be done . 

We just need to book another holiday to rest now ! 
