
We found the New Yorker to be good value and conveniently located in mid-town . 

The building dates back to the 1920s and has plenty of period character . 

I must say that the sash windows made a nice alternative to the air conditioning on the cooler days that we were in NYC . 

The rooms were compact , 
but the beds were comfortable . 

I did find that the noise from slamming doors in the corridor interrupted my sleep a little . 

Given the small size of the room , there was no escaping how close you are to the hallway ! 

The bathroom looked in need of some updating , 
but it was clean and had everything we needed . 

All in all , we were happy with our decision to stay at the New Yorker . 
The hotel is located right next to a subway station 
and you should not have any difficulties hailing a cab in the area . 

Times Square and Empire State are within easy walking distance , plus Madison Square Garden 
and the shops on 34th Street are just a stones throw away . 

As with any big city , you should keep your wits about you 
when walking around , 
especially if you cut through the side streets near the hotel , 
where we saw some shady looking characters . 

No-one bothered us though . 
The Tick-Tock diner attached to the hotel -LRB- open 24 hours -RRB- was good for breakfast , 
but it gets busy during peak times . 

Do not bother going there for dinner however , 
we found the food to be a little disappointing , 
which is a shame as we enjoyed the breakfast . 

For a really good breakfast , visit the Brooklyn Diner on 57th Street -LRB- between 7th and 8th -RRB- , 
then take a walk in the park to burn it off ! 
In summary , if you are looking for a good value hotel in mid-town , 
you should consider the New Yorker . 
