
I 've been intrigued by the marketing and business model for this hotel for quite some time . 

It 's an old apartment building or hotel that rebranded itself 
to serve a different class of customer . 

I study and write about this sort of thing , 
so it was right up my alley , 
and I was kind of looking for an excuse to stay here . 

When a client needed me to go to NYC for a night , 
but it was not clear whether I or they would be paying for the trip , 
I took advantage of the chance to economize . 
Here 's what it boils down to . 

Do you go to a gym ? 

Is it a nice gym ? 

Imagine if that gym then suddenly decided it were a hotel . 

The smell , the thin carpet , the shared bathroom .. . 

It all reminded me of my gym back home . 
The room I stayed in was tiny - -about 6-feet by 8-feet . 

It had a bunk bed . 

-LRB- Ironically , this is just the size of Bernie Madoff 's prison cell at the Manhattan Federal Detention Center . 

That also had a bunk bed . 

But at least Bernie had a -LRB- semi- -RRB- private toilet ! -RRB- I did not realize when I booked the room that I 'd be sharing the bathroom .. . 
My bad , I guess . 

But I think their website should say very prominently when you make the reservation , &quot ; 
This room has no bathroom ! &quot ; 

The shared bathroom itself seemed nice , 
but there 's no getting away from the fact you 're sharing it with tons of strangers . 

And , I 'm no prima donna . 

I 've been to war , 
and thus shared much nastier bathrooms -LRB- and bathrooms-in-name-only -RRB- with a lot of people . 

But still ... Anyway , the furnishings , the smell of sweat and stank , the tiny little sink , even the extreme smell of bleach on the towels 
- -it all reminded me of the locker room at the local Sports Club 
. 
I could go on , 
but here 's the thing : 
I was in NYC on business , 
and if it were not for my previous curiosity about this place , 
I might well have said , &quot ; nuh-uh , &quot ; 
and gone down the street to spend 3x as much at the Waldorf Astoria . 

So , I 'm probably not really the kind of person they 're trying to attract . 

If you 're visiting NYC on a budget , 
by all means , consider it . 

I met some very nice people here , none of them from the USA - -Japan , Australia , and Sweden . 

And I was not even trying to meet people ! 
The staff was nice . 

The location is good . 

The air conditioning did not work at all , 
but what do you expect for 1 the price of any other nearby hotel ? 
If I were 22 again and backpacking through Europe 
-LRB- or whatever they call the opposite of that , 
when 22-year-old Europeans travel through the USA -RRB- 
, I 'd be all over it . 

If I were an adventurous family on a budget - -well , maybe for a night or two . 
Anyone else , you 're better off at a lot of other places in NYC that do not cost much more . 
