
This is a legendary hotel and a prestigious name , 
and that will be sufficient for many visitors . 

Yet , one 's actual experience as a guest can be variable . 
I attempted to check in around noon on a weekday , 
having arrived by an early flight , 
and the clerk was polite but firm . 

Check-in time was 3 p.m. 
and I would have to come back later . 

In my experience , a responsible hotel will nevertheless check the system in such situations 
to see to see whether a room is available . 

She was unwilling to do so . 

I said I would return later and in the interim leave my bags in the hospitality suite in the Waldorf Towers section of the hotel . 

I was staying there for business meetings , 
and the suite was already open . 

The Towers section is normally accessible from the lobby , 
but when I approached it , 
I discovered I needed a pass key , 
so I had to stand in line again at the reception desk 
to get one . 

This time , I ended up with a male clerk who immediately said : &quot ; 
You can certainly have a key to the Towers 
but first let me check 
if a room is available.&quot ; It 

turned out that a room was , in fact , available , 
so I checked in . 

I felt that the luck of the draw was at work here . 

The first time I arrived at the check-in counter , I received less than acceptable attention . 

The second time I received the kind of attentive treatment one would expect from a hotel of this reputation . 
The gorgeously designed lobby has been a familiar New York landmark for generations , 
and it is always a pleasure to be greeted by its warmth and atmosphere , 
but the rooms do not necessarily live up to your initial ground-floor impressions . 

Again they are of variable quality , 
although always clean and well-maintained . 

But I once had accommodation so small that there was not even room for a small desk . 

This time , there was space for a desk . 

but the appointments were pretty basic : Very comfortable queen bed , bedside tables , dresser , TV . 

The only chair was the desk chair : 
there was no easy lounge chair for enjoying TV , reading the paper , or comfortably partaking of a room service meal � '' 
because there was no room for one . 

In brief , many a well-run motor hotel does better . 

The bathroom , 
although offering only a shower , 
The bathroom was well equipped with good lighting and ample hot water . 
The thermostat was eccentric and difficult to regulate . 
Meals were a problem on this particular visit . 

Because I did not think I would enjoy room service food in my room , 
I opted for the public areas . 

The hotel has stopped opening its popular coffee shop in the evening 
-LRB- an unfortunate decision deplored by many regulars -RRB- 
, 
so that leaves either its Bull And Bear Restaurant or a limited menu in its Peacock Alley lounge . 

Peacock Alley was closed 
-LRB- because of kitchen problems , I was told -RRB- 

so that left the Bull And Bear , a steak and seafood place where the food is excellent , 
the choice limited and the prices stratospheric 
-LRB- the most modest steak costs $ 48 ; vegetables are $ 12 each extra -RRB- 
. 

I ate well there , 
but I resented the limited food options on this particular visit . 
