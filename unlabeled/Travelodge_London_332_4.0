
We stayed at the Travelodge Islington , now called the Travelodge Kings Cross Royal Scot , on two separate occasions : April 16-19 and April 25-28 . 

We had booked at the 26 pound per night rate that can be found on the Travelodge website . 

The regular rates through their website range from 60-70 pounds . 

The 26 pound rate is outstanding value for the room and amenities , as well as location of this hotel , 
and it would be hard to find a better room for the regular rates . 
We had rooms 203 and 206 for our two stays . 

Both had an ensuite bathroom with a tub-shower unit , twin beds , a TV with basic channels and with the option to purchase further channels 
-LRB- which we did not do -RRB- 
. 

There is a phone that requires a credit card swipe for use 
- we did not use the phone 
as we had a mobile phone 
. 

The beds are covered with duvets , 
and the duvet covers as well as the bedsheets are changed daily . 

There were extra pillows in the cupboard . 

We received a large bath towel each as well as a smaller towel . 

As is typical in British hotels , 
there were no washcloths . 

Room 206 overlooked the inner courtyard , 
which contains parking spaces . 

Though we had the window open , 
we found the room to be incredibly quiet 
as was the hotel generally . 

Room 203 overlooked Vernon Rise 
-LRB- or Road , I so not remember -RRB- 
. 

There was a double window , 
and with the windows shut there was no street noise . 

Vernon Rise has almost no traffic . 

We could see King 's Cross Road and Penton Rise from our window , 
but could not hear the traffic . 
The hotel is a purpose built structure of 1960s or so architecture . 

The lobby of the hotel is new and comfortable and is attached to the bar . 

There was no smoking in the looby or the restaurant , 
though people smoked on the stoop outside . 

Check-in was quick and easy on both occasions . 

We arrived an hour ahead of the check-in time on our first stay 
but received a room right away . 

There is no baggage check in the hotel , 
so it is wise to double-check room availability 
if you intend 
to arrive early . 

Otherwise , King 's Cross Station offers baggage check . 

We tried the bar , 
which was pleasant enough , 
and the restaurant menu looked reasonable 
- one could eat a simple meal there , 
but there are pubs and restaurants nearby that look good too 
. 

Once through the lobby , we found our rooms 
by taking one of three elevators . 

The elevators are small 
and we could not figure out how to use the stairs 
to get to the lobby , 
so there was the odd delay in getting off our floor at busy times . 

The elevators work fine 
but are a bit scratched up . 

The second floor hallways look a bit worn but look quite decent all in all . 
The rooms were clean and reasonably maintained . 

Some of the woodwork was scratched 
and in places the wallpaper was loose . 

The bathrooms were clean , 
though could have used a more thorough dusting . 

There was a touch of mildew in one of the tubs , 
but the other was spotless . 

There was a large heating and cooling unit on the wall , 
which we did not use . 
The hotel is an easy ten minute walk from King 's Cross rail station . 

We walked down Pentonville Road to King 's Cross Road , 
which we followe right to the hotel . 

We also exited King 's Cross Tube station through the Thames Link station 
-LRB- just follow the signs for Thames Link -RRB- 
, 
which exits right on King 's Cross Road making the walk a bit easier . 

We also accessed the hotel from one of the bus stops on Pentonville Road -LRB- second stop from King 's Cross station -RRB- , 
and went down Weston street - a mere 3 minutes or so to the hotel . 

Alternatively , the hotel is about a 15 minute walk from Angel tube down beautiful leafy streets . 

A number of buses pass close to the hotel , 
and the 63 goes right by . 

The area in general felt very safe . 

The street in front of the hotel is busy-ish , 
but behind are nice residential streets that receive almost no traffic . 

The park in Percy Circus right behind the hotel is quite lovely . 
Overall , for the 26 pound price we found a bargain , I do not think there is a better deal to be found for the money . 

Yes , there are a few rough edges here and there 
but I found them to be no more than what I have experienced for 100 pounds in other London hotels . 

So , the regular rates are quite reasonable too . 

If you book here expecting a five star hotel , 
then this is not the place for you . 

If you book expecting value for what you pay , and a clean quiet hotel , 
then you will be satisfied . 

I noted on check-in that one patron wanted a room change , 
which was speedily accomplished . 

I know there are negative reviews below , 
but I honestly have to say that we were very pleased with the hotel . 

We got more than we expected 
and the price was quite palatable . 
