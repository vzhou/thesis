
We have stayed at the Manchester Grand Hyatt at least a dozen times in the last few years and have enjoyed every stay . 

The location of the Hyatt is in the heart of Seaport Village , a fun place to walk right outside the backdoor of the hotel with small shops and tasty treats . 

The views of the bay from the hotel are panoramic and interesting . 

Looking south toward the city , Coronado bridge and Mexico ; or north toward the military base , the aircraft carrier and cruise ships , and Point Loma . 
Depending on the time of the year that you visit , 
the area activities can vary and do impact the experience . 

The weekend of a game at Petco Stadium can be a crowded time , but also holidays and certain conferences . 

This is a popular conference hotel with large meeting facilities and adjacency to the San Diego Convention Center a few yards down the path . 

Events vary so ask the hotel staff what is going on when you are booking your trip . 

We have stayed during several conferences including a bridge tournament , a financial advisors meeting and a religious youth conference : all very different . 

Ask about hotel and local events before you book . 
The hotel is also walking distance to the parks around the boat harbor . 

On this trip , there was a concert at the park with fireworks afterwards . 

On the same night , there was a game at Petco stadium that ended with fireworks . 

We saw two fireworks shows from our room in the same night 
-LRB- think : LOUD , but fun -RRB- 
. 

Also loud are the trains that run in front of the hotel , including the alarm clanging at the crossings when the trollies pass . 

Fortunately , the trains and alarms do not run all night , 
but they would prevent you from taking an afternoon nap 
if you were a light sleeper . 

If you are looking for a quiet getaway , 
it would be difficult to find in downtown San Diego with its big city sounds . 

Try the Hotel Del Coronado for a beachfront experience . 
The main pool is on a very large deck with outdoor areas for families with children . 

There is also an adult pool , Kin Oasis , for guests who are 21 years or older . 

The adult pool is next to the spa . 

The spa is a great place for a massage , hair or nail service , 
but you may want to ask about any booked events at the Kin Oasis pool 
if you want a quiet spa experience . 

The adult pool is no longer part of the spa and operates as a bar with food service . 

There are two cabanas available for rent as sitting areas for cocktails , not for typical relaxing by the pool in a chaise lounge chair . 

The lack of sun protection options for the adult pool is a disappointment . 

The family pool on the main deck has several cabanas for relaxing by the pool . 
We book a AAA package to include full breakfast at Lael 's , a significant value . 

The buffet is large and includes fruits , nuts , pastries , traditional hot breakfast items , waffles and an omelet station . 

Eat all you want 
and then go for a walk along the water to work off the calories . 
One major change was the sale of the property in March , 2011 . 

The Manchester family sold the hotel to a group of investors . 

We observed no significant differences during our stay . 

There was live jazz in the hotel 's lounge area on Friday night . 

We had never seen this before and may be something new -LRB- but not sure -RRB- . 
The hotel does sell out and does overbook rooms . 

If you are the victim of overbooking , 
we were told that the Hyatt pays for your accommodations at another hotel . 

This has never happened to us , 
but we usually arrive early in the afternoon . 

The hotel has two large towers 
and we have stayed in both . 

No significant differences except maybe the content of the views . 

The newer tower has end rooms with windows facing Coronado island . 

We stayed in one of these rooms one weekend 
and it was a nice feature , but not a necessity . 
The hotel is also walking distance to the Gaslamp district with great restaurant and night life choices . 

Ask the concierge for recommendations and bring good walking shoes . 

There are also pedi-cabs that will offer you a ride . 

The pedal pumpers charge about $ 12-15 for a distance that you could walk in about 10-15 minutes . 

We rode one 
once because it was warmer than walking on a cold night . 

The night temps do cool down in San Diego , even in the summer , 
so bring a light jacket or sweater for evenings . 
Our room was a bit musty on this stay , 
but the air conditioning worked well 
and we opened the room 's window for some fresh air . 

The Hyatt now offers a room category for people who have allergies : hypoallergenic bedding and an air filtration system . 
We stay at the Hyatt for a getaway from work , 
so we have never used internet services . 
