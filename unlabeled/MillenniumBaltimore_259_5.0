
I stayed at the Biltmore during a recent professional nursing convention with some trepidation . 

I read reviews that described the rooms as shabby &amp ; outdated , the staff as unfriendly , 
and the neighborhood as unsafe . 

Travelling across the country alone , 
I was very nervous about what I had gotten myself into . 

I could not have been happier with my stay &amp ; 
wonder what was going on with those who had a negative experience . 

To say that the lobby and the common areas of the hotel are stunning is an understatement . 

Everything speaks to the rich history of this landmark . 

I especially enjoyed learning about the hotel 's role in the glory days of old Hollywood . 

The staff was friendly and helpful , 
directing me to the best ways to get around the city and the things to do . 

Cabs were very easy to come by , thanks to the staff of the hotel , 
but I chose to walk to and from the Convention Center each day . 

I did not ever feel unsafe walking alone downtown . 

I found the facilities to be more than adequate- the fitness center has a lot of equipment 
-LRB- more than I was expecting , based on one review -RRB- 
and was open around the clock . 

This was great for someone who was out of sorts with the time change and wide awake at 3 a.m. . 

most days , ready to get going . 

Although I did not eat in the hotel much , 
I had a wonderful , if pricey , breakfast at Smeraldi 's . 

For more value , there 's a great little coffee shop around the corner on 6th , called the Yorkshire Grill , 
where I got a delicious breakfast for less than $ 10 , including tip . 

My room was charming &amp ; comfortable . 

The mattresses have , indeed , been upgraded 
and the linens were crisp , clean , and of high quality . 

Yes , the TV was a little small , 
but who needs to spend much time watching TV 
when you 're in LA ? 

If I wanted to nitpick , 
I 'd say that the bathtub could have been re-glazed . 

Other than that , my room was perfect . 

I met several other people staying at other local hotels for the convention who stated they would have rather been at the Biltmore . 
