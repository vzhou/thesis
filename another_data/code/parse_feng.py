# coding=utf-8
from __future__ import unicode_literals

import sys
sys.path.append('/proj/anne-phd/software/discourse_parse_dist/src')
import os
import codecs
import parse


parser = parse.DiscourseParser(verbose = False, seg = False, output = True, SGML = False, edus = False)
homedir = 'texts_sb'
#originaldir = 'labeled'
#cleandir = 'clean_labeled'


#def clean():
  #for f in os.listdir(originaldir):
    #fo = open(os.path.join(homedir,f),'w')
    #for line in codecs.open(os.path.join(originaldir,f), encoding='utf-8'):
      #if (not line.split()) or line.startswith('.') or line.startswith('0') or line.startswith('!') or line.startswith('-RRB-') or line.startswith(','):
	#pass
      #else:
	##line = line.replace('�','pound').replace('$','dollar')
	#fo.write(line)
    
    #fo.close()  

#def add_label():
  #for f in os.listdir(cleandir):
    #fs = open(os.path.join(homedir,f)+'.txt','w')
    #fedu = open(os.path.join(homedir,f)+'.txt.edus','w')
    
    #sentence = ''
    #for line in codecs.open(os.path.join(cleandir,f), encoding='utf-8'):
      #edu = line.split('\t')[0].strip()
      #fedu.write('<edu>%s</edu>\n'%edu)
      #if edu.endswith('.') or edu.endswith('!'):
	#sentence += edu
	#fs.write('%s<s>'%sentence)
	#sentence = ''
      #else:
	#sentence += edu+' '
    
    #fs.close()
    #fedu.close()

def sbd():
  global dir
  for f in os.listdir(homedir):
    print f
    with open(os.path.join(homedir,f)) as openf:
      text = openf.read().replace('\n','<s>\n')
    open(os.path.join(homedir,f),'w').write(text)
    
    
def parse_file(f):
  global parser
  parser.parse(f)
  
  
def parse_all():
  number = 0
  flog = open('log','w')
  files = os.listdir(homedir)
  for fi in files:
    if fi.endswith('.txt'):
      try:
        parse_file(os.path.join(homedir, fi))
        flog.write('Parsed'+fi+'\n')
        number += 1
      except Exception, e:
        flog.write('Failed'+fi+'\n')
        renew()
  print number

def renew():
  global parser
  parser = parse.DiscourseParser(verbose = False, seg = False, output = True, SGML = False, edus = False)



def go():
  parse_file('/proj/anne-phd/victor/bitbucket_copy/another_data/data/tmp/kitchen_positive.markable_17.txt')
  #parse_all()
  
go()