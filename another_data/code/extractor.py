import re
import xml.etree.ElementTree as ET
import os
import codecs
import copy


def extract_texts():
  loc_review = 'raw_data/Sentiment_GOLDSTANDARD/Markables'
  loc_word = 'raw_data/Sentiment_GOLDSTANDARD/Basedata'
  loc_text = 'text/texts'
  punc = [',', '.', ':', '!', '?', ';', '-']
  junk_ender = re.compile(r' \bUSA\b')  # There is piece of junk info at the beginning, such as rating info, categories, etc. This ends with an rate, say 'x.x'
  
  for fn in os.listdir(loc_word):
    fnr = fn.replace('words','review_level')
    genre = fn.replace('_words.xml','') 
    
    words = read_words(os.path.join(loc_word,fn))
    reviews = read_reviews(os.path.join(loc_review,fnr))
    
    for k, v in reviews.items():
      i = v[0]-1
      content = [' '+words.get(x) if words.get(x) not in punc else words.get(x) for x in range(v[0]+1,v[1]+1)]
      
      isusa = 0
      for i in range(len(content)):
        if junk_ender.match(content[i]):
          content = content[i+1:]
          isusa = 1
          break
          
      if isusa == 0:
        fo = codecs.open(os.path.join('temp',genre+'.'+k+'.txt'),'w', encoding='utf-8')
      else:
        fo = codecs.open(os.path.join(loc_text,genre+'.'+k+'.txt'),'w', encoding='utf-8')
        
      fo.write(''.join(content))
      fo.write('.\n')
      fo.close()

    
def extract_edus():
  loc_rp  = 'raw_data/Sentiment_GOLDSTANDARD/Markables' #location of review and polarity
  loc_word = 'raw_data/Sentiment_GOLDSTANDARD/Basedata'
  loc_edu = 'text/edus'
  loc_out = 'labeled'
  
  for fn in os.listdir(loc_word):
    fnr = fn.replace('words','review_level')  #name of file of review level, say cellphone_negative_review_level
    fnp = fn.replace('words','polarityGold_level') #name of file of polarity gold annotation
    genre = fn.replace('_words.xml','')  #part of name of a specified review, say, cellphone_negative_xxxx, will followed by marlable_id
    
    words = read_words(os.path.join(loc_word,fn))
    polarity = read_polarity(os.path.join(loc_rp, fnp))
    #print polarity
    
    reviews = read_reviews(os.path.join(loc_rp,fnr))
    
    for k, v in reviews.items():
      start, end = v[0], v[1]
      fne = genre + '.' + k + '.txt.edus'  #name of file of edu, which will be used to generate gold standard
      fno = genre + '.' + k #name of file of output
      tmp_words = [words[i] for i in range(start,end+1)] #words in this review
      tmp_polarity = [polarity[i] for i in range(start,end+1)] #coresponding polarities in this review 
      #print tmp_polarity
      if os.path.exists(os.path.join(loc_out,fno)) and (len(open(os.path.join(loc_out,fno)).readlines())>1):
        print 'Parsed,',fne
      elif os.path.exists(os.path.join(loc_edu,fne)):
        print 'Processing,', fne
        fedu = open(os.path.join(loc_edu,fne))
        fo = codecs.open(os.path.join(loc_out,fno), 'w', encoding = 'utf-8')      
        lines = fedu.readlines()
        votes = []
        matchlines(copy.deepcopy(lines), tmp_words, tmp_polarity, votes)
        for i in range(len(lines)):
          fo.write('%s\t%s\n'%(lines[i].strip(),pl_name(votes[i])))
        fo.close()
      else:
        print fne, 'is Filtered out, rite?'
      
        
      
      
def matchlines(l,ws,pl,result):
  entry = l[0].split()
  if result == []:
    i1 = ws.index(entry[0])
  else:
    i1 = 0
    
  thisline = ''.join(entry)
  cmpline = ''
  for i in range(i1,i1+len(entry)+7):
    cmpline += ws[i]
    #print cmpline, thisline
    #if thisline.startswith('IdorecommendTasty'):
      #status = 1
      #i = ws.index('.')
      #break
    if thisline in cmpline:
      status = 1
      break
    elif not cmpline in thisline:
      status = 0
      break
  
  #print thisline,ws,i1

  if status == 0:
    ws = ws[i1+1:]
    pl = pl[i1+1:]
    matchlines(l,ws,pl,result)
  else:
    #print pl[i:i+len(entry)]
    i2 = i
    result.append(sum(pl[i1:i2+1]))
    
    if len(l)>2:
      l = l[1:]
      ws = ws[i2+1:]
      pl = pl[i2+1:]
      #print l,ws
      matchlines(l,ws,pl,result)
    elif len(l) == 2:
      result.append(sum(pl[i2+1:]))
    else:
      pass
  
  #tmp_result = copy.deepcopy(result)
  #return tmp_result
      
      
    
    
  
      
def read_reviews(f):
  tree = ET.parse(f)
  review = {} #review_id:(start, end)
  start_end = re.compile(r'word_(\d+)..word_(\d+)')
  
  for child in tree.getroot():
    review[child.attrib.get('id')] = (int(start_end.match(child.attrib.get('span')).group(1)), int(start_end.match(child.attrib.get('span')).group(2)))
    
  return review

def read_words(f):
  tree = ET.parse(f)
  words = {} #index:word
  index = 1
  for child in tree.getroot():
    words[index] = child.text
    index += 1
    
  return words
  

def read_polarity(f):
  tree = ET.parse(f)
  polarity = {} # index:polarity
  get_index = re.compile(r'word_(\d+)')
  
  for child in tree.getroot():
    polarity[int(get_index.match(child.attrib.get('span')).group(1))] = pl_digital(child.attrib.get('polarity'))
  
  return polarity

def pl_name(d):
  """
  cast pos num to 'pos', etc.
  """
  if d>0:
    return 'pos'
  elif d<0:
    return 'neg'
  else:
    return 'neu'
  
  
  
def pl_digital(p):
  """
  cast neg-->-1, pos-->1, neu--> 0
  """
  if 'negative' in p:
    return -1
  elif 'positive' in p:
    return 1
  else:
    return 0
  
extract_edus()
