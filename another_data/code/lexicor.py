import os


def load_lexicons():
  afinn_path = '../../polarity list/AFINN-111.txt'
  huliu_negative_path = '../../polarity list/Hu_Liu/negative-words.txt'
  huliu_positive_path = '../../polarity list/Hu_Liu/positive-words.txt'
  ofinder_path = '../../polarity list/subjclueslen1-HLTEMNLP05.tff'
  lafinn = dict(map(lambda (k,v): (k,int(v)), [ line.split('\t') for line in open(afinn_path) ]))
  lhln = dict.fromkeys([line.strip() for line in open(huliu_negative_path)], -1)
  lhlp = dict.fromkeys([line.strip() for line in open(huliu_positive_path)], 1)
  lhl = dict(lhlp, **lhln)
  lof = {i[2][6:]:s2d(i[5][14:]) for i in [line.split() for line in open(ofinder_path)]}
  return lafinn, lhl, lof

def load():
  gold = '../data/labeled'
  out = '../data/lexico-summed'
  lafinn, lhl, lof = load_lexicons()
  correct = 0
  total = 0
  for f in os.listdir(gold):
    print 'processing, ',f
    c,t = load_file(os.path.join(gold,f),os.path.join(out,f),lafinn,lhl,lof)
    correct += c
    total += t
  print correct, total
  
def load_file(f, of, lafinn, lhl, lof):
  negation = ["not", "no", "don't", "doesn't", "never", "hardly", 'none', 'nothing', 'nowhere','neither', 'nor', 'nobody', 'scarely', 'barely', "can't", "won't", "wouldn't", "shouldn't", "couldn't"]
  negation_break = ['only', 'but', 'than']
  correct = 0
  total = 0
  with open(f) as fg, open(of, 'w') as fo:
    for line in fg:
      entry = line.split('\t')
      score = 0
      is_negated = 1
      for w in entry[0].lower().split():
        if w in negation:
          is_negated = -1
        elif w in negation_break and is_negated == -1:
          is_negated = 1
        else:
          score += is_negated * vote(w, lafinn, lhl, lof)
      fo.write('%s\t%s\t%s'%(entry[0],score,entry[1]))
      total += 1
      correct += simple_classify(score,entry[1].strip())
  return correct, total

def vote(w,l1,l2,l3):
  s1, s2, s3 = l1.get(w,0), l2.get(w,0), l3.get(w,0)
  #print w, s1,s2,s3
  if sum([s1,s2,s3])>0:
    return 1
  elif sum([s1,s2,s3])<0:
    return -1
  else:
    return 0

def simple_classify(score, label):
  if (score>0 and label=='pos') or (score<0 and label=='neg') or (score==0 and label=='neu'):
    return 1
  else:
    return 0

def s2d(s):
  """
  string to digit
  """
  if s == 'positive':
    return 1
  elif s == 'negative':
    return -1
  else:
    return 0


def go():
  load()

go()
  