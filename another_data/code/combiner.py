import os
import itertools

feng = '../data/feng/feng-featured-3'
lexi = '../data/lexico-summed'
gold = '../data/labeled'
adja = '../data/adjacent/adjacent_2'

outpath = '../result'

#merge feng, adjacent_1, lexi
def merge1():
  global feng, lexi, gold, adja
  fengfeatures = get_allfengfeatures()
  with open(os.path.join(outpath,'feng-gold'),'w') as fo, open(os.path.join(outpath,'tooshort.filterout'), 'w') as fso:
    for f in os.listdir(gold):
      ff = open(os.path.join(feng,f)).readlines()
      fg = open(os.path.join(gold,f)).readlines()
      fa = open(os.path.join(adja,f)).readlines()
      for i in range(len(fg)):
        if fg[i].split('\t')[0].split() == '(' or fg[i].split('\t')[0].split() == '.':
          fso.write(fg[i].split('\t')[0]+'\n')
        else:
          fengs = map(lambda x:str(fengfeatures.get(x)),ff[i].split(','))
          fo.write('%s,%s,%s'%(','.join(fa[i].split(',')[:3]), ','.join(fengs), fg[i].split('\t')[-1]))


def get_allfengfeatures():
  global feng
  features = []
  for f in os.listdir(feng):
    with open(os.path.join(feng,f)) as ff:
      for line in ff:
        features.append(line.split(','))
  features = set(itertools.chain(*features))
  return dict(zip(features, range(len(features))))

    
def summarize():
  global adja, feng
  out = open('/home/eastdog/thesis/another_data/result/feng-gold.3', 'w')
  for fil in os.listdir(feng):
    fa = open(os.path.join(adja,fil)).readlines()
    ff = open(os.path.join(feng,fil)).readlines()
    if len(fa) != len(ff):
      print fil
    else:
      for i in range(len(fa)):
        entry = fa[i].split(',')
        aline = entry[:-1]
        aline.append(ff[i].strip())
        aline.append(entry[-1])
        out.write(','.join(aline))

def wekaize():
  source = open('/home/eastdog/thesis/another_data/result/feng-gold.3').readlines()
  out = open('/home/eastdog/thesis/another_data/result/feng-gold.3.arff', 'w')
  allrelation = [line.split(',')[5:-1] for line in source]
  allrelation = set(itertools.chain(*allrelation))
  index = dict(zip(allrelation,range(len(allrelation))))
  total = len(allrelation)+5

  out.write('@RELATION EDU_polarity_prediction\n\n')
  out.write('@ATTRIBUTE prev1 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE next1 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE prev2 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE next2 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE lexi NUMERIC\n')
  for i in range(len(allrelation)):
    out.write('@ATTRIBUTE rst%s NUMERIC\n'%i)
  out.write('@ATTRIBUTE class {pos,neg,neu}')
  out.write('\n@DATA\n')

  for line in source:
    entry = line.split(',')
    relations = entry[5:-1]
    relationindexs = sorted(list(set([index[r]+5 for r in relations])))
    relationindexs = ['%s %s'%(item,1) for item in relationindexs]
    rst = ','.join(relationindexs)
    rest = ['%s %s'%((i,entry[i])) for i in range(5)]
    rest = ','.join(rest)
    out.write('{%s,%s,%s %s}\n'%(rest,rst,total,entry[-1].strip()))

summarize()
wekaize()