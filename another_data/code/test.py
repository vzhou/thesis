import os
import re
import shutil


def clean():
  mix = '/home/eastdog/thesis/another_data/data/text/texts_sb'
  destination = '/home/eastdog/thesis/another_data/data/text/edus'

  for f in os.listdir(mix):
    if f.endswith('.edus'):
      edu = open(os.path.join(mix,f))
      fo = open(os.path.join(destination,f),'w')
      for line in edu:
        fo.write(line.replace('<edu>','').replace('</edu>',''))
      fo.close()

def check_fpowf():
  """
  check whether feng parser's output is well formed, say, NOT put two edus in same line
  """
  notwell = re.compile(r'(.*\[\w\]\[\w\])(.*)')
  
  fengpath = 'feng-parsed'
  goldpath = 'labeled'
  
  for f in os.listdir(goldpath):
    length = len(open(os.path.join(goldpath,f)).readlines())
    ff = open(os.path.join(fengpath,f+'.txt.tree'))
    i = 0
    for line in open(os.path.join(fengpath,f+'.txt.tree')):
      if not line.startswith('(') and '[' not in line:
        i += 1  
    if i != length:
      print f, i, length

    #for index, line in enumerate(ff.readlines()):
      #if ('[N]' or '[S]') in line and (not line.rstrip().endswith(']')):
        #print f, index
    
    #lines = []
    #with open(os.path.join(fengpath,f+'.txt.tree')) as ff:
      #for line in ff:
        #if ('[N]' or '[S]') in line and (not line.rstrip().endswith(']')):
          #s1, s2 = notwell.findall(line.rstrip())[0]
          #sp = ' '*(len(s1)-len(s1.lstrip())+2)
          #s21, s22 = sp+(s2[:len(s2)/2].lstrip()), sp+(s2[len(s2)/2:].rstrip())
          #lines.append(s1)
          #lines.append(s21)
          #lines.append(s22)
        #else:
          #lines.append(line.rstrip())
    #with open(os.path.join(fengpath,f+'.txt.tree'), 'w') as ff:
      #ff.write('\n'.join(lines))





check_fpowf()