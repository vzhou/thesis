My problems with nextel did not stop when I canceled my service.<s>
I will get to the problems with the service.<s>
When I went to get a new phone a day before my contract expired , I of course wanted to keep my number.<s>
So as I went to get my Cingular phone ( highly recommended ) , I had the old number switched to my new number.<s>
Today , I get in the mail my nextel statement for the month with a $ 200 early termination even though I had not canceled my service , but had simply got a new phone and switched my number over.<s>
This was on icing on the cake after 2 years of absolutely amazingy bad service from nextel.<s>
In my town , not a small town by any means , there are very few places that have any service.<s>
In the largest subdivision , you can not get service anywhere.<s>
I constantly get comments from people trying to reach me that get the classic nextel line ... Please hold while the nextel subscriber you are trying to reach is located.<s>
Most of the time I would not even recieve incoming calls , calls would go straight to my voice mail.<s>
Not only would I not get the calls , but often I would not even get the voice mails until days or even weeks later.<s>
When I did actually take calls ( and call out ) , I can not count the number of times I 've had my call dropped.<s>
I recieved so many different error messages sometimes I wonder if they just spend all their time at nextel thinking of different error messages to program into their phones.<s>
That brings me to the selling point of nextel , the famed and well-loved direct connect feature.<s>
This is an absolute joke.<s>
Direct connect only works MAYBE half the time I tried to use it.<s>
Even then , in the middle of the conversation I would be greeted by the loud beep of my phone telling me I wont be talking to that person anymore.<s>
Of course , I would expect nothing less from the nextel network.<s>
Text messaging with nextel is next to impossible.<s>
If it did not take hours to send or recieve a message , it took days , or maybe did n't even come at all.<s>
The internet access was fine , it the rare instance that it worked.<s>
Of course , when you do n't have service , you do n't get the internet access either.<s>
I was constantly getting the message " network not available " .<s>
Calling and trying to complain to nextel is futile.<s>
Trying to get through to customer service takes forever and once you get through they do nothing.<s>
My experience with nextel has been nothing short of a disaster and I write this trying to warn anyone considering nextel.<s>
Stick with Verizon or Cingular and you will be much happier in the end.<s>
Do n't make the same mistake I did .. <s>
