I had a Pampered Chef sprayer that I loved , but it got clogged and stopped working after 2 or 3 years.<s>
I should have tried harder to find a Pampered Chef rep to buy another one , the Pampered Chef one works better than this one.<s>
However , this is not a horrible product.<s>
One drawback compared to the Pampered Chef one is that you ca n't see through this one to see how high the oil level is.<s>
You want to fill it just a bit higher than halfway , and when it is less than 1/3 full you need to fill it back up or it wo n't work as well.<s>
This unit is even more finicky than the Pampered Chef one about the level , it almost wo n't spray if the level is too low.<s>
But since you ca n't see through it , even holding it up to the light , you have to look down in and estimate how high it is.<s>
The reflective coating inside makes it very hard to see how high it really is.<s>
You do n't want to fill it all the way up , either.<s>
Then , you need to pump it more than the Pampered Chef one needed to be pumped.<s>
This one sprays a very fine spray , finer than the Pampered Chef one.<s>
And it sprays quite nicely if you have the correct level of oil in there.<s>
I think I know why my old one got clogged.<s>
So I 'm trying something new : each time I 'm done , before I put it away , I twist the screw on cap around the nozzle and release the pressure.<s>
I think if you leave it pressured up all the time , like I did with my old one , it damages the nozzle over time.<s>
But time will tell if I 'm right.<s>
Another tip -- do n't fill it with anything but olive oil.<s>
Other oils evaporate and leave a sticky/greasy feeling on the outside of the sprayer.<s>
You know what I mean if you use vegetable oil for stovetop cooking a lot , you get that greasy feeling on things that you leave near the stove or that hang over it.<s>
Olive oil does n't do that , that 's why I now do n't use anything else on my stovetop ( except sometimes peanut oil for stir frying ) .<s>
This type of sprayer will work much better if you just use Olive Oil in it , trust me.<s>
But if you have a choice , go find a Pampered Chef rep and by their sprayer.<s>
It 's cheaper and better quality.<s>
