Within a few weeks of purchase the teflon in the Zojirushi pan started to blister and flake off.<s>
This is NOT the result of customer abuse but a manufacturing defect.<s>
This is my third ( and final ) bread machine.<s>
The first two were Breadman Ultimates.<s>
I should note that I make bread in the machine about 6 times a week on average , pretty much every morning.<s>
The first Breadman worked for over a year until the teflon in the pan started to fail.<s>
This was probably my fault for allowing the pan to soak for extended periods of time ( water can get behind the teflon and ruin the bond ) .<s>
I bought an new Breadman Ultimate because the entire machine was only slightly more than the pan alone and the old machine was starting to have mechanical problems.<s>
This time I took extremely good care of the pan ( letting it cool down , no soaking ) because it 's clearly the weak link in the system.<s>
Within 8 months the stirring shaft that drives the paddle in the pan simply fell out.<s>
Whatever the detente mechanism was that holds the shaft to the pan failed , making the machine useless.<s>
Currently it does n't seem very easy ( or even possible ) to buy a replacement pan for the Breadman.<s>
At this point I was done with the Breadman.<s>
Much as I did n't want to spring for the extra money for the Zojirushi , and realizing that my default augmented sourdough recipe probably would n't work the same in the new machine , I did n't see an alternative and got one.<s>
Within 3 weeks the teflon in the Zoji pan formed two blisters.<s>
One is bit smaller than a dime and all the teflon at this spot has flaked off ; the other spot is blistering but the teflon is still intact.<s>
I have been extremely careful with the pan : I wait for it to cool and then remove the baked-on bread under the paddles with warm water using mostly my fingers.<s>
I do n't let it soak at all.<s>
This is clearly a manufacturing defect.<s>
I have further complaints with respect to this machine as compared to the Breadman Ultimate : 1 ) The top of the bread rarely gets brown.<s>
I 've added more sugar to the recipe which helps slightly but not much.<s>
2 ) I do n't understand what the crust control option does.<s>
It does n't seem to increase baking time which it does on the Breadman so it does n't make much sense.<s>
3 ) The programmability is very weak.<s>
On the Breadman you can take any of the existing standard options and modify them as well as make up entirely new programs.<s>
4 ) The programmability options are very limited : - You ca n't control the knockdown time between risings.- You ca n't add risings to the default three.<s>
They do give you a lot of time control on the risings.- You ca n't separate the mixing cycle from the kneading cycle : it 's all one cycle.- When you turn off the preheat cycle , this only affects the built-in programs and not the custom ones.- And worst of all : you ca n't bake for more than 70 minutes.<s>
This is utterly ridiculous.<s>
I 'm sure this is a " safety feature " but it means you ca n't correct for the pathetic lack of crust formation or have larger recipes and so forth.<s>
This is almost a killer limitation in my opinion.<s>
For those other reviewers that are having issues with rising , I 've determined that the machine is very sensitive to the amount , type , and quality of yeast.<s>
If you * exactly * follow the manufacturers instuctions with respect to yeast and use fresh yeast , it should work okay.<s>
This means you must use rapid rise yeast with the Quick programs and active dry yeast with the longer standard programs.<s>
Even using Fleischmann 's instead of Red Star made a difference.<s>
The Breadman was pretty much indifferent to yeast variations.<s>
At this point I 'm done with bread machines.<s>
They are a failed technology.<s>
In fact , teflon is a dangerous and substandard technology.<s>
All you have to do is search for teflon ( Polytetrafluoroethylene ) and toxic to come up with some pretty alarming data on this noxious substance.<s>
The manufacturing process results in significant environmental contamination.<s>
A direct consumer health risk due to toxic outgassing occurs when teflon is exposed to temperatures above 500 degrees ( eg : frying pans ) .<s>
Perhaps bread machines are not that risky given the lower temps involved in baking and the fact that wet dough will keep the average pan temperature pretty low , but if you know anything about toxicity you will realize that it is always a sliding scale : there will be some outgassing at lower temperatures.<s>
Imagine low levels of toxic gas released into your homemade bread.<s>
I 've thought about this , but there 's no way to make a bread machine without teflon -- going from mixing dough to baking requires teflon , there is just no other viable solution.<s>
I love making bread every day and I 'm going to miss this activity but at this point the manufacturing defects and use of teflon mean I have to give up on this technology.<s>
It 's back to the occasional hand-made loaf -- it 's the only rational and healthy solution.<s>
