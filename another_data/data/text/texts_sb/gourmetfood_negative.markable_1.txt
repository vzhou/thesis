I really do like my Senseo coffee maker.<s>
I , like others who have reviewed the Kona Pods here , was always searching for a better tasting coffee pod to use in the machine.<s>
I was excited to see that Kona now made pods and was also happy to see that they had a pod that was a ' 2 cup style '.<s>
I have not found it to be a huge deal to use 2 smaller ' 1 cup ' size pods , but it sounded nice to be able to just put one large pod in the machine instead of 2 smaller one.<s>
After reading some of the reviews here , I could not figure out what some of the reviewers meant by the Kona pods making different amount of coffee per cup each time or how the coffee could be strange and undrinkable.<s>
It just did n't make sense.<s>
Well , it still does n't make sense , but it certainly happened with the coffee I made using the Kona pods also.<s>
My results were overall poor and it was very disappointing as the Kona pods cost a lot more than Folgers or other pod brands that I can buy locally.<s>
The first thing I noticed was that the coffee in the 2 cup sized Kona pods is packed in much more tightly than in any other pod I have ever used in my Senseo.<s>
The instructions for the Kona pods say to wiggle the pods to break up the coffee before inserting into the machine.<s>
The Kona pods feel very hard when taken out of their package.<s>
I always wiggled and jiggled the pods for about 10 seconds before putting them in the Senseo.<s>
After my problems with the Kona pods , my husband decided that I was not ' wiggling ' them enough.<s>
He promptly wiggled one to the point that it broke open.<s>
So , there is just so much that can be done to the pod before putting it in the machine.<s>
The first thing that I noticed was that the amount of coffee brewed when using the Kona pod was quite a bit less than when using 2 smaller pods.<s>
That did not make much sense , but after several cups of Kona , it was obvious that there was never the amount of coffee coming out as before when using 2 small one cup pods.<s>
The best I can figure out is that the Kona pods themselves seem to ' hold ' a lot more water.<s>
There is more water that remains in the pod itself after the brewing is done.<s>
The pod itself is heavier when I take it out to throw it away.<s>
So , I believe the reason that there is less actual coffee using Kona pods is that the pods themselves retain a lot more water and it can not pass through into the coffee cup.<s>
Then , as others have mentioned , there is the problem that happens to me about 1/2 the time when using the Kona pods.<s>
The problem is that for whatever reason , the water does not actually go through and penetrate the Kona pods during the brewing cycle.<s>
The ' coffee ' that is supposed to be coming out of the spout is just water ! This problem is one that I still can not really figure out.<s>
The Kona pods are a little smaller in diameter than all the other pods I have tried.<s>
So , I guess that somehow the water has enough room to ' go around ' the actual pod and come out of the spout without ' going through ' the pod to brew coffee.<s>
I also wonder if the paper filter of the Kona pods is different than other pods and for some reason the Kona paper is more difficult for water to penetrate.<s>
I have tried wetting the Kona pod paper filter right before starting to brew the coffee thinking that perhaps it would help the water to penetrate the paper and pod.<s>
But , that has not worked for me.<s>
So , it is back to regular more reasonably priced coffee pods for me.<s>
I have wasted too many expensive Kona pods trying to get a proper cup of coffee from them.<s>
Even when the Kona pods brew a cup of coffee , it is not as large of an amount of coffee as I get using other brands.<s>
So , adding the cost of the Kona pods to the fact that it brews a smaller amount of coffee equals a product that is way too expensive when it does actually work.<s>
I have been reading of a product called an EcoPod.<s>
It is a refillable pod for these type of coffeemakers.<s>
It might be the next thing I try .. <s>
