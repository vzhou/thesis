<edu>Le Creuset manufactures porcelain enameled cast iron cooking vessels as well as other quality products .</edu>
<edu>The enameled cookware is cast from molten iron</edu>
<edu>formed in sand moulds</edu>
<edu>which are used only once .</edu>
<edu>For this reason , one pan may look like another ,</edu>
<edu>but each is unique in its own way .</edu>
<edu>Cast iron is a highly efficient material</edu>
<edu>which absorbs and distributes heat efficiently .</edu>
<edu>For this reason ,</edu>
<edu>it is recommended to use on low to medium heat .</edu>
<edu>There are a few exceptions like boiling water</edu>
<edu>where cooking on high heat is appropriate and okay .</edu>
<edu>Otherwise , cooking with high heat may cause food</edu>
<edu>to stick , discolor the enamel coating , or cook the food unevenly .</edu>
<edu>Remove cast iron from the heat</edu>
<edu>and it does not cool off quickly .</edu>
<edu>This helps your food stay warm while serving .</edu>
<edu>Beware ;</edu>
<edu>iron tends to be heavy</edu>
<edu>so these cooking vessels tend to be heavy as well .</edu>
<edu>Cast iron is an effective material for cooking</edu>
<edu>so why cover it with enamel ? Iron tends to rust ;</edu>
<edu>iron oxide</edu>
<edu>( rust )</edu>
<edu>does n't add any complimentary flavors to your food .</edu>
<edu>If your cast iron cooking vessel was not enameled ,</edu>
<edu>you would have to season</edu>
<edu>and maintain it .</edu>
<edu>Adding the porcelain enamel not only looks good ,</edu>
<edu>but has multiple benefits .</edu>
<edu>The enamel ,</edu>
<edu>being a solid coating , is one of the most hygienic surfaces to cook on ,</edu>
<edu>does not stain , absorb odors , retain flavors ,</edu>
<edu>and is easy to clean .</edu>
<edu>The enamel can be damaged , so limit cooking tools to plastic or wood .</edu>
<edu>The benefits of cooking with Le Creuset cast iron are many ,</edu>
<edu>but lets get down to the roasting pan specifically .</edu>
<edu>This pan holds 5 1/4 quarts of food , measures 9 3/4 x 14 x 3 inches internally , and weighs 10.2 pounds .</edu>
<edu>5 1/4 quarts will typically feed 8 - 10 people</edu>
<edu>when it comes to casseroles , so consider your recipes</edu>
<edu>when determining the size</edu>
<edu>you need .</edu>
<edu>Except for roasts , the roasting pan tends to do best if at least 3/4 full .</edu>
<edu>Any less</edu>
<edu>and you may overwhelm your food with heat evenly dispersed in the pot .</edu>
<edu>I find that the cast iron adds a crispy texture to food</edu>
<edu>that may be desirable with a dish like mac-n-cheese ,</edu>
<edu>while not so desirable for other dishes .</edu>
<edu>Add the Le Creuset 9 x 11 1/2 inch roasting rack for roasting chickens and pork loins</edu>
<edu>keeping them out of the grease .</edu>
<edu>Some other possibilities for this pan include casseroles , pork chops</edu>
<edu>covered with cornbread dressing , dinner rolls , baked squash , lasagna , scalloped potatoes , cobblers , pineapple upside down cake , or bread pudding .</edu>
<edu>The possibilities almost seem endless to your imagination .</edu>
<edu>One feature that people tend to complain about is the cost .</edu>
<edu>The cost does seem steep</edu>
<edu>compared to other pans</edu>
<edu>made from other materials .</edu>
<edu>Heck , cast iron does n't seem like it should be expensive</edu>
<edu>when comparing to that old cast iron pan past generations have used for cornbread and camping .</edu>
<edu>However the manufacturing process requires making a mould for each and every pan produced .</edu>
<edu>They then go through the enameling process</edu>
<edu>before shipping these heavy pans .</edu>
<edu>If you take what goes into making and distributing these high quality cast iron products ,</edu>
<edu>the cost tends to make sense .</edu>
<edu>Then consider that this pan will easily last a lifetime</edu>
<edu>when taken care of .</edu>
<edu>PROS :</edu>
<edu>Extremely versatile Efficient absorption and distribution of heat Hygienic enamel cooking surface Does not stain , absorb odors , or retain flavors Easy to clean</edu>
<edu>when not abused or misused Can be transferred from stove top to the oven to the table</edu>
<edu>Can easily last a lifetime CONS :</edu>
<edu>Its heavy ,</edu>
<edu>as cast iron tends to be weighing in at about 10.2 pounds .</edu>
