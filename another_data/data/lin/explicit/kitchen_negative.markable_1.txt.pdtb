 Within a few weeks of purchase the teflon in the Zojirushi pan started to blister and flake off . 
  This is NOT the result of customer abuse but a manufacturing defect .  
  This is my third -LRB- and final -RRB- bread machine .  
  The first two were Breadman Ultimates .  
  I should note  that I make bread in the machine about 6 times a week on average , pretty much every morning .  
  {Exp_0_Arg1 The first Breadman worked for over a year Exp_0_Arg1} {Exp_0_conn_Asynchronous until Exp_0_conn} {Exp_0_Arg2 the teflon in the pan started to fail . Exp_0_Arg2}  
  This was probably my fault for allowing the pan to soak for extended periods of time -LRB- water can get behind the teflon and ruin the bond -RRB- .  
  {Exp_2_Arg1 {Exp_1_Arg1 I bought an new Breadman Ultimate Exp_1_Arg1} {Exp_1_conn_Cause because Exp_1_conn} {Exp_1_Arg2 the entire machine was only slightly more than the pan alone Exp_1_Arg2} Exp_2_Arg1} {Exp_2_conn_Conjunction and Exp_2_conn} {Exp_2_Arg2 the old machine was starting to have mechanical problems . Exp_2_Arg2}  
  {Exp_3_Arg1 This time I took extremely good care of the pan -LRB- letting it cool down , no soaking -RRB- Exp_3_Arg1} {Exp_3_conn_Cause because Exp_3_conn} {Exp_3_Arg2 it 's clearly the weak link in the system . Exp_3_Arg2}  
  Within 8 months the stirring shaft that drives the paddle in the pan simply fell out .  
  Whatever the detente mechanism was that holds the shaft to the pan failed , making the machine useless .  
  Currently it does n't seem very easy -LRB- or even possible -RRB- to buy a replacement pan for the Breadman .  
 {Exp_4_Arg1 At this point I was done with the Breadman . Exp_4_Arg1} 
 {Exp_4_conn_Alternative Much as Exp_4_conn} {Exp_4_Arg2 I did n't want {Exp_5_Arg1 to spring for the extra money for the Zojirushi , Exp_5_Arg1} {Exp_5_conn_Conjunction and Exp_5_conn} {Exp_5_Arg2 realizing that my default augmented sourdough recipe probably would n't work the same in the new machine Exp_5_Arg2} , I did n't see an alternative and got one . Exp_4_Arg2} 
  Within 3 weeks the teflon in the Zoji pan formed two blisters .  
  One is bit smaller than a dime and all the teflon at this spot has flaked off ; the other spot is blistering {Exp_6_conn_Contrast {Exp_6_Arg1 but Exp_6_conn} {Exp_6_Arg2 the teflon is still intact Exp_6_Arg1} Exp_6_Arg2} .  
  I have been extremely careful with the pan : I wait {Exp_7_Arg1 for it to cool and Exp_7_Arg1} {Exp_7_conn_Asynchronous then Exp_7_conn} {Exp_7_Arg2 remove the baked-on bread under the paddles with warm water using mostly my fingers Exp_7_Arg2} .  
  I do n't let it soak at all .  
  This is clearly a manufacturing defect .  
  I have further complaints with respect to this machine as compared to the Breadman Ultimate : 1 -RRB- The top of the bread rarely gets brown .  
  I 've added more sugar to the recipe which helps slightly but not much .  
  2 -RRB- I do n't understand what the crust control option does .  
  It does n't seem to increase baking time {Exp_8_Arg1 which it does on the Breadman Exp_8_Arg1} {Exp_8_conn_Cause so Exp_8_conn} {Exp_8_Arg2 it does n't make much sense Exp_8_Arg2} .  
  3 -RRB- The programmability is very weak .  
  On the Breadman you can take any of the existing standard options and modify them as well as make up entirely new programs .  
  4 -RRB- The programmability options are very limited : - You ca n't control the knockdown time between risings .  
  - You ca n't add risings to the default three .  
  They do give you a lot of time control on the risings .  
  - You ca n't separate the mixing cycle from the kneading cycle : it 's all one cycle .  
 {Exp_10_Arg1 {Exp_9_Arg1 - {Exp_9_conn_Synchrony When Exp_9_conn} {Exp_9_Arg2 you turn off the preheat cycle Exp_9_Arg2} , this only affects the built-in programs and not the custom ones . Exp_9_Arg1} Exp_10_Arg1} 
 {Exp_10_Arg2 - {Exp_10_conn_Conjunction And Exp_10_conn} worst of all : you ca n't bake for more than 70 minutes . Exp_10_Arg2} 
  This is utterly ridiculous .  
  {Exp_11_Arg1 I 'm sure this is a '' safety feature '' {Exp_11_conn_Contrast but Exp_11_conn} {Exp_11_Arg2 it means you ca n't correct for the pathetic lack of crust formation Exp_11_Arg2} or have larger recipes and so forth . Exp_11_Arg1}  
  This is almost a killer limitation in my opinion .  
  For those other reviewers that are having issues with rising , I 've determined that the machine is very sensitive to the amount , type , and quality of yeast .  
  {Exp_12_conn_Condition If Exp_12_conn} {Exp_12_Arg2 you \* exactly \* Exp_12_Arg2} {Exp_12_Arg1 follow the manufacturers instuctions with respect to yeast and use fresh yeast Exp_12_Arg1} , it should work okay .  
  This means you must use rapid rise yeast with the Quick programs and active dry yeast with the longer standard programs .  
  Even using Fleischmann 's instead of Red Star made a difference .  
  The Breadman was pretty much indifferent to yeast variations .  
  At this point I 'm done with bread machines .  
 {Exp_13_Arg1 They are a failed technology . Exp_13_Arg1} 
 {Exp_13_conn_Conjunction In fact Exp_13_conn} {Exp_13_Arg2 , teflon is a dangerous and substandard technology . Exp_13_Arg2} 
  All you have to do is search for teflon -LRB- Polytetrafluoroethylene -RRB- and toxic to come up with some pretty alarming data on this noxious substance .  
  The manufacturing process results in significant environmental contamination .  
  {Exp_14_Arg1 A direct consumer health risk due to toxic outgassing occurs Exp_14_Arg1} {Exp_14_conn_Condition when Exp_14_conn} {Exp_14_Arg2 teflon is exposed to temperatures above 500 degrees -LRB- eg : frying pans -RRB- . Exp_14_Arg2}  
  {Exp_16_Arg1 Perhaps bread machines are not that risky given the lower temps involved in baking and the fact {Exp_15_Arg1 that wet dough will keep the average pan temperature pretty low , Exp_15_Arg1} {Exp_15_conn_Contrast but Exp_15_conn} {Exp_16_conn_Condition {Exp_15_Arg2 if Exp_16_conn} {Exp_16_Arg2 you know anything about toxicity Exp_15_Arg2} Exp_16_Arg2} you will realize that it is always a sliding scale : there will be some outgassing at lower temperatures . Exp_16_Arg1}  
  Imagine low levels of toxic gas released into your homemade bread .  
  {Exp_17_Arg1 I 've thought about this , Exp_17_Arg1} {Exp_17_conn_Contrast but Exp_17_conn} {Exp_17_Arg2 there 's no way to make a bread machine without teflon Exp_17_Arg2} -- going from mixing dough to baking requires teflon , there is just no other viable solution .  
  {Exp_19_Arg1 {Exp_18_Arg1 I love making bread every day {Exp_18_conn_Conjunction and Exp_18_conn} {Exp_18_Arg2 I 'm going to miss this activity Exp_18_Arg2} Exp_19_Arg1} {Exp_19_conn_Contrast but Exp_19_conn} {Exp_19_Arg2 at this point the manufacturing defects and use of teflon mean I have to give up on this technology . Exp_18_Arg1} Exp_19_Arg2}  
 It 's back to the occasional hand-made loaf -- it 's the only rational and healthy solution . 

