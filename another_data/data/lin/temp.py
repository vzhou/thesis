import os
import itertools
inp = 'featured/texts'

def swith(arg):
  if arg == 'Arg1':
    return 'Arg2'
  elif arg == 'Arg2':
    return 'Arg1'

def onefile(f, path):
  out = 'featured/raw_features'
  fo = open(os.path.join(out,f), 'w')
  #load all the pairs, in order to look up the polarity of the other argument
  index = {}
  f = open(os.path.join(path,f)).readlines()
  for line in f:
    entry = line.strip().split('\t')
    relations = entry[2:]
    if len(relations)!=0:
      for r in relations:
        temp_relation_index = r.split('_')[0]
        index[temp_relation_index] = index.get(temp_relation_index,{})
        index[temp_relation_index][r.split('_')[1]] = entry[1] #(arg1/2: sentiment)

  for line in f:
    entry = line.strip().split('\t')
    relations = entry[2:]
    features = []
    if len(relations)!=0:
      for r in relations:
        if index.get(r.split('_')[0]).get(swith(r.split('_')[1])) != None:
          features.append('%s_%s_%s'%(r.split('_')[1],r.split('_')[2],index.get(r.split('_')[0]).get(swith(r.split('_')[1]))))
    if len(features)==0:
      features.append('non')
    fo.write('%s,%s\n'%(','.join(features), entry[1]))
  fo.close()


def allfile():
  global inp
  for fil in os.listdir(inp):
    print fil
    onefile(fil, inp)


def summarize():
  sourcep = 'featured/raw_features'
  lexi = '../lexico-summed'
  adjacecnt = '../adjacent/adjacent_2'
  target = open('../../result/lin-gold', 'w')
  
  for fil in os.listdir(sourcep):
    fs = open(os.path.join(sourcep,fil)).readlines()
    fl = open(os.path.join(lexi,fil)).readlines()
    fa = open(os.path.join(adjacecnt,fil)).readlines()
    for i in range(len(fs)):
      temp = []
      temp.extend(fa[i].split(',')[:4])
      temp.append(fl[i].split('\t')[1])
      temp.extend(fs[i].split(','))
      target.write(','.join(temp))

def wekaize():
  source = open('/home/eastdog/thesis/another_data/result/lin-gold').readlines()
  out = open('/home/eastdog/thesis/another_data/result/lin-gold.arff', 'w')
  allrelation = [line.split(',')[5:-1] for line in source]
  allrelation = set(itertools.chain(*allrelation))
  index = dict(zip(allrelation,range(len(allrelation))))
  total = len(allrelation)+5

  out.write('@RELATION EDU_polarity_prediction\n\n')
  out.write('@ATTRIBUTE prev1 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE prev2 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE next1 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE next2 {neg,pos,neu,Start,End}\n')
  out.write('@ATTRIBUTE lexi NUMERIC\n')
  for i in range(len(allrelation)):
    out.write('@ATTRIBUTE pdtb%s NUMERIC\n'%i)
  out.write('@ATTRIBUTE class {pos,neg,neu}')
  out.write('\n@DATA\n')

  for line in source:
    entry = line.split(',')
    relations = entry[5:-1]
    relationindexs = sorted(list(set([index[r]+5 for r in relations])))
    relationindexs = ['%s %s'%(item,1) for item in relationindexs]
    pdtb = ','.join(relationindexs)
    rest = ['%s %s'%((i,entry[i])) for i in range(5)]
    rest = ','.join(rest)
    out.write('{%s,%s,%s %s}\n'%(rest,pdtb,total,entry[-1].strip()))


wekaize()
