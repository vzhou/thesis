package org.zirn.sentimentAnalyzer.modules.mmax;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.eml.MMAX2.annotation.markables.Markable;
import org.eml.MMAX2.annotation.markables.MarkableLevel;
import org.eml.MMAX2.discourse.MMAX2Discourse;

import edu.stanford.nlp.process.PTBTokenizer;

/**
 * Reads in mmax file and reads annotations from it, creates goldStandards for all reviews contained in this file
 * (normally: one file positive reviews, one file containing negative reviews)
 * 
 * public GoldStandardOneReview[] gs: contains GoldStandards of all Reviews as Array
 * 
 * @author czirn
 *
 */
public class GoldStandardSeparated {
	
	static final  Logger logger = Logger.getLogger(GoldStandardSeparated.class); //log4j
	
	
	//MMAX
	String mmaxPath;
	String projectName;

	//via this variable the goldStandard information is accessible
	public GoldStandardOneReview[] gs; //each element contains gold standard (tokens + polarities) for 1 review
	
	
	
	public GoldStandardSeparated (String mmaxPathIn, String rawProjectName){
		
		
		MMAX2Discourse discourse;
		MarkableLevel polarityLevelGold;
		MarkableLevel wordLevel; 
		MarkableLevel reviewLevel;
		
		
		this.mmaxPath = mmaxPathIn;
		//this.projectName = "baby_negative.mmax";
		this.projectName = rawProjectName + ".mmax";
		
		//access mmax annotated files
		discourse = MMAX2Discourse.buildDiscourse(mmaxPath+ projectName);
		polarityLevelGold = discourse.getMarkableLevelByName("polarityGold", false);
		reviewLevel = discourse.getMarkableLevelByName("review", false);
		wordLevel = discourse.getMarkableLevelByName("word", false);  
		
	    //get all reviews and iterator for them
	    ArrayList<Markable> reviewMarkables = reviewLevel.getMarkables(new ReviewLengthComparator());
	    Iterator<Markable> reviewsIt = reviewMarkables.iterator();

	    
	    String tokenBuf = "";
	    String polarityBuf = "";
		Vector<String> tokensVectorBuf = new Vector<String>();
		Vector<String> polaritiesVectorBuf = new Vector<String>();
		String[] tokensArrayBuf; // tokensVectorBuf -> tokensVectorBufArray
		String[] polaritiesArrayBuf; // polarityVectorBuf -> polarityVectorBufArray
		//collects gold standards of all reviews, will be converted to GoldStandardOneReview[] gs
		Vector<GoldStandardOneReview> gsBuf = new Vector<GoldStandardOneReview>(); 

		/*
		 * ITERATE OVER REVIEWS (= review markables), COLLECT POLARITY ANNOTATIONS
		 */
	    
	    while (reviewsIt.hasNext()){
	    	Markable m = reviewsIt.next();
	    	String[] markableIdsInReview = m.getDiscourseElementIDs();
	    	
	    	//iterate over element-IDs within review
	    	for (int j = 0; j < markableIdsInReview.length; j ++){
	    		//get polarities for element
	    		Markable[] markPolarGold = polarityLevelGold.getAllMarkablesAtDiscourseElement(markableIdsInReview[j], true);
	    		//iterate over markables of one element
	    		//i < 1 because there are 2 markables at this position,one as word markable and one as polarity markable (but markable is the same) 
	    		if (markPolarGold.length > 0){
			    	for (int i = 0; i < 1; i++){
			    	
		    			logger.debug("\t\tMARKABLE STRING: " + markPolarGold[i].toString());
		    			
		    			
			        	//read markable and remove brackets
			        	tokenBuf = markPolarGold[i].toString().replace("[","").replace("]",""); 

	
			        	//get value (polarity)  for markable
			        	polarityBuf = markPolarGold[i].getAttributeValue("polarity"); 
			        	
			        	//tokenize markable with stanford tokenizer ("don't" ->  "do" "n't")
			        	PTBTokenizer tokenizer = PTBTokenizer.newPTBTokenizer(new StringReader(tokenBuf)); 
			    		while (tokenizer.hasNext()){
			    			
			    			String buf = tokenizer.next().toString();

	
			    				tokensVectorBuf.add(buf); //save token in array(tokenizer.next());
			    				polaritiesVectorBuf.add(polarityBuf);

			    		} //end while

			    	}

	    		}
	    	}  	
	    	
	    	//tokens and polarities that were collected for one review are added to accessible goldStandard
	    	// convert collected vectors to String[]:
	    	tokensArrayBuf = tokensVectorBuf.toArray(new String[tokensVectorBuf.size()]);
	    	polaritiesArrayBuf = polaritiesVectorBuf.toArray(new String[polaritiesVectorBuf.size()]);
	    	GoldStandardOneReview gsorBuf = new GoldStandardOneReview(tokensArrayBuf,polaritiesArrayBuf);
	    	gsBuf.add(gsorBuf);  
	    } //end iterate over reviews
	    
	    
	    //create array, length = amount of reviews
	    this.gs = gsBuf.toArray( new GoldStandardOneReview[gsBuf.size()]);
	    
	    logger.debug("Amount of reviews: " + gs.length);
	} //end  GoldStandardSeparated 
	
	
	
	
	public GoldStandardOneReview[] getGs() {
		return gs;
	}

	public void setGs(GoldStandardOneReview[] gs) {
		this.gs = gs;
	}




	public static void main(String[] args){
		
		System.out.println("GoldStandard");
		String path ="/home/czirn/SentimentAnalysis/Masterthesis";
		String goldStandardPath = path + "/MMAX_Projects/Sentiment_GOLDSTANDARD/";
		String goldProjectNamePositive = "gourmetfood_positive";
		
		GoldStandardSeparated gs = new GoldStandardSeparated(goldStandardPath,goldProjectNamePositive);
	}
}
