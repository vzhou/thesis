(Elaboration[N][S]
  (Evaluation[N][S]
    (Elaboration[N][S]
      (Condition[S][N]
        What an awesome product ! Heavy-duty and sturdy ,
        you 'd never know it was an " assemble-at-home " item .)
      (Elaboration[N][S]
        It 's a quality piece of furniture .
        (Elaboration[N][S]
          (Elaboration[N][S]
            It 's on wheels ,
            (Explanation[N][S]
              which is a good thing
              because it is surprisingly heavy .))
          (Elaboration[N][S]
            (Elaboration[N][S]
              Let 's start at the top :
              The butcher block top .)
            (Elaboration[N][S]
              (Contrast[S][N]
                (same-unit[N][N]
                  (Joint[N][N]
                    It 's three inc
                    hes thick and ,)
                  as such ,)
                it is the heaviest part of the entire kitchen cart .)
              (Elaboration[N][S]
                It is made of fine , high-quality Asian hardwoods .
                (Elaboration[N][S]
                  The finish is oiled and the manufacturer recommends seasoning it regularly with mineral oil .
                  (Evaluation[N][S]
                    (Elaboration[N][S]
                      The next is the cart itself .
                      Immediately under the butcher block top ,)
                    (Elaboration[N][S]
                      there is a 2 " dee
                      p drawer .)))))))))
    (Elaboration[N][S]
      (Elaboration[N][S]
        (Enablement[N][S]
          It 's large and deep enough
          to accommodate a standard utensil holder or other items of your choice .)
        (Elaboration[N][S]
          The pull handle on the drawer
          is fashioned out of brushed chrome .))
      (Elaboration[N][S]
        (Elaboration[N][S]
          (Background[S][N]
            Beneath the drawer 
            , there is a shelf .)
          (Elaboration[N][S]
            (Elaboration[N][S]
              Surrounding the shelf on three sides are brushed chrome rods ,
              adding a modern look .)
            (Elaboration[N][S]
              Matching the brushed chrome are three hooks on the right side of the cart , perfect for hanging larger utensils or pot holders .
              (Elaboration[N][S]
                Below the shelf is a cabinet with two doors .
                (Elaboration[N][S]
                  The pull handles for the doors also have a brushed chrome finish .
                  (Elaboration[N][S]
                    (Elaboration[N][S]
                      (Elaboration[N][S]
                        The cabinet is large enough
                        to hold an entire set of dinnerware)
                      (Elaboration[N][S]
                        ( as in my case
                        where I have eight place settings))
                    that include dinner and luncheon plates , fruit and soup bowls , and more ) .))))))
        (Elaboration[N][S]
          (Elaboration[N][S]
            Finally , heavy-duty , professional grade caster wheels support the whole thing .
            (Elaboration[N][S]
              The two in the front are locking .
              (Elaboration[N][S]
                To prevent stubbed toes , the wheels can easily be rotated and tucked under the unit .
                (Enablement[N][S]
                  (Elaboration[N][S]
                    (Elaboration[N][S]
                      The easy to follow directions for assembly
                      ()
                    (Elaboration[N][S]
                      which are diagrammed and in English )
                      call))
                  only for a Philips screwdriver .))))
          (Elaboration[N][S]
            (Elaboration[N][S]
              A hex wrench is included for the 18 hex bolts .
              (Elaboration[N][S]
                (Elaboration[N][S]
                  (Background[N][S]
                    (same-unit[N][N]
                      (Summary[N][S]
                        I used an electric screwdriver for both the Philips screws
                        ( 14 in all ))
                      (Elaboration[N][S]
                        and the he
                        x bolts ())
                    (Attribution[S][N]
                      after I found
                      the right sized hex tip for my screwdriver ) .))
                  (Attribution[S][N]
                    I can happily report
                    (Elaboration[N][S]
                      that all pieces fit together easily
                      ( no struggling to match up this piece with that piece ))))
                and all holes are pre-drilled .))
            (Evaluation[N][S]
              (Elaboration[N][S]
                The drawer requires assembly also .
                (Elaboration[N][S]
                  (Elaboration[N][S]
                    (Elaboration[N][S]
                      The runners for the drawer are already attached !
                      You)
                    (Elaboration[N][S]
                      must put together the front , sides and back of the drawer with screws
                      ( again , all holes are pre-drilled ) .))
                  (Elaboration[N][S]
                    (same-unit[N][N]
                      (Elaboration[N][S]
                        A word of adv
                        ice regarding)
                      (Elaboration[N][S]
                        assembling the drawer :
                        (Elaboration[N][S]
                          Follow the directions
                          (Enablement[N][S]
                            where it says
                            (Enablement[N][S]
                              to put the screws in only halfway
                              before trying to attach the bottom of the drawer .)))))
                    (Elaboration[N][S]
                      (Elaboration[N][S]
                        I made the mistake of tightening the screws too much and had trouble
                        sliding the bottom into the assembled front , back and sides .)
                      (Elaboration[N][S]
                        (Elaboration[N][S]
                          It 's a snug fit .
                          (Elaboration[N][S]
                            (Joint[N][N]
                              Once I loosened the screws enough , the bottom easily popped into place
                              and I was able to sock it in securely by tightening the screws .)
                            (Elaboration[N][S]
                              (Elaboration[N][S]
                                (Elaboration[N][S]
                                  I have only one other thing
                                  to mention regarding the assembly of the entire cart :)
                                (same-unit[N][N]
                                  (Background[S][N]
                                    Once the body of the cart is assembled and the unit is standing upright ,
                                    you)
                                  (Joint[N][N]
                                    must lift the butcher block
                                    and set it down on the top of the cart .)))
                              (Joint[N][N]
                                (Elaboration[N][S]
                                  At this point ,
                                  you have already placed four cam lock screws on the top of the cart)
                                and they 're pointing upward .))))
                        (Elaboration[N][S]
                          These screws will fit into four pre-drilled holes on the bottom of the butcher block .
                          (Joint[N][N]
                            (Explanation[N][S]
                              You must align the screws on the cart with the holes on the butcher block as you place the butcher block on top of the cart .
                              Because the butcher block is so heavy ,)
                            you may want to have help .)))))))
              (Elaboration[N][S]
                (Elaboration[N][S]
                  But what a reward it was to see the unit fully assembled and then to marvel at what a quality piece of furniture
                  it ended up being .)
                IKEA sells a similar item for twice the money .)))))))
  (Explanation[N][S]
    I give the Home Styles Double Door Kitchen Cart with Butcher Block Top 5 stars
    because of the beauty of the product and its better-than-a-bargain price .))