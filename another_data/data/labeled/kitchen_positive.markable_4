What an awesome product ! Heavy-duty and sturdy ,	pos
you 'd never know it was an " assemble-at-home " item .	pos
It 's a quality piece of furniture .	pos
It 's on wheels ,	pos
which is a good thing	pos
because it is surprisingly heavy .	neg
Let 's start at the top :	neu
The butcher block top .	neu
It 's three inches thick	pos
and ,	pos
as such ,	pos
it is the heaviest part of the entire kitchen cart .	pos
It is made of fine , high-quality Asian hardwoods .	pos
The finish is oiled and the manufacturer recommends seasoning it regularly with mineral oil .	neu
The next is the cart itself .	neu
Immediately under the butcher block top ,	neu
there is a 2 "	neu
deep drawer .	neu
It 's large and deep enough	pos
to accommodate a standard utensil holder or other items of your choice .	pos
The pull handle on the drawer	neu
is fashioned out of brushed chrome .	neu
Beneath the drawer ,	neu
there is a shelf .	neu
Surrounding the shelf on three sides are brushed chrome rods ,	pos
adding a modern look .	pos
Matching the brushed chrome are three hooks on the right side of the cart , perfect for hanging larger utensils or pot holders .	pos
Below the shelf is a cabinet with two doors .	neu
The pull handles for the doors also have a brushed chrome finish .	pos
The cabinet is large enough	pos
to hold an entire set of dinnerware	pos
( as in my case	pos
where I have eight place settings	pos
that include dinner and luncheon plates , fruit and soup bowls , and more ) .	pos
Finally , heavy-duty , professional grade caster wheels support the whole thing .	pos
The two in the front are locking .	pos
To prevent stubbed toes , the wheels can easily be rotated and tucked under the unit .	pos
The easy to follow directions for assembly	pos
(	pos
which are diagrammed and in English )	pos
call	pos
only for a Philips screwdriver .	pos
A hex wrench is included for the 18 hex bolts .	pos
I used an electric screwdriver for both the Philips screws	neu
( 14 in all )	neu
and the hex bolts	neu
(	neu
after I found	neu
the right sized hex tip for my screwdriver ) .	neu
I can happily report	pos
that all pieces fit together easily	pos
( no struggling to match up this piece with that piece )	pos
and all holes are pre-drilled .	pos
The drawer requires assembly also .	neu
The runners for the drawer are already attached !	pos
You	pos
must put together the front , sides and back of the drawer with screws	pos
( again , all holes are pre-drilled ) .	pos
A word of advice	neu
regarding	neu
assembling the drawer :	neu
Follow the directions	neu
where it says	neu
to put the screws in only halfway	neu
before trying to attach the bottom of the drawer .	neu
I made the mistake of tightening the screws too much and had trouble	neu
sliding the bottom into the assembled front , back and sides .	neu
It 's a snug fit .	pos
Once I loosened the screws enough , the bottom easily popped into place	pos
and I was able to sock it in securely by tightening the screws .	pos
I have only one other thing	neu
to mention regarding the assembly of the entire cart :	neu
Once the body of the cart is assembled and the unit is standing upright ,	neu
you	neu
must lift the butcher block	neu
and set it down on the top of the cart .	neu
At this point ,	neu
you have already placed four cam lock screws on the top of the cart	neu
and they 're pointing upward .	neu
These screws will fit into four pre-drilled holes on the bottom of the butcher block .	neu
You must align the screws on the cart with the holes on the butcher block as you place the butcher block on top of the cart .	neu
Because the butcher block is so heavy ,	neg
you may want to have help .	neg
But what a reward it was to see the unit fully assembled and then to marvel at what a quality piece of furniture	pos
it ended up being .	pos
IKEA sells a similar item for twice the money .	neg
I give the Home Styles Double Door Kitchen Cart with Butcher Block Top 5 stars	pos
because of the beauty of the product and its better-than-a-bargain price .	pos
