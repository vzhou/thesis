\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Background}{5}
\contentsline {subsection}{\numberline {2.1}Discourse Structure Theory}{5}
\contentsline {subsubsection}{\numberline {2.1.1}Rhetorical Structure Theory (RST)}{5}
\contentsline {subsubsection}{\numberline {2.1.2}The Penn Discourse Treebank (PDTB)}{8}
\contentsline {subsection}{\numberline {2.2}Discourse Parsers}{9}
\contentsline {subsection}{\numberline {2.3}Sentiment Analysis}{11}
\contentsline {subsubsection}{\numberline {2.3.1}Sentiment Analysis: Approaches}{11}
\contentsline {subsubsection}{\numberline {2.3.2}Sentiment Analysis with discourse features}{12}
\contentsline {subsubsection}{\numberline {2.3.3}Aspect-based Sentiment Analysis}{13}
\contentsline {subsection}{\numberline {2.4}Summary}{14}
\contentsline {section}{\numberline {3}Datasets and resources}{15}
\contentsline {subsection}{\numberline {3.1}Datasets}{15}
\contentsline {subsubsection}{\numberline {3.1.1}TripAdvisor Dataset}{15}
\contentsline {subsubsection}{\numberline {3.1.2}Multi-Domain-Sentiment Dataset}{16}
\contentsline {subsection}{\numberline {3.2}Lexical Resources}{18}
\contentsline {subsection}{\numberline {3.3}Summary}{19}
\contentsline {section}{\numberline {4}Sentiment Classification}{20}
\contentsline {subsection}{\numberline {4.1}Lexicon-based features}{20}
\contentsline {subsection}{\numberline {4.2}Simple lexical score}{20}
\contentsline {subsubsection}{\numberline {4.2.1}Relevant aspect classification}{21}
\contentsline {subsection}{\numberline {4.3}Adjacency-based features}{22}
\contentsline {subsection}{\numberline {4.4}RST-based discourse features}{23}
\contentsline {subsubsection}{\numberline {4.4.1}Relation Representations}{24}
\contentsline {paragraph}{Direct relation representation}{24}
\contentsline {paragraph}{Longer path representation}{24}
\contentsline {subsubsection}{\numberline {4.4.2}Models}{28}
\contentsline {paragraph}{Full path model}{28}
\contentsline {paragraph}{Path length controlled model}{28}
\contentsline {subsection}{\numberline {4.5}PDTB-based discourse features}{29}
\contentsline {subsubsection}{\numberline {4.5.1}EDU Segmentation}{29}
\contentsline {subsubsection}{\numberline {4.5.2}Feature Modelling}{31}
\contentsline {subsection}{\numberline {4.6}Aspect shift}{32}
\contentsline {subsection}{\numberline {4.7}Summary}{32}
\contentsline {section}{\numberline {5}Experiments}{33}
\contentsline {subsection}{\numberline {5.1}Experiment Settings}{33}
\contentsline {subsection}{\numberline {5.2}Statistics}{33}
\contentsline {subsection}{\numberline {5.3}Evaluation}{35}
\contentsline {subsubsection}{\numberline {5.3.1}Lexicon-based Baseline}{35}
\contentsline {subsubsection}{\numberline {5.3.2}Adjacency-based Baseline}{36}
\contentsline {subsubsection}{\numberline {5.3.3}RST based}{37}
\contentsline {subsubsection}{\numberline {5.3.4}PDTB based}{39}
\contentsline {subsubsection}{\numberline {5.3.5}Comparison: RST v.s. PDTB}{39}
\contentsline {subsection}{\numberline {5.4}Summary}{40}
\contentsline {section}{\numberline {6}Conclusions and Future work}{41}
