\begin{thebibliography}{}

\bibitem[Brody and Elhadad, 2010]{brody2010unsupervised}
Brody, S. and Elhadad, N. (2010).
\newblock An unsupervised aspect-sentiment model for online reviews.
\newblock In {\em Human Language Technologies: The 2010 Annual Conference of
  the North American Chapter of the Association for Computational Linguistics},
  pages 804--812. Association for Computational Linguistics.

\bibitem[Carlson et~al., 2003]{carlson2003building}
Carlson, L., Marcu, D., and Okurowski, M.~E. (2003).
\newblock {\em Building a discourse-tagged corpus in the framework of
  rhetorical structure theory}.
\newblock Springer.

\bibitem[Das and Chen, 2001]{das2001yahoo}
Das, S. and Chen, M. (2001).
\newblock Yahoo! for amazon: Extracting market sentiment from stock message
  boards.
\newblock In {\em Proceedings of the Asia Pacific Finance Association Annual
  Conference (APFA)}, volume~35, page~43.

\bibitem[De~Marneffe et~al., 2006]{de2006generating}
De~Marneffe, M.-C., MacCartney, B., Manning, C.~D., et~al. (2006).
\newblock Generating typed dependency parses from phrase structure parses.
\newblock In {\em Proceedings of LREC}, volume~6, pages 449--454.

\bibitem[Feng and Hirst, 2012]{feng2012text}
Feng, V.~W. and Hirst, G. (2012).
\newblock Text-level discourse parsing with rich linguistic features.
\newblock In {\em Proceedings of the 50th Annual Meeting of the Association for
  Computational Linguistics: Long Papers-Volume 1}, pages 60--68. Association
  for Computational Linguistics.

\bibitem[Hall et~al., 2009]{hall2009weka}
Hall, M., Frank, E., Holmes, G., Pfahringer, B., Reutemann, P., and Witten,
  I.~H. (2009).
\newblock The weka data mining software: an update.
\newblock {\em ACM SIGKDD Explorations Newsletter}, 11(1):10--18.

\bibitem[Heerschop et~al., 2011]{heerschop2011polarity}
Heerschop, B., Goossen, F., Hogenboom, A., Frasincar, F., Kaymak, U., and
  de~Jong, F. (2011).
\newblock Polarity analysis of texts using discourse structure.
\newblock In {\em Proceedings of the 20th ACM international conference on
  Information and knowledge management}, pages 1061--1070. ACM.

\bibitem[Hernault et~al., 2010]{hernault2010hilda}
Hernault, H., Prendinger, H., Ishizuka, M., et~al. (2010).
\newblock Hilda: a discourse parser using support vector machine
  classification.
\newblock {\em Dialogue \& Discourse}, 1(3).

\bibitem[Horrigan, 2008]{horrigan2008online}
Horrigan, J.~A. (2008).
\newblock Online shopping.
\newblock {\em Pew Internet \& American Life Project Report}, 36.

\bibitem[Hu and Liu, 2004]{hu2004mining}
Hu, M. and Liu, B. (2004).
\newblock Mining and summarizing customer reviews.
\newblock In {\em Proceedings of the tenth ACM SIGKDD international conference
  on Knowledge discovery and data mining}, pages 168--177. ACM.

\bibitem[Lazaridou et~al., 2013]{kozhevnikov-titov:2013:ACL2013}
Lazaridou, A., Titov, I., and Sporleder, C. (2013).
\newblock A bayesian model for joint unsupervised induction of sentiment,
  aspect and discourse representations.
\newblock In {\em To Appear in Proceedings of the 51th Annual Meeting of the
  Association for Computational Linguistics}, Sofia, Bulgaria. Association for
  Computational Linguistics.

\bibitem[le~Cessie and van Houwelingen, 1992]{leCessie1992}
le~Cessie, S. and van Houwelingen, J. (1992).
\newblock Ridge estimators in logistic regression.
\newblock {\em Applied Statistics}, 41(1):191--201.

\bibitem[Lin et~al., 2010]{lin2010pdtb}
Lin, Z., Ng, H.~T., and Kan, M.-Y. (2010).
\newblock A pdtb-styled end-to-end discourse parser.
\newblock Technical report, Cambridge Univ Press.

\bibitem[Mann and Thompson, 1988]{mann1988rhetorical}
Mann, W.~C. and Thompson, S.~A. (1988).
\newblock Rhetorical structure theory: Toward a functional theory of text
  organization.
\newblock {\em Text}, 8(3):243--281.

\bibitem[McNemar, 1947]{mcnemar1947note}
McNemar, Q. (1947).
\newblock Note on the sampling error of the difference between correlated
  proportions or percentages.
\newblock {\em Psychometrika}, 12(2):153--157.

\bibitem[Nielsen, 2011]{nielsen2011new}
Nielsen, F.~{\AA}. (2011).
\newblock A new anew: Evaluation of a word list for sentiment analysis in
  microblogs.
\newblock {\em arXiv preprint arXiv:1103.2903}.

\bibitem[Polanyi and Zaenen, 2006]{polanyi2006contextual}
Polanyi, L. and Zaenen, A. (2006).
\newblock Contextual valence shifters.
\newblock In {\em Computing attitude and affect in text: Theory and
  applications}, pages 1--10. Springer.

\bibitem[Prasad et~al., 2008]{prasad2008penn}
Prasad, R., Dinesh, N., Lee, A., Miltsakaki, E., Robaldo, L., Joshi, A.~K., and
  Webber, B.~L. (2008).
\newblock The penn discourse treebank 2.0.
\newblock In {\em LREC}. Citeseer.

\bibitem[Prasad et~al., 2007]{prasad2007penn}
Prasad, R., Miltsakaki, E., Dinesh, N., Lee, A., Joshi, A., Robaldo, L., and
  Webber, B.~L. (2007).
\newblock The penn discourse treebank 2.0 annotation manual.

\bibitem[Somasundaran et~al., 2009]{somasundaran2009supervised}
Somasundaran, S., Namata, G., Wiebe, J., and Getoor, L. (2009).
\newblock Supervised and unsupervised methods in employing discourse relations
  for improving opinion polarity classification.
\newblock In {\em Proceedings of the 2009 Conference on Empirical Methods in
  Natural Language Processing: Volume 1-Volume 1}, pages 170--179. Association
  for Computational Linguistics.

\bibitem[Soricut and Marcu, 2003]{soricut2003sentence}
Soricut, R. and Marcu, D. (2003).
\newblock Sentence level discourse parsing using syntactic and lexical
  information.
\newblock In {\em Proceedings of the 2003 Conference of the North American
  Chapter of the Association for Computational Linguistics on Human Language
  Technology-Volume 1}, pages 149--156. Association for Computational
  Linguistics.

\bibitem[Taboada et~al., 2009]{taboada2009genre}
Taboada, M., Brooke, J., and Stede, M. (2009).
\newblock Genre-based paragraph classification for sentiment analysis.
\newblock In {\em Proceedings of the SIGDIAL 2009 Conference: The 10th Annual
  Meeting of the Special Interest Group on Discourse and Dialogue}, pages
  62--70. Association for Computational Linguistics.

\bibitem[Taboada et~al., 2008]{taboada2008extracting}
Taboada, M., Voll, K., and Brooke, J. (2008).
\newblock Extracting sentiment as a function of discourse structure and
  topicality.
\newblock {\em Simon Fraser Univeristy School of Computing Science Technical
  Report}.

\bibitem[Tong, 2001]{tong2001operational}
Tong, R.~M. (2001).
\newblock An operational system for detecting and tracking opinions in on-line
  discussion.
\newblock In {\em Working Notes of the ACM SIGIR 2001 Workshop on Operational
  Text Classification}, volume~1, page~6.

\bibitem[Wang et~al., 2010]{wang2010latent}
Wang, H., Lu, Y., and Zhai, C. (2010).
\newblock Latent aspect rating analysis on review text data: a rating
  regression approach.
\newblock In {\em Proceedings of the 16th ACM SIGKDD international conference
  on Knowledge discovery and data mining}, pages 783--792. ACM.

\bibitem[Webber et~al., 2012]{webber2012discourse}
Webber, B., Egg, M., and Kordoni, V. (2012).
\newblock Discourse structure and language technology.
\newblock {\em Natural Language Engineering}, 18(4):437--490.

\bibitem[Wilson et~al., 2005]{wilson2005opinionfinder}
Wilson, T., Hoffmann, P., Somasundaran, S., Kessler, J., Wiebe, J., Choi, Y.,
  Cardie, C., Riloff, E., and Patwardhan, S. (2005).
\newblock Opinionfinder: A system for subjectivity analysis.
\newblock In {\em Proceedings of HLT/EMNLP on Interactive Demonstrations},
  pages 34--35. Association for Computational Linguistics.

\bibitem[Wu and Qiu, 2012]{wu2012exploiting}
Wu, F. W.~Y. and Qiu, L. (2012).
\newblock Exploiting discourse relations for sentiment analysis.

\bibitem[Zhou et~al., 2011]{zhou2011unsupervised}
Zhou, L., Li, B., Gao, W., Wei, Z., and Wong, K.-F. (2011).
\newblock Unsupervised discovery of discourse relations for eliminating
  intra-sentence polarity ambiguities.
\newblock In {\em Proceedings of the Conference on Empirical Methods in Natural
  Language Processing}, pages 162--171. Association for Computational
  Linguistics.

\bibitem[Zirn et~al., 2011]{zirn2011fine}
Zirn, C., Niepert, M., Stuckenschmidt, H., and Strube, M. (2011).
\newblock Fine-grained sentiment analysis with structural features.
\newblock In {\em IJCNLP}, pages 336--344.

\end{thebibliography}
