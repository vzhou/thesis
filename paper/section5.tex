\section{Experiments}
In this chapter we explain the implementation of features introduced in Chapter 4 on two of the datasets. First we introduce the settings of the experiments, including the tool and algorithm used in the experiments. Then we analyse the parsing results, to be aware of the distribution of discourse relations. Finally we introduce the evaluations of different models, respectively two baselines, RST-based models and PDTB-based models.

\subsection{Experiment Settings}
We use \textit{Weka} [\cite{hall2009weka}] to perform our classification task, with a \textit{Logistic Regression} classifier.

Weka (Waikato Environment for Knowledge Analysis) is a popular suite of machine learning software written in Java, developed at the University of Waikato, New Zealand. Weka contains a collection of algorithms for data analysis and model prediction. After modelling the features introduced in Chapter 4, we transform the data into the format that Weka takes and perform the classification task. We use a 10 folders cross-validation and measure the results by accuracy.

The Logistic Regression classifier Weka implemented was based on [\cite{leCessie1992}]. The algorithm builds and uses a multinomial logistic regression model (in our case, three categories classification). A \textit{Logistic} function, which is also referred to as \textit{sigmoid} function, is employed in Logistic Regression classifier. It takes a vector of variables \textbf{x} as input and outputs the probabilities of \textbf{x} to each class. For a binary classification problem (say, two classes y = 0 and y = 1), the probability of given data \textbf{x} belonging to each class is defined as follows where \textbf{w} is the parameter vector:
$$p(y=1|\mathbf{x;w}) = p_1(\mathbf{x}) = \frac{1}{1+e^{-w*x}}$$
$$p(y=0|\mathbf{x;w}) = 1- p_1(\mathbf{x})$$
This classifier could also work for multi-class classification problem. For a k class classification problem, the probability of \textbf{x} belonging to class K is:
$$p(y=K|\mathbf{x})=\frac{1}{1+\sum_{l=1}^{K-1} exp(\mathbf{w_l*\mathbf{x}})}$$


\subsection{Statistics}
In this section we analyse the parsing results of different parsers on our datasets. We would like to see the distribution of different discourse relations among texts from different domains. We take the two different datasets as two domains: the TripAdvisor dataset for hotels reviews; the Multi-Domain-Sentiment dataset as product reviews.

\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline
\textbf{Relation}    & \multicolumn{2}{|l|}{\textbf{TripAdvisor}} & \multicolumn{2}{|l|}{\textbf{Multi-Domain}} \\ \hline
Elaboration          & 881                 & 60.67\%               & 4032                & 62.58\%                \\ \hline
Joint                & 204                 & 14.05\%               & 562                 & 8.72\%                 \\ \hline
Contrast             & 119                 & 8.20\%                & 249                 & 3.86\%                 \\ \hline
Explanation          & 54                  & 3.72\%                & 242                 & 3.76\%                 \\ \hline
Evaluation           & 44                  & 3.03\%                & 167                 & 2.59\%                 \\ \hline
Background           & 37                  & 2.55\%                & 218                 & 3.38\%                 \\ \hline
Enablement           & 22                  & 1.52\%                & 91                  & 1.41\%                 \\ \hline
Condition            & 21                  & 1.45\%                & 80                  & 1.24\%                 \\ \hline
Attribution          & 19                  & 1.31\%                & 428                 & 6.64\%                 \\ \hline
Temporal             & 18                  & 1.24\%                & 68                  & 1.06\%                 \\ \hline
Cause                & 16                  & 1.10\%                & 44                  & 0.68\%                 \\ \hline
same-unit            & 8                   & 0.55\%                & 194                 & 3.01\%                 \\ \hline
Summary              & 7                   & 0.48\%                & 44                  & 0.68\%                 \\ \hline
Comparison           & 2                   & 0.14\%                & 11                  & 0.17\%                 \\ \hline
Topic-Change         & 0                   & 0.00\%                & 1                   & 0.02\%                 \\ \hline
textual-organization & 0                   & 0.00\%                & 2                   & 0.03\%                 \\ \hline
Topic-Comment        & 0                   & 0.00\%                & 1                   & 0.02\%                 \\ \hline
Manner-Means         & 0                   & 0.00\%                & 9                   & 0.14\%                 \\ \hline
\textbf{Sum}         & 1452                &                      & 6443                &                       \\ \hline
\end{tabular}
\caption{RST relation distribution}\label{tab:rstdistr}
\end{table}
Table \ref{tab:rstdistr} shows the relation distributions among two datasets, measured by count and portion, sorted by counts in the TripAdvisor dataset. Both datasets contain over 60 percentage of Elaboration relations. There are also quite many Joint, Contrast, Explanation, Evaluation, Attribution relations. The count rankings of relations between two domains are not so different, except that Attribution relation appears more often in product reviews.

Table \ref{tab:pdtbdistr} shows a similar statistic of the PDTB parsing results. No PDTB relations take more than 50 percents and the distribution is more uniform. Conjunction and Contrast take up the majority relations. 
\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline
\textbf{Explicit Relation}   & \multicolumn{2}{|l|}{\textbf{TripAdvisor}} & \multicolumn{2}{|l|}{\textbf{Multi-Domain}} \\ \hline
Conjunction         & 218                   & 37.14\%             & 510                      & 27.96\%           \\ \hline
Contrast            & 119                   & 20.27\%             & 336                      & 18.42\%           \\ \hline
Cause               & 63                    & 10.73\%             & 255                      & 13.98\%           \\ \hline
Condition           & 57                    & 9.71\%              & 217                      & 11.90\%           \\ \hline
Synchrony           & 53                    & 9.03\%              & 217                      & 11.90\%           \\ \hline
Asynchronous        & 37                    & 6.30\%              & 191                      & 10.47\%           \\ \hline
Concession          & 23                    & 3.92\%              & 36                       & 1.97\%            \\ \hline
Alternative         & 10                    & 1.70\%              & 42                       & 2.30\%            \\ \hline
Restatement         & 3                     & 0.51\%              & 12                       & 0.66\%            \\ \hline
List                & 2                     & 0.34\%              & 1                        & 0.05\%            \\ \hline
Instantiation       & 1                     & 0.17\%              & 5                        & 0.27\%            \\ \hline
Exception           & 1                     & 0.17\%              & 1                        & 0.05\%            \\ \hline
Pragmatic-condition & 0                     & 0.00\%              & 1                        & 0.05\%            \\ \hline
\textbf{Sum}        & \textbf{587}          & \textbf{}          & \textbf{1824}            &                  \\ \hline
\end{tabular}
\caption{PDTB relation distribution}\label{tab:pdtbdistr}
\end{table}

By comparing Table \ref{tab:rstdistr} and Table \ref{tab:pdtbdistr}, we can see the number of relations discovered in the Lin parser is much smaller than the one of the Feng-Hirst parser (around one third). It means the modelling of PDTB is more sparse than the one of RST. The number of PDTB relations is also less than half of the number of EDUs (587 v.s. 1541 for the TripAdvisor data and 1824 v.s. 5677 for the Multi-Domain-Sentiment dataset), which means there are some EDUs that don't involve in any relations (otherwise the number of relations has to be at least half of the number of EDUs).


\subsection{Evaluation}
\subsubsection{Lexicon-based Baseline}
We set up two baselines in our experiments. The first one implements the lexicon-based method (referred as \textit{L}) and the second one takes the lexicon-based method as basis, adding adjacency-based features.

The lexicon-based method assigns each EDU a score from -3 to 3, from most negative to most positive. The classification procedure is simple: if the score is positive number, then classify this EDU as positive; if the score is negative, then classify this EDU as negative; if the score equals to 0, then classify this EDU as neutral. For the TripAdvisor dataset, there are two ways of getting this lexical score, one is the simple score, the other one is the aspect relevant score. They are referred as \textit{L-sp} and \textit{L-rel} respectively. For Multi-Domain-Sentiment dataset, there is no annotations of aspects, so there is only \textit{L-sp} score. The results measured by accuracy is shown in Table \ref{tab:lexicon}. The aspect-relevant method doesn't over-perform the simple score. This might because there are not enough polarity-bearing words in a short text span as EDU, filtering out some by aspects might lead to more EDUs without any sentiment scores. The lexicon-based baseline for the Multi-Domain-Sentiment dataset performs worse than the TripAdvisor dataset, this might because we use the segmentations of the Feng-Hirst parser for the Multi-Domain-Sentiment dataset and the segmentations produced by this parser are usually too short to include some polarity-bearing words. These short EDUs will be classified as \textit{neutral} while in this dataset there are not many true neutral EDUs.

\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Model}}            & \textbf{TripAdvisor} 	& \textbf{Multi-Domain} \\ \hline
L-sp   & 61          & 52           \\ \hline
L-rel  & 50.3        &  -            \\ \hline
\end{tabular}
\caption{Lexicon baseline}\label{tab:lexicon}
\end{table}

\subsubsection{Adjacency-based Baseline}
The adjacency-based method considers the sentiments of previous and next EDU or EDUs. We take two windows size. The Prev2Next2 (stands for adjacent window width 2) model considers two adjacent EDUs both before and after the current EDU (four in total). The Prev1Next1 model, similarly, means adjacent window width 1 with one EDU on each direction of the current EDU. Another method of considering only the previous EDU is tried and named Prev. The value of each feature is the sentiment of corresponding EDU or \textit{'Start'} if it's the first EDU of a review or \textit{End} if it's the last EDU. If the corresponding EDUs are not the starting or ending EDUs, the polarities from the gold standard are used. Table \ref{tab:adjacency_example} shows a mock-up example how the feature representation works.
\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{EDU}} & \textbf{Prev1} & \textbf{Next1} \\ \hline
 EDU1, pos                         & Start          & pos            \\ \hline
 EDU2, pos                         & pos            & neg            \\ \hline
 EDU3, neg                         & pos            & neg            \\ \hline
 EDU4, pos                         & neg            & End            \\ \hline
\end{tabular}
\caption{Adjacency representation}\label{tab:adjacency_example}
\end{table}

The TripAdvisor dataset also has aspect annotations. Some more information about whether there is an aspect shift from the current EDU to previous/next EDUs. For example, if the polarity of the previous EDU is ``pos" and there is an change of aspects, the feature will be valued as \textit{pos-Yes} instead of \textit{pos}. The aspect shift feature is named as \textit{Asp} and it applies to the TripAdvisor dataset only. 

Table \ref{tab:adjacency} shows the accuracy results after adding adjacency-based features. There is an significant improvement for both datasets. The results show that if we take into more consideration of adjacent EDUs, the prediction is more accurate. Adjacency can be taken as a simple idea of measuring how two EDUs are related. Although adjacency doesn't indicate rich linguistic information, it suggests to some degree the continuity of polarity expressions among EDUs. In results of models on the TripAdvisor dataset, adding \textit{Asp} improves the performance, but not significantly. It might be more helpful if the size of the TripAdvisor dataset would be larger.

\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Model}}            & \textbf{TripAdvisor} 	& \textbf{Multi-Domain} \\ \hline
Prev2Next2 + Asp 	& 71.6				& 82.39			\\ \hline
Prev2Next2			& 68.47				& -				\\ \hline
Prev1Next1 + Asp	& 71          		& 82.05        	\\ \hline
Prev1Next1			& 68.86				& -				\\ \hline
Prev + Asp			& 67.75 		    & 80            \\ \hline
Prev1				& 67.3			& -				\\ \hline
\end{tabular}
\caption{Adjacency baseline}\label{tab:adjacency}
\end{table}


\subsubsection{RST based}
There are two main different models for RST based features: full path model (referred as \textit{RST-FP}) and length-controlled model (referred as \textit{RST-LC}). We use the short version of the full paths since the original ones are too sparse to introduce any new information. The length limitation we set in this implementation is 2. In these models, features extracted from discourse structures are added to the baseline features. The results measured by accuracy are shown in Table \ref{tab:rstresult1}.

\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Model}}            & \textbf{TripAdvisor} 	& \textbf{Multi-Domain} \\ \hline
RST-FP + Prev2Next2     & 72.6          & 86.03        \\ \hline
RST-FP + L         		& 69.7          & 83.16        \\ \hline
RST-LC + Prev2Next2     & 68         	& \textbf{86.49 }            \\ \hline
RST-LC + L            	& 63.9          & 84.27             \\ \hline
\end{tabular}
\caption{RST-based features}\label{tab:rstresult1}
\end{table}

The general performance of the models on the TripAdvisor dataset is poor, adding \textit{RST-LC} to \textit{Prev2Next2} even makes the accuracy drop (71.6 to 68). The increase from \textit{Prev2Next2} to \textit{RST-FP+Prev2Next2} is not significant\footnote{We use the McNemar's test [\cite{mcnemar1947note}], a non-parametric statistical test}. We address the non-successful results of the models on this dataset to the small size of the data. There are only 1514 EDUs in this dataset, while the size of the Multi-Domain-Sentiment dataset is more than triple of this. 

The results of the Multi-Domain-Sentiment dataset are more promising. The improvements of \textit{RST-FP + Prev2Next2} and \textit{RST-LC + Prev2Next2} over \textit{Prev2Next2} are extremely significant.  Moreover, \textit{RST-LC + L} is significantly better than \textit{Prev2Next2}. Be reminded that the difference between \textit{RST-FP} and \textit{RST-LC} is that \textit{RST-FP} models all possible discourse relations in a discourse tree while RST-LC restricts that the relations should not be in a too high position in a tree. The fact that \textit{RST-LC + Prev2Next2} over-performs \textit{RST-FP + Prev2Next2} suggests that discourse relations beyond certain level of a discourse tree don't contribute to our sentiment prediction task very much.

Given the positive results from the Multi-Domain-Sentiment dataset, we conduct several more experiments on it. First we try using only features of discourse relations for both models \textit{RST-FP} and \textit{RST-LC} without lexical scores or adjacent EDUs' information attached. Then we try adding a smaller amount of adjacent EDUs, say one previous EDU and one next EDU (Prev1Next1). Finally, we construct another model based on \textit{RST-FP} such that in order to predict the sentiment of the current EDU, we use only relations connecting EDUs that are previous to the current one. This is motivated by the natural progress of reading that people infer the meanings of elusive parts according to what has been read in this article instead of something has not been read so far (although they will be helpful to understand the elusive part). This looking-backward-only model is named as \textit{RST-FPB}. The results measured by accuracy are shown in Table \ref{tab:rstresult2}.

\begin{table}[h]
\centering
\begin{tabular}{|l|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Model}} & \textbf{ Multi-Domain } \\ \hline
RST-FP                               & 82.83                   \\ \hline
RST-LC                               & 83.78                   \\ \hline
\multicolumn{2}{|l|}{}                                         \\ \hline
RST-FP + Prev1Next1                         & 86.05                   \\ \hline
RST-LC + Prev1Next1                         & \textbf{86.31}          \\ \hline
\multicolumn{2}{|l|}{}                                         \\ \hline
RST-FPB + Prev2Next2                        & \textbf{85.24}          \\ \hline
RST-FPB + Prev1Next1                        & 84.83                   \\ \hline
RST-PFB                              & 72.8                    \\ \hline
\end{tabular}
\caption{RST-based features, the Multi-Domain-Sentiment dataset}\label{tab:rstresult2}
\end{table}

Using discourse features only slightly over-performs the \textit{Prev2Next2} baseline, but the increase is not significant. Using less adjacent EDUs doesn't cause significant changes of the results for both\textit{ RST-FP} and\textit{ RST-LC} models. This might suggest that with sufficient discourse information, the further adjacent EDUs are less important. Compared to the discourse features only models, the most adjacent EDUs still play their role though. For the backwards models, \textit{RST-FPB + Prev2Next2} is not as good as \textit{RST-FP + Prev2Next2}, but still significantly better than the adjacent models. We address the reason to the lack of forwards information. It might suggest the influence of sentiment shift works both direction: for two EDUs connected by a discourse relation, we could not only use the sentiment of the former EDU (the EDU that appears in the text before the other one) to predict the latter one, but also use the sentiment of the latter EDU to guess what has been talked about. Besides, since \textit{RST-FPB + Prev2Next2} beats the adjacent baseline, our model of using discourse information should still be applicable in real applications.

\subsubsection{PDTB based}
In out PDTB model, we consider only explicit discourse relations since the Lin parser might not provide reliable enough results of classifying implicit relations. As discussed earlier in this chapter, the coverage of PDTB style discourse relations is not so high that some EDUs don't get involved in any relations. We run the experiments on two models: PDTB discourse features plus the lexical scores (\textit{PDTB + L}), and \textit{PDTB + Prev2Next2}. The results measured by accuracy are shown in Table \ref{tab:pdtbresult}.

\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Model}} 	& \textbf{ TripAdvisor } & \textbf{ Multi-Domain } \\ \hline
PDTB + L                    			& 61.2                   & 64.26                   \\ \hline
PDTB + Prev2Next2                  		& \textbf{68.8}          & \textbf{84.61}          \\ \hline
\end{tabular}
\caption{PDTB-based features}\label{tab:pdtbresult}
\end{table}

In the TripAdvisor dataset, both models don't give us positive results: \textit{PDTB + L} beats the best lexical baseline to a tiny degree, and \textit{PDTB + Prev2Next2} doesn't over-perform \textit{Prev2Next2} model at all. We explain this due to the same reason of the RST models' failure: the dataset is not large enough to recognize the prediction patterns among discourse relations. 

In Multi-Domain-Sentiment dataset, \textit{PDTB + L} beat the lexical baseline, but not the adjacent models. It is understandable that the amount of PDTB discourse features is much smaller than the amount of adjacent features. In this dataset there are 6443 EDUs, but only 1824 PDTB discourse relations. After mapping the EDUs and arguments of PDTB discourse relations, we found 3022 EDUs that are not involved in any relation. The amount of all PDTB discourse features is 3705, compared to the amount of all adjacent features (from model \textit{Prev2Next2}) 25772 (4*6443, since we can extract 2 previous EDUs and 2 next EDUs as features for each EDU). The difference of the feature size is obvious, and we think the relatively low coverage is the reason that \textit{PDTB + L} cannot beat \textit{Prev2Next2}. Meanwhile, \textit{PDTB + Prev2Next2} model considers both sets of features and significantly improves the accuracy over \textit{Prev2Next2} (from 82.38 to 84.61).

\subsubsection{Comparison: RST v.s. PDTB}
In this section we compare the performances of two sets of discourse features based on RST and PDTB respectively. Table \ref{tab:comparison} shows the best and worst performance of each approaches, measured by accuracy. 

\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|}
\hline
             & \textbf{ TripAdvisor } & \textbf{ Multi-Domain } \\ \hline
 RST Best    & 72.6                   & 86.49                   \\ \hline
 PDTB Best   & 68.8                   & 84.61                   \\ \hline
 RST Worst   & 63.9                   & 72.8                    \\ \hline
 PDTB Worst  & 61.2                   & 64.26                   \\ \hline
\end{tabular}
\caption{RST-based features}\label{tab:comparison}
\end{table}

Note that all the worst performance cases are from models using only discourse features. We can conclude that both RST and PDTB discourse features are not capable to work alone, while after feeding additional features such as lexical scores and adjacent EDUs' information the performance usually improves. 

The major difference between RST style and PDTB style is that RST discovers EDUs and constructs a tree structure consisting of these EDUs, while the PDTB focus on discovering the discourse relations whose arguments could contain more than one EDU. Table \ref{tab:comparison} shows that RST-based approach always over-performs PDTB-based approach in general, which suggests that RST as a theory might be more suitable than PDTB in the task of sentiment analysis on EDU level. The reason is likely to be the higher coverage of texts such that one EDU could get involved in at least one discourse relation. It suggests that any text span is a component of an article and the coherence within one article is compact. 

\subsection{Summary}
In this chapter we evaluated the features introduced in Chapter 4, focusing on the evaluation results of our discourse models. We first introduced the settings of the experiments and did some statistics about distribution of discourse relations and EDUs in our parsing results. Our experiments can be divided into two parts: one for two baselines respectively using lexical scores and the polarities of adjacent EDUs; the other one dealing with discourse related features. We've shown that by adding discourse features to baselines the performance would obtain a significant improvement. We also showed that RST style discourse features is generally more useful than PDTB style features in our task of sentiment prediction on EDU level. 



