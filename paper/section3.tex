\section{Datasets and resources}
In this chapter we introduce external resources used in this thesis, including two datasets and three lexical resources. The datasets consist of reviews from different domain such as hostels and products, with sentiment annotations on EDU level. We use the data to test our discourse structure models on the task of sentiment analysis. The three lexical resources contain list of words and their corresponding sentiments. They are used in some procedure of the sentiment prediction task.

\subsection{Datasets}
We extract two annotated datasets from  [\cite{kozhevnikov-titov:2013:ACL2013}] and [\cite{zirn2011fine}]. The former one consists of annotations of both sentiment and aspect for each EDU, while the latter one contains EDU level sentiment annotations only. All annotations were done by teams of respective authors. 

\subsubsection{TripAdvisor Dataset}
[\cite{kozhevnikov-titov:2013:ACL2013}] implemented an unsupervised method, so the original dataset consists of an unlabelled part and a labelled part which was used as the gold standard for evaluation. For our purpose in this work, we take only the labelled part, referred to TripAdvisor Dataset later. 

This TripAdvisor dataset was retrieved from TripAdvisor.com. It consists of 65 reviews (1541 EDUs, EDUs segmented by SLSEG software package\footnote{http://www.sfu.ca/~mtaboada/research/SLSeg.html}). 9 annotators annotated every EDU with the aspect and sentiment it expresses. Annotators need to choose at least one aspect (multi aspects in one EDU is allowed) from a candidate aspect list (aspect label \textit{rest} is used when EDUs don't refer to any aspect or refer to a very rare aspect) and one polarity score from (-1,0,1) standing for negative, neutral, positive respectively. Consider the following examples:
\begin{example}
TripAdvisor Dataset annotations
\begin{itemize}
\item[(a) ]Booked this hotel based on the reviews and the reasonable pricing . 	\emph{\textbf{value}}	\emph{pos}
\item[(b) ]Beautiful setting and excellent service . 	\emph{\textbf{location, service}}	\emph{pos}
\end{itemize}
\end{example}
There are three parts for one item (EDU): the content of the EDU, its aspect(s) and its polarity. Multiple aspects are allowed, for instance in (b) there are two aspects \textit{location} and\textit{ service }since both are mentioned in the EDU.

Table \ref{tab:tripad} and Table \ref{tab:trippd} show the distribution of aspect and polarity in this dataset. 

\begin{table}[h]
\centering
\begin{tabular}{|l|l|}
\hline
\textbf{Aspects } & \textbf{Frequency} \\ \hline
service           & 246                \\ \hline
value             & 55                 \\ \hline
location          & 121                \\ \hline
rooms             & 316                \\ \hline
sleep quality     & 56                 \\ \hline
cleanliness       & 59                 \\ \hline
amenities         & 180                \\ \hline
food              & 81                 \\ \hline
recommendation    & 121                \\ \hline
rest              & 306                \\ \hline
\textbf{Total}    & 1541               \\ \hline
\end{tabular}
\caption{Aspect Distribution, TripAdvisor}\label{tab:tripad}
\end{table}

\begin{table}[h]
\centering
\begin{tabular}{|l|l|}
\hline
\textbf{Polarity} & \textbf{Frequency} \\ \hline
positive        & 577                \\ \hline
negative       & 551                \\ \hline
neutral        & 413                \\ \hline
\textbf{Total} & 1541               \\ \hline
\end{tabular}
\caption{Polarity Distribution, TripAdvisor}\label{tab:trippd}
\end{table}

[\cite{kozhevnikov-titov:2013:ACL2013}] used Cohen's kappa score to measure the inter-annotator agreement(IAA): 0.66 for aspect labelling, 0.70 for the sentiment annotation and 0.61 for the joint task of both annotations.

\subsubsection{Multi-Domain-Sentiment Dataset}
This dataset is rearranged from the dataset used in [\cite{zirn2011fine}](referred as Zirn Dataset), by filtering out duplicated reviews and rearranging the EDU segmentation. The contents of Zirn dataset are retrieved from Amazon\footnote{www.amazon.com}, subdividing into three categories ``Cell Phones \& Services", ``Gourmet Food" and ``Kitchen \& Housewares". Three annotators labelled all \textit{passages} of reviews as positive, negative and neutral where a passage was defined as ``a sequence of words sharing the same opinion". The boundaries of passages were chosen by the annotators independently. Fleiss kappa score was used to measure the inter-annotator agreement, 0.40 to 0.45 for negative reviews (\textit{fair agreement}) and 0.60 to 0.84 for positive reviews (\textit{strong agreement}).

For each word in the corpus, a polarity label was given as follows:
\begin{itemize}
\item[1. ]Find out three polarity labels that three annotators has chosen for the respective passages containing the word.
\item[2. ]If the majority of the three labels is \underline{positive} or \underline{negative}, it is taken as the polarity label of this word;
\item[3. ]Otherwise the general polarity of this entire review is given to the label. 
\end{itemize}

This procedure can be taken in such a way that a polarity label was chosen for each word according to its polarity in the context. Then the polarity of an EDU is defined as the majority label of polarity labels of all tokens in this EDU. By this method, the problem of EDU boundary disagreement could be solved and the rearrangement of EDU segmentation is more flexible. 

We use the segmentation results of the Feng-Hirst parser to obtain the EDUs and use above method to generate the polarity labels for each EDU. For each EDU, a polarity label is attached. (There are no aspect annotations in this dataset, as in the TripAdvisor Dataset). An example of one item in this dataset is given in the following:
\begin{example}
Multi-Domain-Sentiment Dataset Annotation

This is NOT the result of customer abuse but a manufacturing defect.	\emph{\textbf{neg}}
\end{example} 


The Multi-Domain-Sentiment dataset we extracted contains 97 reviews (5677 EDUs in total), 31 under \textit{Cell Phone \& Service} category, 31 under \textit{Gourmet Food} category, 35 under \textit{Kitchen \& Housewares} category. The distribution of polarity counted by number of EDUs can be found in Table \ref{tab:kfcdist}:

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
Category              & positive & negative & neutral & \textbf{  Sum  } \\ \hline
Cell Phones \& Service & 833      & 1093     & 359     & 2285             \\ \hline
Gourmet Food          & 426      & 463      & 244     & 1133             \\ \hline
Kitchen \& Housewares  & 816      & 1000     & 443     & 2259             \\ \hline
\textbf{Sum}          & 2075     & 2556     & 1046    & 5677             \\ \hline
\end{tabular}
\caption{Polarity Distribution, Multi-Domain-Sentiment}\label{tab:kfcdist}
\end{table}



\subsection{Lexical Resources}
There are several publicly available lexical resources for sentiment analysis. These resources provide list of words and their polarities, i.e., whether they are positive or negative or neutral. Some resources provide some additional information such as polarity score that tells how positive or negative a word is. In order to have a wider coverage and a better fitting score, we use three lexical resources. As introduced in Chapter 2, they are referred to AFINN, Hu-Liu and Opinion Finder Lexicon. We use each lexical resource to classify the polarity of an EDU separately, and use a voting schema among these three results to get the final lexical decision.

AFINN [\cite{nielsen2011new}] is an affective lexicon collected by Finn Årup Nielsen. It is developed in the \textit{Responsible Business in the Blogosphere} project whose purpose is ``to investigate how corporate reputations as responsible business are constructed online in virtual social networks". AFINN consists of 2477 English words, originally extracted from Twitter and extended later. Each word is rated by a valence value from -5 to +5.

Hu-Liu is a word list being collected over years starting from [\cite{hu2004mining}] by Minqing Hu and Bing Liu. This list contains two sub-lists, one for positive words and one for negative words. There are around 6800 words in total. The authors also mentioned they included some misspelled words since they appear frequently in social media content.

The Opinion Finder Lexicon provides more detailed information. According to the instructions, there are 6 aspects of descriptions for each word\footnote{Following description taken from official instructions by the developers, a \textit{clue} here refers to one line in this lexicon}:
\begin{quote}
\begin{itemize}
\item[a.] \textit{type - either strongsubj or weaksubj. A clue that is subjective in most context is considered strongly subjective (strongsubj), and those that may only have certain subjective usages are considered weakly subjective (weaksubj).}
\item[b.]\textit{len - length of the clue in words.
	All clues in this file are single words.}
\item[c.]\textit{word1 - token or stem of the clue}
\item[d.]\textit{pos1 - part of speech of the clue, may be anypos (any part of speech)}
\item[e.]\textit{stemmed1 - y (yes) or n (no).
	If stemmed1=y, this means that the
	clue should match all unstemmed variants of the word with the
	corresponding part of speech.  For example, ``abuse, pos1=verb, stemmed1=y", will
	match ``abuses" (verb),``abused" (verb), ``abusing" (verb), but not
	``abuse" (noun) or ``abuses" (noun).}
\item[f.]\textit{priorpolarity - positive, negative, both, neutral
	The prior polarity of the clue.  Out of context, whether the
	clue seem to evoke something positive or something negative.}
\end{itemize}
\end{quote}

In this lexicon there are 8221 \textit{clues} among which there are 6878 words (The difference is due to the fact that same word with different part of speech tags will result in different clues). Although this lexicon provides rich descriptions for each word, we take only two of the subjects: word and priorpolarity. It is found in this lexicon that the priorpolarities of same word with different POS tags are consistent, which makes it unnecessary to distinguish it. We extract from this lexicon a list of (word, priorpolarity) pairs and use it for our lexicon-based features.

\subsection{Summary}
In this chapter we discussed the datasets and additional lexical resources used in this thesis. The TripAdvisor dataset has a smaller size, but contains annotations of aspect information. The Multi-Domain-Sentiment dataset is larger, and includes three different domains. Three additional lexical resources are used to construct reliable lexicons for the lexicon-based approaches of our sentiment prediction task.

