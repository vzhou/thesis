\section{Background}
This chapter introduces the background of theories and research work relevant to this thesis. They are divided into three sections: the first section is about two discourse theories,the second section introduces two state-of-the-art discourse parsers corresponding to mentioned two theories, the third section introduces various research topics and approaches in the field of sentiment analysis. 

\subsection{Discourse Structure Theory}
Discourse analysis is a kind of text analysis beyond sentence level. Generally a discourse consists of a sequence of sentences, although structure within one sentence also matters sometimes. These sentences, or shorter text spans (for example, sub-sentences), are the basic components of discourse. Discourse structure, as stated in \cite{webber2012discourse}, “are the patterns that one sees in multi-sentence (multi-clausal) texts.” Analysing this structure is important to understand information in the text.

There are several theories proposed to model discourse structure. Two of them are involved in this thesis, both of which try to use links known as discourse relations to connect different sentences/clauses/EDUs within same text. Rhetorical Structure Theory (RST) \cite{mann1988rhetorical} was originally developed as part of studies of computer-based text generation, by a team at Information Sciences Institute (part of University of Southern California). It has been commonly used in numerous Natural Language Processing (NLP) tasks, such as Information Extraction, Auto Summarization, etc. The Penn Discourse Treebank (PDTB) \cite{prasad2008penn} is a corpus annotated with information related to discourse structure by Institute for Research in Cognitive Science, University of Pennsylvania. 

\subsubsection{Rhetorical Structure Theory (RST)}
Rhetorical Structure Theory (RST) is a theory describing discourse relations among discourse segments. As introduced in Chapter 1, an EDU is a minimal block of a discourse analysis which is a meaningful, continues expression and has independent functional integrity. Different EDUs followed by some rules constitute a text. These rules or regularities is the coherence of a text. RST is a way to explain the coherence of texts: each EDU has an evident role, the roles are transited through discourse relations.

In RST, two non-overlapping spans of text are connected by a discourse relation with one span has a specific role to the other. The essential one is called \textit{nucleus} and the supporting one is called \textit{satellite}. For instance, in a \textit{Background} relation, text whose understanding is being facilitated is recognized as \textit{Nucleus} and text for facilitating understanding is recognized as \textit{Satellite}. As described in [\cite{mann1988rhetorical}], four fields are required by a relation definition:
\begin{quote}
\textit{1. Constraints on the Nucleus,\\
2. Constraints on the Satellite,\\
3. Constraints on the combination of Nucleus and Satellite,\\
4. The Effect.}
\end{quote}
For example, in the definition of EVIDENCE relation, \textit{constraints on N} include ``Reader might not believe N to a degree satisfactory to Writer"; \textit{constraints on S} require ``The reader believes S or will find it credible"; \textit{constraints on the N + S combination} says ``R's comprehending S increases R's belief of N"; and \textit{the effect} is ``R's belief of N is increased". These definitions of relations are the principles for identifying the relations together with the spans in text. A list of relations defined in RST con be found in Figure \ref{fig:rstrelations}, including 12 groups of relations and possible sub-classes.
\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{rst_relations.png}
\caption{RST relation types}
\label{fig:rstrelations}
\end{figure}

In most cases a relation links two text spans (EDUs), usually adjacent but not obligatory. As introduced, each EDU is distinguished between nucleus and satellite. If the relation doesn't have a particular centering preference, it could be multinuclear also where two text spans are treated equally. For example, Contrast is a multinuclear relation.

Possible RST structures are defined by schemas which are patterns consisting of some text spans, a specification of relations between them, and how nuclei are related to the whole collection. Five kinds of schemas recognized by RST is illustrated in Figure \ref{fig:rstschema}. Horizontal lines stand for text spans, the curves indicate relations with the arrow pointing to nuclei and the straight lines identify nuclei. There is one kind of schema that is not mentioned here, which is a single relation with nucleus and satellite.
\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{rst_schemas.png}
\caption{RST schemas}
\label{fig:rstschema}
\end{figure}

A global RST tree structure is constructed for each document/text. A typical RST tree is constructed as following: leaves of the tree are EDUs; adjacent leaves (usually two, exception can be found) can be connected by a RST discourse relation with nuclei and satellite distinguished and this relation is the node that connects these two EDUs; a node may connect to another node or a leaf; nodes keep joining until one root node is constructed. Figure \ref{fig:rsttree}\footnote{From INTRO TO RST, http://www.sfu.ca/rst/01intro/intro.html} illustrates how RST-style parsing result is organized and represented.

\begin{figure}[ht]
\centering
\includegraphics[width=1\textwidth]{rst_tree.png}
\caption{Rhetorical Structure Theory (RST) Tree}
\label{fig:rsttree}
\end{figure}

\subsubsection{The Penn Discourse Treebank (PDTB)}
The Penn Discourse Treebank (PDTB) is a corpus annotated with information related to discourse structure and discourse semantics, by Institute for Research in Cognitive Science, University of Pennsylvania. The annotation was done on the Wall Street Journal (WSJ) Corpus which contains more than one million words. The PDTB focuses on encoding discourse relations. The PDTB follows a lexically-grounded approach to discover explicit discourse relations. \textit{Discourse connectives} are the necessary lexical items that imply or help identify the discourse relations. Consider the following example:
\begin{example}
\textbf{\emph{Because}} mutual fund trades don’t take effect until the market close,\emph{ these shareholders effectively stayed put}.
\end{example}
The Cause relation can be annotated by marking the discourse connective \textit{Because}.

PTDB takes a binary predicate-argument view of discourse relations. A discourse connective is regarded as a predicate that takes two spans of text as its arguments, namely Arg1 and Arg2. Compared to RST, PDTB annotates local relations instead of tree-level long-distance relations. There is no commitment that a high-level structure can be built based on PDTB annotations of relations and arguments. The PDTB provides a three level hierarchical schema for relations, as shown in Figure \ref{fig:pdtbrelations}. In PDTB there are 14 relation tags, constituting 4 parent classes: Temporal, Comparison, Contingency, Expansion.
\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{pdtb_relations.png}
\caption{PDTB relation types}
\label{fig:pdtbrelations}
\end{figure}

Discourse relations realized by discourse connectives that are drawn from syntactically well-deﬁned classes are called \textit{Explicit} relations and these connectives are also called Explicit connectives. There is no constraint for the argument of explicit connectives: they could be anywhere in the text. Consider the following example. Arg1 is in italics, and Arg2 is in bold. We can observe that Arg1 can appear embedded in Arg2.
\begin{example}
\emph{As an indicator of the tight grain supply situation in the U.S., market analysts said that \textbf{late Tuesday the Chinese government}}, which often buys U.S. grains in quantity, \emph{\textbf{turned} \underline{instead} \textbf{to Britain to buy 500,000 metric tons of wheat.}}
\end{example}

Explicit relations are distinguished from \textit{Implicit} relations which hold between two adjacent sentences (or EDUs) in the absence of explicit connectives. There might be a implicit discourse connective connecting two text spans or none. Consider the following example of an implicit Restatement relation without any connectives connecting two arguments. Arg1 is \textit{italicized} and Arg2 is \textbf{bolded}.
\begin{example}
My wife and I stayed at the Empire Hotel for 8 nights on our honeymoon. \emph{\textbf{We booked a superior hotel and even though it was supposed to be for honeymooners they placed us on the 3rd floor.}}
\end{example}

For both explicit and implicit relations, there exist two and only two arguments. For explicit relations, Arg2 is the argument to which the connective is syntactically bound and Arg1 is the other argument. As for implicit relations, Arg1 and Arg2 are labelled by their linear order in the text. 

\subsection{Discourse Parsers}
In this section we introduce two discourse parsers used in this thesis and how the parser work on our datasets. There are several discourse parsers publicly available, such as HILDA discourse parser [\cite{hernault2010hilda}], The Feng-Hirst parser [\cite{feng2012text}], the Lin parser [\cite{lin2010pdtb}]. The first two parsers are based on RST theory and the last one is an end-to-end PDTB-style parser. We choose the Feng-Hirst parser and the Lin parser for our discourse parsing task for the following reasons:
\begin{itemize}
\item[1. ]They represent two most popular discourse analysis theories or styles: RST and PDTB, which allows us to make comparisons on the performance of our sentiment analysis task between these two discourse theories.
\item[2. ]HILDA parser is the ﬁrst fully-implemented feature-based RST-style discourse parser that works at the full text level. The Feng-Hirst parser takes HILDA as the basis and reports an improvement over HILDA parser on several features. According to this the Feng-Hirst parser can be regarded as a state-of-the-art RST parser.
\end{itemize}


The Feng-Hirst parser is a RST-style text-level discourse parser developed by Wei Feng and Graeme Hirst in Department of Computer Science, University of Toronto. It is based on a previous RST text-level discourse parser HILDA and incorporates rich-linguistic features such as semantic similarity and cue phrases.

The Feng-Hirst parser takes a text \textit{T} as input, outputs a segmentation \textit{S} of EDUs of \textit{T} (User specified segmentation is accepted) and discourse parse tree based on \textit{T} and \textit{S}. The Feng-Hirst parser was trained and tested on the RST Discourse Treebank (RST-DT) [\cite{carlson2003building}]. There are two steps in the work flow: EDU segmentation and Tree building. Previous parser HILDA has achieved a \textit{F}-score of 93.8\% on the EDU segmentation task and The Feng-Hirst parser took the EDU segmentation results from HILDA. For the tree building task the Feng-Hirst parser reported a 95.64\% accuracy and 89.51\% \textit{F}-score, both significantly over-perform HILDA parser. 

The Lin parser was developed by Ziheng Lin, Hwee Tou Ng and Min-Yen Kan from Department of Computer Science, National University of Singapore. It is the first full end-to-end discourse parser in the PDTB style. It takes a text \textit{T} as input and outputs a discourse structure of \textit{T}. The components of this parser consists of the connective classifier, the argument labeller, the explicit classifier, the non-explicit classifier, and the attribution span labeller. Figure \ref{fig:pdtbworkflow} shows the work flow of this parser. The relations the Lin parser tries to discover are the Level 2 relations in the PDTB discourse relation hierarchy (Recall Figure \ref{fig:pdtbrelations} for details). 
\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{pdtb_workflow.png}
\caption{The work flow of the Lin parser}
\label{fig:pdtbworkflow}
\end{figure}

The training and testing of the Lin parser was based on PDTB, using Sec. 02–21 for training, Sec. 22 for development, and Sec. 23 for testing as suggested in [\cite{prasad2007penn}]. They reported a \textit{F} score of 86.77\% over the baseline (uses only the connectives as features, obtain \textit{F} score of 86.00\%) for their explicit classifier; a \textit{F} score of 39.63\% for the implicit classifier. Given the fact that the results of implicit relations identification is ideal and might introduce noise to our later sentiment classification task. We take only parsing results of explicit relations and their arguments in our work.


\subsection{Sentiment Analysis}
Work which deals with the computational models of opinion, sentiment, and subjectivity in texts (most common), speeches and other forms of natural language, is known as opinion mining or sentiment analysis. The term "sentiment analysis" is now used to refer computational analysis which automatically extract, evaluate and predict/determine the judgement/attitude in given texts. Early work appears during 2001 such as [\cite{das2001yahoo}] and [\cite{tong2001operational}] whose focus were market or business use. It is still true that work in sentiment analysis field has potential on marketing applications in the Web. This is also one of the reasons that most work in this field are about online reviews such as product reviews, hotels reviews, restaurant reviews, etc.

In the following sections we first briefly introduce general approaches to sentiment analysis, and then introduce work with consideration of discourse and aspect.

\subsubsection{Sentiment Analysis: Approaches}
Sentiment analysis could be carried on at different levels based on the length of input text, from word level to review level. Word-level and phrase-level sentiment analysis take ad-vantage of previous work in \textit{Distributional Semantic}, with the objective of determining the polarities of unseen words or phrase. This is out of the range of this thesis, since our major task is to test the influence of discourse structure on EDU-level analysis.

Some common approaches to sentiment analysis beyond sentence level are named here: Language model is capable to classify whether a span of text is subjective and further positive or negative, to a certain degree. It is commonly used as a basic feature for classification. (for instance, in [\cite{taboada2009genre}] n-gram features are used.); Additional lexical resources are built as knowledge bases with polarity annotated tokens/words/concepts. There are many such lexical resources for English and we choose three of them in this work for a wider coverage: AFINN\footnote{http://www2.imm.dtu.dk/pubdb/views/publication\_details.php?id=6010}  [\cite{nielsen2011new}], A list collected by Minqing Hu and Bing Liu (referred to Hu-Liu)\footnote{http://www.cs.uic.edu/~liub/FBS/opinion-lexicon-English.rar} since their work [\cite{hu2004mining}], Lexicon of OpinionFinder system\footnote{http://mpqa.cs.pitt.edu/opinionfinder/} [\cite{wilson2005opinionfinder}].

\subsubsection{Sentiment Analysis with discourse features}
In the field of sentiment analysis, much work has been done based on local information of sentences (say, without considering relation or connection between sentences). [\cite{polanyi2006contextual}] argues that local concentration “is incomplete and often gives the wrong results when implemented directly”, and polarity calculation is affected by some lexical and discourse context. Discourse information is therefore considered as an important feature for polarity prediction.

Instead of considering each sentence equally, people have tried to measure different degree of contributions of sentences or sub-sentence to the overall sentiment expressed in a document. There are two main approaches to this discourse-sensitive document level sentiment analysis task. One approach is rule-based. [\cite{somasundaran2009supervised}] employs a constraint-based approach to restrict opinion prediction such that text spans targeted at same entity with relations in their discourse schema will be constrained with same polarity. [\cite{zhou2011unsupervised}] defines a discourse schema with discourse constraints on polarity to dis-cover intra-sentence level discourse relations for eliminating polarity ambiguities. One ex-ample of these discourse constraints is that two pieces of text spans connected by a Contrast relation hold opposite polarities.

Another approach is weight-based. After extracting discourse structures from text, text spans are assigned different weights for contributing the polarity score according to their roles in discourse relations. [\cite{taboada2008extracting}] hypothesizes several weighting schemes, and achieves best results on “1.5-0.5 weight” which gives a weight of 1.5 to words expressed in nuclei and 0.5 to satellites. Later work such as [\cite{wu2012exploiting}] assumes that nuclei and satellites of different relations should also be weighted differently, and some relations should have higher weights than others. They train a linear optimizer to find the best weights. Both approaches reported improvements over a lexicon-based baseline that does not take discourse structure into account. 

The focus of previous work has been document level polarity prediction using discourse information. To extract sentiment polarity on a more fine-grained level, some work has been done on sentence/sub-sentence level polarity prediction. [\cite{zirn2011fine}] uses discourse relations on sub-sentence level sentiment prediction. Our intuition is similar to this work, but we consider a broader range of discourse relations while they only distinguish Contrast relations and non-Contrast relations. Moreover, we take into consideration the influence of discourse relations on aspects shift rather than on polarity alone.

Works mentioned above detect discourse relations by looking for explicit discourse connectives, or training classifiers on annotated data. Some works take advantages of state-of-the-art discourse parsers. For example, [\cite{taboada2008extracting}] and [\cite{heerschop2011polarity}] use some additional tools like Sentence-level PArsing of DiscousE (SPADE) [\cite{soricut2003sentence}] to indicate discourse relations. Using discourse parsers may bring in noise, but will increase the coverage of discourse information.

The term discourse structure was used by all works mentioned, while in some work it just refers to the usage of discourse phenomenoa, in some other works RST-style discourse structure is referred to. Discourse parsers we use in this work implement not only RST-style structure, but also PDTB-style structure, and hence we uniformly use the term discourse structure for these two specific theories.

\subsubsection{Aspect-based Sentiment Analysis}
We take aspect information into consideration in our work for the following reasons. First, in order to predict the "relevant" polarity expressed by an EDU, we shall know the topic or aspect of review this EDU is about. By only focusing on content relevant to the current aspect, the prediction result we get should be more realistic. Second, discourse structure could indicate not only information about polarity continuity, but also some other information, one of which is aspect information. We add features about aspect in our work and could test whether aspect information has influence on polarity continuity through whole text.

There are several works concerning aspect-based sentiment analysis. [\cite{wang2010latent}] and [\cite{brody2010unsupervised}] use the Latent Dirichlet allocation (LDA) topic modeling method to gather content about same topic aspect and use some state-of-art features to calculate the polarity of that aspect and each aspect’s influence on overall sentiment rating. They also calculate different influence of aspect on the overall polarity of a review. We got some motivations from previous work about how to detect aspect-relevant content.

Our implementation of aspect extraction (will be discussed in Chapter 4) is pretty simple since this is not prior task. We consider most aspect information are expressed in nouns and noun phrases. So we implemented a method clustering all mentioned noun phrases for each aspect and select those beyond a threshold as the \textit{representatives} that could reflect one aspect. Aspect shifts from EDU to EDU are tested, to examine whether this information could help our sentiment prediction task.

\subsection{Summary}
In this chapter we introduced two different style discourse theories. We took one state-of-the-art discourse parser corresponding theories. The Feng-Hirst parser implemented RST theory which constructs a tree structure for each text, while the Lin parser is PDTB style so that it focuses more on local connections. We also discussed different approaches to sentiment analysis, pointing out our major research problem is EDU level sentiment prediction, with simple lexical features and features extracted from discourse structures.


