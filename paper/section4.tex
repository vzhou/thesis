\section{Sentiment Classification}
In this chapter, we describe the features used in our sentiment classification task. The first set of features contains lexical scores for EDUs, from three lexical resources; the second set of features considers the influences of adjacent EDUs; the other two sets of features respectively model parsing results of the Feng-Hirst parser and the Lin parser.

The basic assumption of using discourse features is as follows: when we want to predict the sentiment of a certain EDU  (we call it current EDU), we can use the sentiments of other EDUs which have some discourse relations with current one. The type of discourse relation might tell whether there is going to be a polarity shift from the linked EDU and the direction of the polarity shift. For example, a contrast relation is likely to trigger a polarity shift, and the shift is probably from negative to positive or vice versa. The goal of modelling discourse features is to extract information from our parsing results and normalize this information in order to be trained for machine learning algorithms.

This chapter is organized as follows, section 4.1 explains a simple lexicon-based method which we take as a baseline; section 4.2 introduces how we use the polarities of adjacent EDUs to predict current EDU; section 4.3 and 4.4 respectively present the modelling procedure of parsing results of the Feng-Hirst parser and the Lin parser.

\subsection{Lexicon-based features}
As there are many ways to take advantage of lexical resources in the sentiment analysis task, as introduced in Chapter 2, we would like to take a simple one as the baseline of this work. We implement two methods for using three of our lexical resources: the first one takes an EDU as bag-of-words; the second one considers syntactic structure.

\subsection{Simple lexical score}
The bag-of-words method works as follows. For each EDU, we look up the polarity score of each word in three lexical resources (any unfound word will be assigned 0), and sum up the scores for each EDU. Since we have three dictionaries, we have three summed scores for each EDU. We take each score as the decision (1 for positive, -1 for negative, 0 for neutral) of each dictionary, and vote among these three decisions. For example, a voting result of 3 means all three dictionaries give a positive score for certain EDU. This voting result is taken as the simple polarity score. Its value is an integer from range [-3,3] corresponding to most negative to most positive. This method is applied to both of the datasets and used as the baseline of the experiment. 

The bag-of-words method considers negations. Once a negation connective\footnote{negation connectives used in this thesis include ``not", ``no", ``don't", ``doesn't", ``never", ``hardly", ``none", ``nothing", ``nowhere", ``neither", ``nor", ``nobody", ``scarely", ``barely", ``can't", ``won't", ``wouldn't", ``shouldn't", ``couldn't"} is found in an EDU, the polarity score of the rest will be multiplied by \textbf{-1}, which means the polarity of the rest of the EDU is inverted. Consider the following example,

\begin{example}
negation

This(0) is(0) not(0) a(0) nice(3) place(0) to(0) stay(0).
\end{example}
Numbers in the brackets are the polarity scores found in AFINN for each word. The final AFINN polarity score of this EDU is calculated as: $$ (0+0+0)+(-1)*(0+3+0+0+0)=-3 $$


We also implement another method for the TripAdvisor dataset, taking into consideration more syntactic information. In this dataset we have the annotations of aspects information and we want to distinguish contents within one EDU by whether it is aspect relevant. Precisely if some part of a EDU is not relevant to the topic/aspect being talked about in this EDU, we will ignore the polarity value of this part of text. To understand the motivation of this method, see the following example,
\begin{example}
Aspect relevant
\\On that terribly rainy night we were glad to meet that helpful stuff.  \emph{\textbf{service}}  \emph{pos}
\end{example}
In this EDU, the token \textit{terribly} bears negative sentiment while it is not relevant to the current aspect (service). So its polarity score shouldn't be counted into the polarity result of this EDU.

\subsubsection{Relevant aspect classification}
A procedure of aspect classification is needed. We implemented a simple method since our prior focus is sentiment prediction task using discourse structure. The goal of this step is to gather representative noun phrases or nouns of each aspect. With this step we can distinguish between relevant and irrelevant content with regard to the current aspect. Aspect information is obtained from annotations of the corpus. We cluster the most representative lemmas (ideally nouns) for each aspect, using a chunker TreeTagger\footnote{http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/}. Each aspect corresponds to a bag of lemmas. For each EDU, we first identify words relevant to the concerned aspect, using the clusters mentioned above. We then try to find out connections between these aspect relevant words and words that bear a non-zero polarity score. We use Stanford Dependency Parser [\cite{de2006generating}] to find out these connections. If there exist a dependency between a polarity-bearing word and an aspect-relevant word (or a pronoun), or there exist another word that has a dependency with polarity-bearing word and aspect-relevant word (or a pronoun), then this polarity-bearing word is considered as aspect relevant. Words that are not aspect relevant are not counted when calculating the polarity score of a EDU. Consider the example mentioned above,

\begin{example}
Aspect relevant lexical score
\\On(0) that(0) terribly(-3) rainy(-1) night(0) we(0) were(0) glad(3) to(0) meet(0) that(0) helpful(2) stuff(0). \emph{\textbf{service}}  \emph{pos}
\end{example}

Scores in the brackets are found in AFINN. The simple polarity score of this EDU would be: $$sum(0,0,-3,-1,0,0,0,3,0,0,0,2,0)=1$$
The word ``night" is not found in the aspect relevant cluster of current aspect (``service") and ``stuff" is. Following dependencies about relevant polarity-bearing words in this EDU are found in the parsing results of Stanford Dependency Parser:

\textit{advmod(rainy-4, terribly-3), amod(night-5, rainy-4), nsubj(glad-8, we-6), amod(stuff-13, helpful-12)}

A decision is made that ``terribly" and ``rainy" are aspect irrelevant, while ``glad" and ``helpful" are aspect relevant. The aspect-relevant polarity score is updated  as: $$sum(0,0,0,0,0,3,0,0,0,2,0)=5$$

We can see that the aspect-relevant score could better reflect the real polarity trend for this EDU.

With this extra step, we are able to filter out the noise from aspect irrelevant words and get a more accurate polarity score. The result of this method is referred to aspect-relevant polarity score, to be distinguished from the results of the previous method, namely simple polarity score.

\subsection{Adjacency-based features}
In order to predict the sentiment of an EDU (current EDU) that may not have an evident lexical score, we might want to consider the polarities of adjacent EDUs of the current EDU. This method is based on the assumption that people trend to write coherent text. This coherence exists between text spans (e.g. EDUs) that are close to each other. So it is very likely that adjacent EDUs share the same polarity of current EDU. Consider the following example:
\begin{example}
Adjacent EDUs
\begin{itemize}
\item[(1) ]It is outdated , 	\emph{\textbf{rooms}}	\emph{neg}
\item[(2) ]the television was old and did not work properly , \emph{\textbf{rooms}}	\emph{neg}
\item[(3) ]the phone did not work properly , 	\emph{\textbf{rooms}}	\emph{neg}
\item[(4) ]and there was this exercise bike next to the bed . 	\emph{\textbf{rooms}}	?
\end{itemize}
\end{example}
In this example, all EDUs are coherent. In order to predict the sentiment of EDU (4), we can take into consideration of EDU (1) to EDU (3) and predict the forth one as\textit{ negative} also.

To generalize this adjacency-based method, we can set up a window about how many adjacent EDUs should be considered. The polarities of these EDUs will be modelled as a feature for current EDU with respect to their relative location. For example, in order to predict the sentiment of EDU (4) with window size 2, we add a feature named \textit{previous-1} with the value \textit{negative}, and a feature named \textit{previous-2} with the value \textit{negative}. \textit{next-1} and \textit{next-2} could be modelled similarly


\subsection{RST-based discourse features}
For each review, the Feng-Hirst parser gives a RST parsing tree as result. The leaves of the tree are EDUs. Two EDUs are connected by a relation, and this relation is taken as a relation node for further tree construction. The term \textit{relation node} is defined as the sub-tree structure with this relation as the root.

Our task here is how to model the parsing tree such that each EDU gets a set of features which can indicate the current EDU's relations with others. These relations include the type of discourse relation that link the current EDU to other EDUs, the role the current EDU plays in a certain relation (say, nucleus or satellite), whether there is an aspect shift from another linked EDU (used for the TripAdvisor dataset only). Since the tree structure modelling is complicated, we start with representation of direct relations that connect two EDUs (leaves in the tree), and then explain how to extend the representation for relations that connect nodes.

\subsubsection{Relation Representations}
\paragraph{Direct relation representation}.

Consider the following example from our parsing results of the Feng-Hirst parser of the TripAdvisor dataset:

\begin{example}
Contrast relation
\begin{itemize}
\item[] Contrast [S][N]
\item[] [We seemed to be the only non business folks] $_\mathrm{EDU1}$
\item[] [but that was not a problem] $_\mathrm{EDU2}$
\end{itemize}
\end{example}

EDU1 (satellite) and EDU2 (nucleus) are connected by a contrast relation. When we want to predict the sentiment of EDU2, we would like some “help” from EDUs we've seen in the text, in this case, EDU1. Given that EDU1 is neutral, the information we can use for predicting the sentiment of EDU2 is that it is the Nucleus of a contrast relation in which the other EDU is neutral, formally \textit{nucleus-contrast-neu}. 

To make it easier to understand, we can think the role (nucleus or satellite) as the name of edge in tree structure. The available information we have for predict sentiment of EDU2, represented as a tree structure, is shown in the following diagram:

\begin{center}
\Tree[4]{ & \K{Contrast}\B{dl}_{satellite}\B{dr}^{nucleus} \\
\K[-12]{\emph{We seemed to be the only non business folks}}\B{d} & & \K[12]{\emph{but that was not a problem}}\B{d} \\
\K{EDU1,\emph{neutral}}& &\K{EDU2,?} }
\end{center}

For EDU2, \textit{nucleus-contrast} is the path it has to go through in order to connect to EDU1, and polarity of EDU1 is neutral. Then we add \textit{nucleus-contrast-neu} as a feature for EDU2 and name \textit{nucleus-contrast-neu} the path of EDU2 in this contrast relation. We don't include in the path the edge that connects EDU1 (not like nucleus-contrast-\textbf{satellite}) because the role current EDU plays is the central concern and the other role can be inferred.

\paragraph{Longer path representation}.

In previous section we discussed how to model one relation that links two EDUs directly. Modelling higher level of the tree is similar. There are two more issues for describing indirect relations, first one is how to extend the path representation if it is longer; the other one is how to determine the polarity of a relation node.

Consider following example,
\begin{example}
Longer path
\begin{center}
\Tree{& \K{r1} \B{dl}_{nucleus}\B{dr}^{satellite}\\
\K[-5]{EDU1,\emph{neutral}} && \K{r2}\B{dl}_{satellite}\B{dr}^{nucleus}\\
& \K[-5]{EDU2,\emph{negative}} && \K[5]{EDU3,\emph{?}}}
\end{center}
\end{example} 

There are two usable paths for EDU3, the first one is the direct path of relation r2:
\begin{itemize}
\item[(1)] \textit{nucleus-r2-neg}
\end{itemize}
the second one is a longer one, up to relation r1:
\begin{itemize}
\item[(2)] \textit{nucleus-r2-satellite-r1-neu}
\end{itemize}

For a given EDU, there is a discourse relation that takes this EDU as its child directly and there is another node/leaf as the other child of this relation. If this relation node is not the root node of the parsing tree, there will be another relation node that takes this relation node as a direct child and provides a sibling node for this relation node. So from the given EDU, every time we seek upwards, we would find a relation that takes current node as source node and another node as target node until we reach the root node. For every relation we go through, we can add a path feature for current EDU with the format ``p-s” where p stands for the path the current EDU has to go through to reach the relation and s stands for the sentiment of the other child node of this relation. Consider the example, we start from EDU3 and look upwards. A relation r2 is found, the path to r2 is \textit{nucleus-r2} and the polarity of the other node/leaf of r2 is \textit{negative}, so we get the first path Path(1) with the ``p-s” format; then we continue looking up, relation r1 is found, the path from EDU3 to r1 is \textit{nucleus-r2-satellite-r1} and the polarity of the other node of r1 is \textit{neutral}, so Path(2) is obtained.

Once we extend the representation to this level, every EDU is involved into one relation at each level of the tree, which is not realistic. Some EDUs play their roles locally and locally only. In RST, \textit{satellites} are those EDUs that are ``less important", and shouldn't influence others so much. So in our path representation, we filter our some paths that get more than two satellites involved. Consider EDU2 in Example 4.6., the path for EDU2 at length two is \textit{satellite-r2-satellite-r1-neutral}. There are two satellites in this path, hence we consider this path nonsignificant and filter it out. The intuition of this filter is that satellites are the complement part in one relation and it is weakly involved in upper level relations.


We also need to define the polarity of a relation node. The polarity of a RST-style discourse relation node depends on polarities of the leaves under this relation. Its value is assigned as follows,
\begin{itemize}
\item[1.] If there is only one nucleus in this relation, the polarity of this relation is the same as the polarity of its nucleus child node (relation clause or leaf EDU).
\item[2.] If there is more than one nucleus in this relation, the polarity of this relation equals to the major polarity of these nuclei.
\item[3.] If there is more than one nucleus in this relation and there is no majority, the polarity of this relation is set as `neutral'.
\end{itemize}
Consider the following examples:
\begin{example}
Relation node polarity Rule 1
\begin{center}
\Tree{&\K{r1}\B{dl}_{satellite}\B{dr}^{nucleus}\\
\K[-4]{e1, \textit{positive}}&&\K[4]{e2, \textit{negative}}
}
\end{center}
\end{example}
A relation node r1 consists of two EDUs e1 and e2, and e2 is the only nucleus. The polarity of nucleus is assigned to the clause. So the polarity of r1 is negative.

\begin{example}
Relation node polarity Rule 2
\begin{center}
\Tree{&\K{r2}\B{dl}_{nucleus}\B{dr}^{nucleus}\\
\K[-4]{e3, \textit{positive}}&&\K[4]{e4, \textit{positive}}}
\end{center}
\end{example}
A relation node r2 consists of two EDUs e3 and e4, and both of them are nuclei. The polarity of their agreement is assigned to the node. So the polarity of r2 is positive.

\begin{example}
Relation node polarity Rule 3
\begin{center}
\Tree{&\K{r3}\B{dl}_{nucleus}\B{dr}^{nucleus}\\
\K[-4]{e5, \textit{positive}}&&\K[4]{e6, \textit{negative}}}
\end{center}
\end{example}
A relation node r3 consists of two EDUs e5 and e6, and both of them are nuclei. The polarity of r3 is set as neutral since there is no majority among polarities of child nodes.

It has been discussed that a RST tree is a tree where leaves are EDUs and nodes are discourse relations that connect its children. We use \textit{path length} to describe how far it is for an EDU to reach a relation node. For a tuple (r, e, n) where r is a discourse relation that connects the current EDU e and the other node/leaf n, the path length of this node r with respect to EDU e is defined as the number of relation nodes in the path from e to r. Take paths in Examples 4.6, Path(1) \textit{nucleus-r2-neg} has a path length 1, Path (2) \textit{nucleus-r2-satellite-r1-neu} has a path length 2.

From the structural information of a parse tree, we can extract more information to distinguish discourse relations of different heights. Consider a pair (r1, e, n1) and (r2, e, n2) with e as the current EDU whose sentiment we want to predict, if r1 has a longer path length than r2, then relation r1 might be weaker, which makes r2-n2 more important than r1-n1 when predicting the sentiment of e. Consider the following example, 

\begin{example}
Path length influence \newline
[My partner and I stayed for two weekend nights.] $_\mathrm{EDU1}$
[While it was not a hideous hotel experience, ] $_\mathrm{EDU2}$
[there was no joy going back to our room.] $_\mathrm{EDU3}$
\end{example}
\begin{center}
\Tree{& \K{Joint(r1)} \B{dl}_{nucleus}\B{dr}^{nucleus}\\
\K[-5]{EDU1,\emph{neutral}} && \K[5]{Contrast(r2)}\B{dl}_{satellite}\B{dr}^{nucleus}\\
& \K[-3]{EDU2,\emph{negative}} && \K[3]{EDU3,?}}
\end{center}

In order to predict the polarity of EDU3, we can use the polarities of EDUs that have discourse relations with it, in this case, EDU1 and EDU2. EDU3 and EDU2 are connected directly by a contrast relation r2 (path length 1); EDU3 and EDU1 are connected indirectly through another node and the corresponding relation is a joint relation r1 (path length 2). A normal interpretation is that EDU3 is closer related to EDU2 than EDU1, which means the contrast relation r2 should have more influence than the joint relation r1 when predicting the polarity of EDU3.

The more general assumption of this scenario is the further a relation is from an EDU, the less important the role it could play on sentiment prediction. In order to test this assumption, we implement two models based on the parsing results of the Feng-Hirst parser: one takes into consideration all available discourse relations that are reachable from the current EDU (the one whose sentiment we want to predict); the other one is limited to relations within certain path length.



\subsubsection{Models}
\paragraph{Full path model}
In this model all relations are considered and treated equally. Consider Example 4.10, EDU3 gets involved in two discourse relations: Contrast relation r2 connects two EDUs, while Joint relation r1 connects a EDU and a relation node. There are two paths that could be taken as useful information for predicting the polarity of EDU3, respectively,
\begin{itemize}
\item[{(a)}]\textit{nucleus-Contrast-nucleus-Joint-neutral}
\item[{(b)}]\textit{nucleus-Contrast-negative}
\end{itemize}
Path (b) describe the Contrast relation between EDU2 and EDU3 and the fact that EDU3 is the nucleus in this relation and EDU2 is negative. Direct relations as such are clear, while indirect relations like Path(a) are more complicated. Facts we can extract from this joint relation include: The Joint relation connects one EDU and one relation node; the polarity of this EDU is neutral; both this EDU and the node are nuclei. When we use Path(a) to predict the polarity of EDU3, what's actually being used is the influence from the Joint relation r1. In other word, Path(a) is applicable because EDU3 is a descendant of a node which is a child of this Joint relation. So in Path(a), the Contrast relation is more a connecting node than a relation that provides more expressive information. Motivated by this, we can rewrite Path(a) as a short version Path(a.s),

\begin{itemize}
\item[{(a.s)}]\textit{ nucleus-Joint-neutral}
\end{itemize}
Its meaning is \textit{current EDU is the nucleus of this joint relation, and the other argument of this relation is neutral}. We can benefit from the short rewriting role such that it's easier for the classifier to find the regularity of how each relation works since the representation is uniform. By rewriting the path in a shorter version, we can also avoid the problem of feature sparsity. In fact, it is shown from our datasets that if the longer version path is used, the feature space is too sparse to train a applicable model.


\paragraph{Path length controlled model}
This model restricts the relations to a certain path length. The assumption is that EDUs that are closer to each other trend to express related topics and to be more coherent. This can be applied to the discourse parsing tree: For a certain EDU, if using a relation with a longer path to model discourse structure, the further the connecting text span is, and hence the less coherent they are expected to be. In other words, if we choose a higher level discourse relation to model the discourse structure of an EDU, it might not be helpful for the sentiment prediction task. Moreover, it might introduce noise if it is treated equally as other lower level relations.

In order to test this, we implement a path length controlled model which limits the path length from the current EDU to a relation no longer than two. It means only two types of relations are considered, the first type of relations connect the current EDU to some other node/leaf; the second type of relations connect the parent node of the current EDU to some other node/leaf. We set the maximum length to two, but it is extendable, another length restriction can be replaced and tested in the model easily.

In this model, we also distinguish the path length of a relation in the feature space. For example, for a contrast relation with path length one where current EDU is nucleus and the other node is negative, instead of using \textit{nucleus-contrast-neg} as the feature, we use \textit{length1-nucleus-contrast-neg}. Since our path length limitation is 2, the increase of size of features is still manageable. 

\subsection{PDTB-based discourse features}
For each review, the Lin parser identifies all the explicit and implicit discourse relations including the connectives (if any), the corresponding arguments (Arg1 and Arg2). Modelling the relations as features for PDTB-style parsing results is not as complicated as modelling RST trees, but it is not obvious to determine the boundaries of EDUs. In this section, we first explain how the parsing results of the Lin parser are mapped to EDUs, and then introduce the procedure of feature modelling.
\subsubsection{EDU Segmentation}
The annotations in our datasets are EDU-based such that every EDU has a polarity label, while the parsing results of the Lin parser (any other PDTB-style parsers should be similar) segment texts into \textit{Connectives, Arg1s} and \textit{Arg2s}. There are two differences between this representation and RST-style EDU representation:
\begin{itemize}
\item[(1) ] Both RST and PDTB representations from the Lin parser's parsing results take two major text spans. But the parsing result has a special \textit{connective} which doesn't belong to Arg1 or Arg2. We would like to map arguments to EDUs, but there will be some cases that PDTB arguments and annotated EDUs cannot match perfectly since EDUs contain the connective tokens but arguments don't.
\item[(2) ]As discussed in Chapter 2, the argument of a PDTB discourse relation doesn't have to be continuous. Some other text could embed into one argument. But RST EDUs are continuous, as is our annotated data.
\end{itemize}

To solve Issue(1), in the procedure of mapping the Lin parser's parsing results to annotated EDUs, we accept loose mappings. If a text span of the parsing result and the content of an annotated EDU is similar enough such that the difference is no more than one token, the mapping is considered tenable. Then we can analyse the relations between EDUs in this PDTB-style discourse structures. Consider the following example:
\begin{example}
PDTB EDU mapping
\begin{itemize}
\item[(a) ]\{Exp\_1\_Arg1 \emph{\textbf{There 's a great big tv}} Exp\_1\_Arg1\} \{Exp\_1\_conn\_Conjunction \emph{\textbf{and}} Exp\_1\_conn\} \{Exp\_1\_Arg2 \emph{\textbf{the bathroom is quite nice}} Exp\_1\_Arg2\}
\item[(b) ]EDU1:	There 's a great big tv 	\emph{\textbf{amenities}}	\emph{pos} \newline
EDU2:	and the bathroom is quite nice 	\emph{\textbf{rooms}}	\emph{pos}
\end{itemize}
\end{example}

(a) is the representation of the parsing results from the Lin parser, and (b) is the annotated data. In (a) \textit{Exp\_1} indexes this is the first explicit discourse relation in this text and \textit{\_conn} indicates a connective. We number the two EDUs as EDU1 and EDU2. EDU1 could be mapped to the Arg1 in the relation, while there is difference of one token (``and") between EDU2 and Arg2. This difference is ignored and we consider EDU2 and Arg2 match. Similar to a RST relation then, a PDTB-style Conjunction relation holds between EDU1 and EDU2.


Issue(2) happens quite often in the parsing results and the solutions varies depending on the concrete cases. Consider the following example:
\begin{example}
PDTB argument that crosses EDU boundaries

\{Exp\_3\_Arg1 \emph{\textbf{We were on the 10th floor which I was pleased about}} \{Exp\_3\_conn\_Cause \emph{\textbf{because}} Exp\_3\_conn\} \{Exp\_3\_Arg2 \emph{\textbf{we had a view of the city plus}} Exp\_3\_Arg2\} \emph{\textbf{we did not hear a lot of trafiic .}} Exp\_3\_Arg1\}
\end{example}
\textit{Exp\_3} indexes that this is the third explicit discourse relation in this text and \textit{\_conn} identifies the connective. In this example Arg2 is embedded within Arg1. We can divide the text into three EDUs if there is no other constraint, but we need to map this to the annotated data, which is as follows:
\begin{example}
Annotated data which cannot map to PDTB results
\begin{itemize}
\item[1. ]We were on the 10th floor which I was pleased about 	\emph{\textbf{rooms}}	\emph{pos}
\item[2. ]because we had a view of the city plus we did not hear a lot of trafiic . 	\emph{\textbf{rooms}}	\emph{pos}
\end{itemize}
\end{example}

The contents of EDU2 appear both in the Arg1 and Arg2 of the Cause relation of parsing result in previous example, which results in a case that this EDU could map to no parsing results. We are not able to assert EDU2 belongs to the first or second argument of this relation, and cannot assign to it the \textit{relation-polarity\_of\_the\_other\_argument} pattern either. In this case, we drop this relation because incorrect mappings introduce noises.

Once all annotated EDUs and arguments are mapped, we can take the discourse relations between arguments and build the PDTB-style discourse model for our sentiment prediction task.

\subsubsection{Feature Modelling}
One EDU (argument) in the PDTB parsing result involves in at most two discourse relations, sometimes none since we filter out the implicit relations. The PDTB-style discourse relations model local and linear structures, which means all relations connect only EDUs rather than relation nodes. Modelling PDTB discourse relations is quite similar to modelling direct RST relations. Instead of using nucleus/satellite to distinguish different arguments, PDTB relations use Arg1 and Arg2. Since Arg1 and Arg2 have their meanings (See Chapter 2), we decide to keep the distinction.

Consider the following example:
\begin{example}
PDTB feature modelling
\begin{itemize}
\item[(1) ]\{Exp\_2\_Arg1 \emph{The location is great} Exp\_2\_Arg1\} \{Exp\_2\_conn\_Conjunction \emph{and} Exp\_2\_conn\} \{Exp\_2\_Arg2 \emph{it 's a beautiful , grand old hotel.} Exp\_2\_Arg2\}
\item[(2) ]The location is great 	\emph{\textbf{location}}	\emph{pos}\\
and it 's a beautiful , grand old hotel. 	\emph{\textbf{amenities}}	\emph{?}
\end{itemize}
\end{example}
(1) is the parsing result of the Lin parser and (2) is from the annotated data. Suppose we want to predict the sentiment of the second EDU, what can be extracted from the parsing results include that two EDUs hold between a Conjunction relation, the second EDU is Arg2 and Arg1 is positive. We represent this as \textit{Arg2-Conjunction-pos}. This representation is similar to the representation of RST direct relations (those relations that has a path length 1). 

The PDTB features are sparse. There are two main reasons:
\begin{itemize}
\item[1. ]All the implicit relations are filtered out.
\item[2. ]Unlike RST, it's not necessary for every EDU to get involved into at least one discourse relations. In fact in the parsing results of the Lin parser, there are many EDUs standing alone, not connecting to any other EDU.
\end{itemize}

The second reason is also the major difference between RST and PDTB. RST cares more about a global discourse tree whose leaves are EDUs, while PDTB pays more attention to discovering discourse relations whose two arguments could be text spans of any length. In other words, RST concerns discourse structures at all levels and PDTB concerns more local structures. We will evaluate both methods and examine which representation is better in our sentiment prediction task.

\subsection{Aspect shift}
We also hypothesized in Chapter 1 that discourse relations may trigger a shift of aspects. In one of our dataset, the TripAdvisor dataset, there are annotations of aspects. So when modelling the path representation, we could also add some information about aspect shift. Consider Example 4.14., the original representation is \textit{Arg2-Conjunction-pos}, and we find there is an aspect shift from these two EDUs. So we rewrite this representation as \textit{Arg2-Conjunction-pos-Yes} where ``Yes" indicates that the aspect information of these two EDUs are shifted. 

As discussed in Chapter 1, the aspect shift information may help us predict the changes of sentiments from EDUs to EDUs better. But we only have the aspect annotations in one dataset the TripAdvisor dataset and the size of the dataset is small. If the aspect shift information is introduced into the representations, the features will be more sparse and hence it will be harder for the classifier to be trained.

\subsection{Summary}
In this chapter we discussed about four sets of features used in this thesis. The first set is lexicon based and assigns a polarity lexical score to each EDU; the second set is adjacency based which considers the sentiments of surrounding EDUs to predict the current one; the third and fourth sets are discourse based: the RST discourse parse tree is linearized to model the relations between EDUs; the PDTB relations are modelled in a similar way.


































