\section{Introduction}

Sentiment analysis\footnote{The task is also called \textit{Opinion Mining} in some literatures.}  has become an attractive research area in the field of natural language processing, with the aim of determining the attitude or sentiment expressed by a text. With a rapid growth of user generated contents on Web, especially in online review sites, it is found an interesting and useful task to find out what other people think. In a survey conducted by [\cite{horrigan2008online}] on over 2000 American adults, it is shown that among readers of online reviews of restaurants, hotels, and various services (e.g., travel agencies or doctors), between 73\% and 87\% report that reviews had a significant influence on their purchase.

The goal of sentiment analysis is to determine the sentiment or polarity\footnote{While the term \textit{polarity} is associated with several linguistic phenomena, it will be used to refer to sentiment polarity in this thesis.} of a piece of text. The length of target text leads to several various research problems. It starts with review or article level sentiment analysis, focusing on the overall polarity of a whole review or article. Polarities borne by shorter text within one review or article will not be differentiated.

Aspect-based\footnote{It is also referred as \textit{feature-based} sometimes, and an \textit{aspect} is called a \textit{topic}. \textit{Aspect} will be used in this thesis.} sentiment analysis goes one step beyond general sentiment analysis. An aspect is an abstract set of relevant things that people can judge. For example, service is a commonly used aspect in restaurant reviews. For aspect-based approach, a fine-grained analysis is made in order to identify sentiment orientations at the level of aspects or features. In such study, a procedure of aspect extraction is conducted before sentiment polarity prediction, and sentiments of each aspect are summarized afterwards. This task meets a practical demand for some online review sites such that, on a website that sells cellphones, reviews with distinctions on aspects like sound, battery, screen or camera would be useful.

There are some more fine-grained sentiment analysis, such as sentence-level, phrase-level and word-level sentiment analysis. Our focus in this thesis is sentiment analysis on elementary discourse unit (EDU) level. An EDU is a minimal block of a discourse analysis which is a meaningful and continues expression. It could be a sentence, a clause or a phrase. We think that the polarity within one EDU is continues, which makes EDU-level analysis important to understand the content precisely. If we regard an article or a paragraph consisting of minimal units that bear an uniform opinion, mining these uniform opinions would be an essential and important task to understand the whole article or paragraph. Consider following example from TripAdvisor\footnote{http://www.tripadvisor.com/}:
\begin{example}
Different opinions within one sentence

\textbf{We had a great dining experience at Rave to celebrate my son 's exam success,} but unfortunately the second time of visiting was not so good, \textit{when we just wanted a drink before going out for a meal.}
\end{example}
This is one sentence consisting of three EDUs with different topics and sentiments: the first EDU talks about previous experience and the polarity is positive; the second EDU is about this experience and is negative; the last one describes the time and is neutral. If we consider the overall sentiment of the whole text span, taking it as negative will lose certain mentioned information, resulting in an incomplete understanding of the text.

EDUs are linked together in an article via discourse relations. We hypothesize that some discourse relations will indicate a shift in either aspect, polarity, or both aspect and polarity. Motivated by this, in this thesis, we would try to apply some discourse features to predict sentiments of EDUs, aiming to understand sentiment-bearing text more specifically.

Discourse information could tell the shifts of aspects or/and polarity to some degree, and might be an interesting feature to improve polarity prediction. Here are two examples of aspect or/and polarity changes between two EDUs connected by a contrast relation, taken from TripAdvisor.
\begin{example}
Shift of Polarity and/or Aspect
\begin{itemize}
\item[(a) ][Although the appearance of the hotel front pales in comparison with the other 4 neighbouring hotel,]$_\mathrm{EDU1}$ [but the room was surprising roomy by NYC stds , clean and well-equipped.]$_\mathrm{EDU2}$
\item[(b) ][At some points there were large queues at check-in which we saw,]$_\mathrm{EDU1}$ [but what can you expect with a hotel with 1700 rooms!]$_\mathrm{EDU2}$
\end{itemize}
\end{example}

The contrast relation in (a) co-occurs with both an aspect change ($location \rightarrow rooms$) and a polarity change ($neg \rightarrow pos$), while in (b) there is only a polarity shift ($neg \rightarrow neu$).

Different discourse relations may also have different influences on polarity and/or aspect shift. As our primary goal is to predict polarity of an EDU \footnote{We refer the EDU we want to predict to current EDU in the rest of this thesis}, we will use data with aspect annotations to make it simple and clear. Our main focus is to measure different dis-course relations' influence on polarity continuity.

This method could be useful when there isn't enough lexical information to classify the sentiment or the words' sentiments within one EDU is contradictory. The latter scenario is likely due to the fact that natural language expression could be ambiguous, while the sentiment expressed in one EDU is definable. It is also a proper method to understand some sentences or some parts of sentences by understanding context.

We take text files from two datasets, use two different style discourse parsers to parse these files into EDUs and extract discourse relations among EDUs. These relations, together with adjacent relations and polarity scores from additional lexical resources, are used to build our model to represent these EDUs. Then we use cross-validation method to train and test our model. Following figure illustrates the work flow:

\begin{figure}[ht]
\centering
\includegraphics[width=0.6\textwidth]{workflow.png}
\caption{Work flow}
\label{fig:workflow}
\end{figure}


The contributions of this thesis include: apply rich discourse features to EDU level sentiment prediction with consideration of aspects; two state-of-the-art discourse parsers based on different theories are tested and compared.

The contents of rest of this thesis are organized as following,

Chapter 2 introduces the background of related research work, including different discourse modelling theories and previous approaches to sentiment analysis.\newline
Chapter 3 introduces additional resources employed in this thesis, including two datasets and three lexical resources.\newline
Chapter 4 explains features we use to classify sentiments, and how we extract these features from additional lexical resources and parsers' outputs.\newline
Chapter 5 presents the implementation of our methods and evaluation of the results, followed by comparisons and analysis on these results.\newline
Chapter 6 concludes with a brief summary of this thesis and possible feature work.\newline

